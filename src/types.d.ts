import {Color, Font} from 'excalibur';

// @ts-ignore
declare class ExcaliburFontHack extends Font {
  public _drawText (ctx: CanvasRenderingContext2D, text: string, colorOverride: Color, lineHeight: number): void
}