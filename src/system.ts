import {Entity, ImageSource, Logger, LogLevel, vec, Vector} from 'excalibur';

// HAHAHAHAHAHAH
// Prevents reading the whole src when checking if an image is loaded!
ImageSource.prototype.isLoaded = function () {
  return this.data.width > 0;
};

// let dynImport = () => {
//   // noinspection CommaExpressionJS
//    return  (Math.random(), 'FAKE')
// }
// import(`../res/${dynImport()}.png`)
// import(`../res/${dynImport()}.json`)

export const isProduction = process.env.FORCE_DEBUG == 'true' ? false : (process.env.NODE_ENV == 'production');

Logger.getInstance().defaultLevel = isProduction ? LogLevel.Fatal : LogLevel.Debug;

export const registerInWindow = (key: string, value: any) => (window as any)[key] = value;
export const debug = (...args: any) => isProduction ? null : console.debug(...args);
export const debugError = (...args: any) => isProduction ? null : console.error(...args);
export const debugEntity = (entity: Entity, ...args: any) => isProduction ? null : console.debug(`[${entity.name || entity.id}]`, ...args);

registerInWindow('vec', vec);
registerInWindow('Vector', Vector);

if (!isProduction) {
  window.addEventListener('focus', () => debug('document focus in'));
  window.addEventListener('blur', () => debug('document focus out'));
}