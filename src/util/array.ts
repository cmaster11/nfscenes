export function arrayLast<T> (arr: T[]): T | null {
  return arr[arr.length - 1];
}