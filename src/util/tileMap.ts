import {TiledMapResource} from '@excaliburjs/plugin-tiled';
import {Side, vec, Vector} from 'excalibur';

export function fixTileMapVector (v: Vector) {
  return v.add(vec(8, -8));
}

export function fixTileMapVectorForNPC (v: Vector) {
  // 14? :D 32 (npc height) -
  return fixTileMapVector(v)
    // // This applies a full -16 up, so the actor is in line with Tiled
    .add(vec(0, -8));
  //
  // // Then, we apply a vertical shift so that the actor collider
  // // actually is in level with the Tiled UPPER tile
  // .add(vec(0, -DirectionalActor.colliderOffsetStandingY / 2));
}

export function extractTileMapEdges (map: TiledMapResource) {
  // Extract all edge points
  const mapLayers = map.getTileMapLayers();
  const mapEdgePoints = mapLayers
    .map((l) => l.data.filter((c) => c.graphics.length != 0))
    .flat()
    .reduce((acc: Vector[], el) => {
      // For each cell, look around and if the next cell is empty, then mark the edge as a polygon one
      const {x, y, width, height} = el;
      // debug('cell', el.index, el.graphics.length, x, y);

      // Top
      if (!mapLayers.find((l) => (l.getCellByPoint(x, y - height)?.graphics.length || 0) > 0)) {
        acc.push(vec(x, y), vec(x + width, y));
      }
      // Bottom
      if (!mapLayers.find((l) => (l.getCellByPoint(x, y + height)?.graphics.length || 0) > 0)) {
        acc.push(vec(x, y + height), vec(x + width, y + height));
      }
      // Right
      if (!mapLayers.find((l) => (l.getCellByPoint(x + width, y)?.graphics.length || 0) > 0)) {
        acc.push(vec(x + width, y), vec(x + width, y + height));
      }
      // Left
      if (!mapLayers.find((l) => (l.getCellByPoint(x - width, y)?.graphics.length || 0) > 0)) {
        acc.push(vec(x, y), vec(x, y + height));
      }

      return acc;
    }, [])
    .reduce((acc: Vector[], el) => {

      // Dedup edge points
      if (acc.find((aEl) => aEl.equals(el))) {
        return acc;
      }

      acc.push(el);

      return acc;
    }, []);

  const sortedMapEdgePoints: Vector[] = [mapEdgePoints[0]];
  const mapEdgePointsToProcess = mapEdgePoints.copyWithin(0, 1);

  while (mapEdgePointsToProcess.length > 0) {
    // Find the closes point to the last one
    const lastPoint = sortedMapEdgePoints[sortedMapEdgePoints.length - 1];

    let closesPoint!: Vector;
    let closesPointDist = 9999;
    let closesPointIdx!: number;

    mapEdgePointsToProcess.forEach((el, idx) => {
      const dist = el.distance(lastPoint);
      if (dist < closesPointDist) {
        closesPointDist = dist;
        closesPoint = el;
        closesPointIdx = idx;
      }
    });

    mapEdgePointsToProcess.splice(closesPointIdx, 1);
    sortedMapEdgePoints.push(closesPoint);
  }

  const edges: [Vector, Vector][] = [];
  let lastEdgePoints: Vector[] = [];
  let lastEdgeSide!: Side;
  sortedMapEdgePoints.forEach((el, idx) => {
    if (idx == 0) {
      lastEdgePoints.push(el);
      return;
    }

    const lastEdgePoint = lastEdgePoints[lastEdgePoints.length - 1];
    const newSide = Side.fromDirection(el.sub(lastEdgePoint));

    if (lastEdgePoints.length == 1) {
      lastEdgePoints.push(el);
      lastEdgeSide = newSide;
      return;
    }

    if (lastEdgeSide != newSide) {
      edges.push([lastEdgePoints[0], lastEdgePoint]);
      lastEdgePoints = [lastEdgePoint, el];
      lastEdgeSide = newSide;
      return;
    }

    lastEdgePoints.push(el);
  });

  // Close the path
  edges.push([lastEdgePoints[0], edges[0][0]]);

  return edges;
}