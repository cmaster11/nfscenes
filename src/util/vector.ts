import {BoundingBox, Vector} from 'excalibur';

export function vectorAvg (...vectors: Vector[]): Vector {
  const len = vectors.length;
  if (len == 0) {
    return Vector.Zero;
  }
  if (len == 1) {
    return vectors[0];
  }

  return vectors.reduce((acc, el, idx) => {
    if (idx == 0) {
      return acc;
    }
    return acc.add(el);
  }, vectors[0]).scaleEqual(1 / len);
}

export function vecLimitByBoundingBox (v: Vector, bb: BoundingBox, padding: Vector): Vector {
  const newVec = v.clone();

  if (newVec.x < bb.left + padding.x) {
    newVec.x = bb.left + padding.x;
  } else if (newVec.x >= bb.right - padding.x) {
    newVec.x = bb.right - padding.x;
  }

  if (newVec.y < bb.top + padding.y) {
    newVec.y = bb.top + padding.y;
  } else if (newVec.y >= bb.bottom - padding.y) {
    newVec.y = bb.bottom - padding.y;
  }

  return newVec;
}