import {Shape, vec, Vector} from 'excalibur';
import {PolygonCollider} from 'excalibur/build/dist/Collision/Colliders/PolygonCollider';

const colliderSizeRegex = /^(\d+)x(\d+)(?:\+(-?\d+)x(-?\d+))?$/;

export function getColliderBoxFromColliderSizeStringForTileMapObject (value: string, anchor = Vector.Zero): PolygonCollider {
  const match = colliderSizeRegex.exec(value);
  if (!match) {
    throw new Error(`invalid colliderSize value ${value}`);
  }

  const [_, width, height, x, y] = match;

  const offset = vec(
    (x != null ? parseInt(x, 10) : 0),
    (y != null ? parseInt(y, 10) : 0),
  );

  const size = vec(
    parseInt(width, 10),
    parseInt(height, 10),
  );

  return Shape.Box(
    size.x,
    size.y,
    anchor,
    offset.sub(vec(0, 16)),
  );
}