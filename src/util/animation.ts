import {Animation, AnimationStrategy, Side, SpriteSheet, Util} from 'excalibur';

export function getAnimationFromSpriteSheetRow (
  spriteSheet: SpriteSheet,
  rowIndex: number,
  colIdxs: number[],
  durationPerFrameMs: number,
  strategy: AnimationStrategy = AnimationStrategy.Loop,
): Animation {
  return new Animation({
    frames: colIdxs.map((colIdx: number) => {
      return spriteSheet.getSprite(colIdx, rowIndex)!;
    })
      .map((f) => ({
        graphic: f,
        duration: durationPerFrameMs,
      })),
    strategy: strategy,
  });
}

export function getAnimationsForAllSides (
  spriteSheet: SpriteSheet,
  rowIndex: number,
  frameCount: number,
  durationPerFrameMs: number,
  strategy: AnimationStrategy = AnimationStrategy.Loop,
): Partial<Record<Side, Animation>> {
  return [Side.Right, Side.Top, Side.Left, Side.Bottom]
    .reduce((acc: Partial<Record<Side, Animation>>, side, idx) => {
      acc[side] = getAnimationFromSpriteSheetRow(
        spriteSheet,
        rowIndex,
        Util.range(idx * frameCount, idx * frameCount + (frameCount - 1)),
        durationPerFrameMs,
        strategy,
      );
      return acc;
    }, {});
}