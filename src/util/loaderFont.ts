import {Loadable} from 'excalibur';

export class LoaderFont implements Loadable<any> {
  data: boolean = false;
  private readonly font: string;
  private readonly timeout: number;
  private readonly interval: number;

  constructor (
    font: string,
    timeout = 5000,
    interval = 100) {
    this.font = font;
    this.timeout = timeout;
    this.interval = interval;
  }

  isLoaded (): boolean {
    return this.data;
  }

  async load (): Promise<boolean> {
    if (this.data) {
      return true;
    }

    let poller: number = 0;
    let timeout: number = 0;

    return new Promise((resolve, reject) => {
      // repeatedly poll check
      poller = window.setInterval(async () => {
        try {
          await document.fonts.load(this.font);
        } catch (err) {
          clearInterval(poller);
          if (timeout) {
            clearTimeout(timeout);
          }
          reject(err);
        }
        if (document.fonts.check(this.font)) {
          clearInterval(poller);
          if (timeout) {
            clearTimeout(timeout);
          }
          this.data = true;
          resolve(true);
        }
      }, this.interval);
      timeout = window.setTimeout(() => {
        clearInterval(poller);
        reject(new Error('timeout loading font'));
      }, this.timeout);
    });
  }
}