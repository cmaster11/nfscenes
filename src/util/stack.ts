import {debug} from '../system';

export type StackArgs<T> = {
  debugName?: string,
  defaultValue?: T,
  onTopValueChange?: (value: T | undefined) => void
}

export class Stack<T> {
  private values: T[] = [];

  constructor (private args?: StackArgs<T>) {
    if (args?.defaultValue != undefined) {
      this.args?.onTopValueChange?.(args.defaultValue);
    }
  }

  push (value: T) {
    this.values.push(value);
    this.onTopValueChange();
  }

  clear () {
    this.values = [];
    this.onTopValueChange();
  }

  pop (): T | undefined {
    const len = this.values.length;
    if (len == 0) {
      return this.args?.defaultValue;
    }
    const popped = this.values.pop();
    this.onTopValueChange();
    return popped;
  }

  top (): T | undefined {
    const len = this.values.length;
    if (len == 0) {
      return this.args?.defaultValue;
    }
    return this.values[len - 1];
  }

  private onTopValueChange () {
    const top = this.top();
    debug(this.args?.debugName, 'new stack value', top);
    this.args?.onTopValueChange?.(top);
  }
}