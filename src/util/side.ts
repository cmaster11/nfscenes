import {Side, Vector} from 'excalibur';

export function getSideFromString (value: string): Side | undefined {
  const direction = (<any>Side)[value];
  if (direction == null || direction == Side.None) {
    return undefined;
  }
  return direction;
}

export function getUnitVectorForSide (side: Side): Vector {
  switch (side) {
    case Side.None:
      return Vector.Zero;
    case Side.Top:
      return Vector.Up;
    case Side.Bottom:
      return Vector.Down;
    case Side.Left:
      return Vector.Left;
    case Side.Right:
      return Vector.Right;
  }
}