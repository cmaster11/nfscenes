import {Actor, Side, Sprite, SpriteSheet} from 'excalibur';
import {ReferenceComponent} from '../common/referenceComponent';

export function getResizedSprite (sprite: Sprite, width: number, height: number) {
  const cloned = sprite.clone();
  cloned.destSize = {width: width, height: height};
  return cloned;
}

export function getStillSpritesForAllSides (
  spriteSheet: SpriteSheet,
  rowIndex: number,
): Partial<Record<Side, Sprite>> {
  return [Side.Right, Side.Top, Side.Left, Side.Bottom]
    .reduce((acc: Partial<Record<Side, Sprite>>, side, idx) => {
      acc[side] = spriteSheet.getSprite(
        idx,
        rowIndex,
      )!;
      return acc;
    }, {});
}

export function getSittingSpritesForAllSides (
  spriteSheet: SpriteSheet,
  rowIndex: number,
): Partial<Record<Side, Sprite>> {
  return [Side.Right, Side.Left]
    .reduce((acc: Partial<Record<Side, Sprite>>, side, idx) => {
      acc[side] = spriteSheet.getSprite(
        idx * 6,
        rowIndex,
      )!;
      return acc;
    }, {});
}

export function getFirstSpriteInActor (actor: Actor | undefined): Sprite | undefined {
  if (actor == null) {
    return undefined;
  }

  if (actor.has(ReferenceComponent)) {
    actor = actor.get(ReferenceComponent)!.reference;
  }

  return actor.graphics.layers.get()
    .map((l) => l.graphics.map((g) => g.graphic instanceof Sprite ? g.graphic : undefined))
    .flat()
    .filter((g) => g)
    .at(0);
}