import {StateValueMap} from 'xstate';

export function getStateKey (stateValue: string | StateValueMap): string {
  return typeof stateValue == 'string' ? stateValue : Object.keys(stateValue).at(0) as string;
}