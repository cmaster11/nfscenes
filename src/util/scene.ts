import {TiledObject, TiledObjectComponent} from '@excaliburjs/plugin-tiled';
import {Actor, Scene} from 'excalibur';
import {registerInWindow} from '../system';
import {commonTags} from '../common/constants';

export function getActorForTiledObject (scene: Scene, o: TiledObject, skipFakeColliders = true): Actor | undefined {
  return scene.actors
    .filter((a) => skipFakeColliders ? (!a.hasTag(commonTags.fakeCollider)) : true)
    .find((a) => a.get(TiledObjectComponent)?.object.rawObject == o.rawObject);
}

export function getTiledObjectForActor (a: Actor): TiledObject | undefined {
  return a.get(TiledObjectComponent)?.object;
}

registerInWindow('getActorForTiledObject', getActorForTiledObject);
registerInWindow('getTiledObjectForActor', getTiledObjectForActor);