import * as ex from 'excalibur';
import {ActorArgs, Engine, GraphicsGroup, GraphicsLayer, ScreenElement, Sprite, Text, vec, Vector} from 'excalibur';
import {Completer} from '../util/completer';
import stripIndent from 'strip-indent';
import {BaseLevelController} from './baseLevelController';
import {GraphicsGrouping} from 'excalibur/build/dist/Graphics/GraphicsGroup';
import {getResizedSprite} from '../util/sprite';
import {debugEntity, registerInWindow} from '../system';
import {commonSounds} from './sounds';
import {sprites} from './resources';
import {fonts} from './fonts';

const dialogCornerSize = 32;
const dialogMainTextOrigin = vec(dialogCornerSize, dialogCornerSize);
const dialogWidth = 500;
const dialogLines = 4;
const dialogMaxChoiceRows = 2;
const dialogLineHeight = 20;

// 2 + 1, the 1 is the continue button
const dialogMaxChars = Math.floor((dialogWidth - (2 + 1) * dialogCornerSize) / 11);
const dialogWrapRegex = new RegExp(`(?![^\\n]{1,${dialogMaxChars}}$)([^\\n]{1,${dialogMaxChars}})\\s`, 'g');

const animationDialogBoxContinue = new ex.Animation({
  frames: [{
    graphic: getResizedSprite(sprites.dialogBox.getSprite(3, 0)!, dialogCornerSize, dialogCornerSize),
  }, {
    graphic: getResizedSprite(sprites.dialogBox.getSprite(3, 1)!, dialogCornerSize, dialogCornerSize),
  }],
  frameDuration: 350,
});

const animationDialogBoxChoose = new ex.Animation({
  frames: [{
    graphic: getResizedSprite(sprites.dialogBox.getSprite(4, 0)!, dialogCornerSize, dialogCornerSize),
  }, {
    graphic: getResizedSprite(sprites.dialogBox.getSprite(4, 1)!, dialogCornerSize, dialogCornerSize),
  }],
  frameDuration: 350,
});
registerInWindow('animationDialogBoxChoose', animationDialogBoxChoose);

type DialogGraphics = {
  dialogGraphics: GraphicsGroup
  dialogSize: Vector
}

const dialogSpriteCornerTopLeft = getResizedSprite(sprites.dialogBox.getSprite(0, 0)!, dialogCornerSize, dialogCornerSize);
const dialogSpriteCornerTopRight = getResizedSprite(sprites.dialogBox.getSprite(2, 0)!, dialogCornerSize, dialogCornerSize);
const dialogSpriteCornerBottomLeft = getResizedSprite(sprites.dialogBox.getSprite(0, 2)!, dialogCornerSize, dialogCornerSize);
const dialogSpriteCornerBottomRight = getResizedSprite(sprites.dialogBox.getSprite(2, 2)!, dialogCornerSize, dialogCornerSize);

function getDialogPanel (dialogWidth: number, dialogLines: number): DialogGraphics {
  // How many lines to show in the dialog
  const dialogSize = vec(dialogWidth,
    dialogCornerSize * 2 +
    // Accept 4 lines of text
    // The +2 is a corrective for the line height
    dialogLineHeight * dialogLines);

  const spriteBorderTop = getResizedSprite(sprites.dialogBox.getSprite(1, 0)!, dialogWidth - 2 * dialogCornerSize, dialogCornerSize);
  const spriteBorderBottom = getResizedSprite(sprites.dialogBox.getSprite(1, 2)!, dialogWidth - 2 * dialogCornerSize, dialogCornerSize);
  const spriteBorderLeft = getResizedSprite(sprites.dialogBox.getSprite(0, 1)!, dialogCornerSize, dialogSize.y - 2 * dialogCornerSize);
  const spriteBorderRight = getResizedSprite(sprites.dialogBox.getSprite(2, 1)!, dialogCornerSize, dialogSize.y - 2 * dialogCornerSize);
  const spriteBgCenter = getResizedSprite(sprites.dialogBox.getSprite(1, 1)!, dialogWidth - 2 * dialogCornerSize, dialogSize.y - 2 * dialogCornerSize);

  const dialogGraphics = new GraphicsGroup({
    members: [
      {
        graphic: dialogSpriteCornerTopLeft,
        pos: vec(0, 0),
      },
      {
        graphic: spriteBorderTop,
        pos: vec(dialogCornerSize, 0),
      },
      {
        graphic: dialogSpriteCornerTopRight,
        pos: vec(dialogWidth - dialogCornerSize, 0),
      },

      {
        graphic: spriteBorderLeft,
        pos: vec(0, dialogCornerSize),
      },
      {
        graphic: spriteBgCenter,
        pos: vec(dialogCornerSize, dialogCornerSize),
      },
      {
        graphic: spriteBorderRight,
        pos: vec(dialogWidth - dialogCornerSize, dialogCornerSize),
      },

      {
        graphic: dialogSpriteCornerBottomLeft!,
        pos: vec(0, dialogSize.y - dialogCornerSize),
      },
      {
        graphic: spriteBorderBottom,
        pos: vec(dialogCornerSize, dialogSize.y - dialogCornerSize),
      },
      {
        graphic: dialogSpriteCornerBottomRight,
        pos: vec(dialogWidth - dialogCornerSize, dialogSize.y - dialogCornerSize),
      },
    ],
  });

  return {
    dialogGraphics,
    dialogSize,
  };
}

export type DialogOptions = {
  sprite?: Sprite
  choices?: DialogChoiceArgs
  ignoreInteractionDisabled?: boolean
}

export type DialogChoiceArgs = {
  title: string,
  choices: string[]
}

export class DialogBox<T extends BaseLevelController<T>> extends ScreenElement {
  lci: T;

  dialogGraphics: DialogGraphics;
  leftSpriteBgGraphics: DialogGraphics;

  rightLayersOffset = Vector.Zero;

  layerBg: GraphicsLayer;
  layerMainText: GraphicsLayer;
  layerLeftSpriteBg: GraphicsLayer;
  layerContinue: GraphicsLayer;

  private _batches: string[][] = [];
  private _choices?: DialogChoiceArgs;
  private _currentBatchIdx = 0;
  private _doneCompleter: Completer<number> | null = null;
  private _currentChoiceIdx: number = -1;

  constructor (lci: T, args?: ActorArgs) {
    super({
      name: 'dialogBox',
      visible: false,
      ...args,
    });

    this.lci = lci;
    this.dialogGraphics = getDialogPanel(dialogWidth, dialogLines);

    // Make a square on the left
    // noinspection JSSuspiciousNameCombination
    this.leftSpriteBgGraphics = getDialogPanel(this.dialogGraphics.dialogSize.y, dialogLines);

    this.layerBg = this.graphics.layers.create({
      name: 'background',
      order: -1,
    });
    this.layerBg.use(this.dialogGraphics.dialogGraphics);

    this.layerLeftSpriteBg = this.graphics.layers.create({
      name: 'leftSpriteBg',
      order: -1,
    });

    this.layerMainText = this.graphics.layers.create({
      name: 'mainText',
      order: 0,
    });

    this.layerContinue = this.graphics.layers.create({
      name: 'continue',
      order: 1,
    });
  }

  _updateLayersPositions () {
    let rightLayersOffset = Vector.Zero;

    if (this.layerLeftSpriteBg.graphics.length > 0) {
      rightLayersOffset = vec(this.leftSpriteBgGraphics.dialogSize.x - 14, 0);
    }

    this.layerLeftSpriteBg.offset = Vector.Zero;
    this.layerBg.offset = rightLayersOffset;
    this.layerMainText.offset = rightLayersOffset.add(dialogMainTextOrigin);
    this.layerContinue.offset = rightLayersOffset.add(vec(
      dialogWidth - dialogCornerSize - 26,
      this.dialogGraphics.dialogSize.y - dialogCornerSize - 26,
    ));

    this.pos = vec(this._engine.screen.resolution.width - this.dialogGraphics.dialogSize.x - rightLayersOffset.x, this._engine.screen.resolution.height - this.dialogGraphics.dialogSize!.y);
  }

  onInitialize (_engine: Engine) {
    super.onInitialize(_engine);
  }

  _getTextBatches (text: string): string[][] {
    // Sanitize
    text = stripIndent(text);
    text = text.trim();

    // Wrap words limiting the characters
    text = text.replace(dialogWrapRegex, '$1\n');

    // Split the text in batches of [n] lines
    const allLines = text.split('\n');

    return allLines.reduce((batches: string[][], el, idx) => {
      const batchIdx = Math.floor(idx / dialogLines);
      if (batches.length - 1 < batchIdx) {
        batches.push([]);
      }

      batches[batchIdx].push(el);

      return batches;
    }, []);
  }

  async show (text: string | string[], options?: DialogOptions): Promise<number> {
    debugEntity(this, 'show dialog', text, options);

    const interactionWasDisabled = this.lci.interactionDisabled;
    if (interactionWasDisabled && options?.ignoreInteractionDisabled != true) {
      throw new Error('Another dialog is active!');
    }

    if (!interactionWasDisabled) {
      this.lci.setInteractionDisabled(true);
    }

    if (typeof text == 'string') {
      this._batches = this._getTextBatches(text);
    } else {
      // Treat as an array, which forces batches
      this._batches = text.reduce((arr: string[][], batch) => {
        const batches = this._getTextBatches(batch);
        arr.push(...batches);
        return arr;
      }, []);
    }

    this._currentChoiceIdx = -1;
    this._choices = options?.choices;

    this._doneCompleter = new Completer<number>();
    this._currentBatchIdx = 0;
    this.layerLeftSpriteBg.hide();

    if (options?.sprite) {
      let sprite = options.sprite.clone();
      const spriteSize = this.leftSpriteBgGraphics.dialogSize.y - 2 * dialogCornerSize;

      // We always want a square here, so if the source sprite is NOT a square, crop it
      if (sprite.sourceView.width != sprite.sourceView.height) {
        if (sprite.sourceView.width > sprite.sourceView.height) {
          const ref = sprite.sourceView.height;
          sprite.sourceView.x += (sprite.sourceView.width - ref) / 2;
          sprite.sourceView.width = ref;
        } else {
          const ref = sprite.sourceView.width;
          sprite.sourceView.y += (sprite.sourceView.height - ref) / 2;
          sprite.sourceView.height = ref;
        }
      }

      // We want to show a sprite on the left side
      this.layerLeftSpriteBg.use(new GraphicsGroup({
        members: [
          {
            graphic: this.leftSpriteBgGraphics.dialogGraphics,
            pos: Vector.Zero,
          },
          {
            graphic: getResizedSprite(sprite, spriteSize, spriteSize),
            pos: vec(dialogCornerSize, dialogCornerSize),
          },
        ],
      }));
    }

    this._showCurrentBatch();

    this.layerContinue.use(animationDialogBoxContinue);
    this.graphics.visible = true;

    return await this._doneCompleter!.promise.then((value) => {
      if (!interactionWasDisabled) {
        this.lci.setInteractionDisabled(false);
      }
      return value;
    });
  }

  onPreUpdate (engine: Engine, _delta: number) {
    super.onPreUpdate(engine, _delta);

    if (!this.graphics.visible || this.lci.actionHandledThisFrame) {
      return;
    }

    if (engine.input.keyboard.wasPressed(ex.Input.Keys.Space)) {
      this.lci.setActionHandledThisFrame();

      this._currentBatchIdx++;
      if (this._currentBatchIdx >= this._batches.length) {
        commonSounds.dialogBeep.play();

        if (this._choices == null) {
          // Simply close the dialog
          this.graphics.visible = false;
          this._doneCompleter?.complete(-1);
          return;
        }

        if (this._currentChoiceIdx >= 0) {
          // Pick the chosen choice
          this.graphics.visible = false;
          this._doneCompleter?.complete(this._currentChoiceIdx);
          return;
        }

        // Show the choice pick screen
        this._showChoices(0, false);
        return;
      }

      this._showCurrentBatch();
    } else {

      if (this._currentChoiceIdx > -1 && engine.input.keyboard.wasPressed(ex.Input.Keys.Up)) {
        this.lci.setActionHandledThisFrame();
        this._showChoices(this._currentChoiceIdx - 1);
      } else if (this._currentChoiceIdx > -1 && engine.input.keyboard.wasPressed(ex.Input.Keys.Down)) {
        this.lci.setActionHandledThisFrame();
        this._showChoices(this._currentChoiceIdx + 1);
      } else if (this._currentChoiceIdx > -1 && engine.input.keyboard.wasPressed(ex.Input.Keys.Right)) {
        this.lci.setActionHandledThisFrame();

        const nextIdx = this._currentChoiceIdx + dialogMaxChoiceRows;
        if (nextIdx <= this._choices!.choices.length - 1) {
          this._showChoices(nextIdx);
        }

      } else if (this._currentChoiceIdx > -1 && engine.input.keyboard.wasPressed(ex.Input.Keys.Left)) {
        this.lci.setActionHandledThisFrame();

        const nextIdx = this._currentChoiceIdx - dialogMaxChoiceRows;
        if (nextIdx >= 0) {
          this._showChoices(nextIdx);
        }
      }
    }

  }

  private _showCurrentBatch () {
    commonSounds.dialogBeep.play();

    const lines = this._batches[this._currentBatchIdx];

    const graphics: GraphicsGrouping[] = lines.map((el, idx) => {
      const y = idx * dialogLineHeight;
      return {
        graphic: new Text({
          text: el,
          font: fonts.mono32.clone(),
        }),
        pos: vec(0, y),
      };
    });

    this.layerMainText.use(new GraphicsGroup({
      members: graphics,
    }));

    this._updateLayersPositions();
  }

  private _showChoices (idx: number, playChoiceSound: boolean = true) {
    if (playChoiceSound) {
      commonSounds.menuSelect.play();
    }

    if (idx < 0) {
      idx = this._choices!.choices.length - 1;
    } else if (idx >= this._choices!.choices.length) {
      idx = 0;
    }

    this._currentChoiceIdx = idx;

    const choicesGraphics: GraphicsGrouping[] = [];

    this._choices!.choices.forEach((c, idx) => {
      const col = Math.floor(idx / dialogMaxChoiceRows);
      const row = idx % dialogMaxChoiceRows;

      const posX = col * (dialogWidth - 2 * dialogCornerSize) / 2;
      const posY = (2 + row) * dialogLineHeight;

      choicesGraphics.push({
        graphic: new Text({
          text: c,
          font: fonts.mono32.clone(),
        }),
        pos: vec(dialogCornerSize + posX, posY),
      });

      if (idx == this._currentChoiceIdx) {
        choicesGraphics.push({
          graphic: animationDialogBoxChoose,
          pos: vec(posX, posY - (dialogCornerSize / 2 - dialogLineHeight / 2) + 2),
        });
      }
    });

    const graphics = new GraphicsGroup({
      members: [
        {
          graphic: new Text({
            text: this._choices!.title,
            font: fonts.mono32.clone(),
          }),
          pos: Vector.Zero,
        },
        ...choicesGraphics,
      ],
    });

    this.layerContinue.hide();
    this.layerMainText.use(graphics);

    this._updateLayersPositions();
  }
}