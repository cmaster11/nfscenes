import {Actor, Camera, CameraStrategy, Engine, Vector} from 'excalibur';

export class NFLockCameraToActorStrategy implements CameraStrategy<Actor> {
  constructor (public target: Actor, public readonly offset = Vector.Zero) {
  }

  public action = (target: Actor, _cam: Camera, _eng: Engine, _delta: number) => {
    return target.center.add(this.offset);
  };
}