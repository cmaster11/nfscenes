import {Loadable} from 'excalibur';
import {debug} from '../system';

export class WaitForLoadable implements Loadable<boolean> {
  private _done = false;

  get data (): boolean {
    return this._done;
  };

  constructor (
    private label: string,
    private condition: () => boolean,
    private delay = 250,
  ) {
  }

  isLoaded (): boolean {
    return false;
  }

  load (): Promise<boolean> {
    return new Promise((res) => {
      let interval: number;
      interval = window.setInterval(() => {
        if (this.condition()) {
          window.clearInterval(interval);
          this._done = true;

          debug(`loadable [${this.label}] loaded`);
          res(true);
          return;
        }
      }, this.delay);
    });
  }

}