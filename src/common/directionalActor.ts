import * as ex from 'excalibur';
import {Actor, ActorArgs, AddedComponent, Engine, Side, Sprite, SpriteSheet, vec, Vector} from 'excalibur';
import {NFTouchEvent} from './nfTouchEvent';
import {getSittingSpritesForAllSides, getStillSpritesForAllSides} from '../util/sprite';
import {getAnimationsForAllSides} from '../util/animation';
import {debugEntity} from '../system';
import {commonSounds} from './sounds';
import {sleeper} from '../util/promise';
import {commonObjectProperties, commonObjectTypes, npcSpeedAction} from './constants';
import {DoorComponent} from './doorComponent';
import {BaseLevelController} from './baseLevelController';
import {DialogOptions} from './dialogBox';
import {InventoryComponent} from './inventoryComponent';
import {getTiledObjectForActor} from '../util/scene';
import {getSideFromString} from '../util/side';
import {TiledObject, TiledObjectComponent} from '@excaliburjs/plugin-tiled';
import {NPC} from './npc';
import {showUIPopup, UIPopup} from './ui';
import {Stack} from '../util/stack';
import {TrackingCameraLevel} from './trackingCamera';

const animationFrameDuration = 100;

const graphicKeyStill = 'still';
const graphicKeyAnimated = 'animated';
const graphicKeySitting = 'sitting';

export type ShowUIPopupArgsDirectionalActor = {
  duration?: number,
  offset?: Vector
}

export class DirectionalActor<T extends BaseLevelController<T>> extends Actor {
  static readonly colliderWidth = 14;
  static readonly colliderHeight = 12;
  static readonly colliderOffsetStandingY = 12;
  static readonly colliderOffsetSittingY = 12;
  // This is static so that only 1 pc can make sound
  static soundWalkPromise: Promise<any> | null = null;
  trackingCameraLevel: TrackingCameraLevel = TrackingCameraLevel.normal;
  lci: T;
  setDirectionBasedOnVelocity = false;
  readonly fixedDirection: Stack<Side> = new Stack<Side>({
    debugName: `${this.name || this.id}-fixedDirection`,
  });
  readonly isSitting: Stack<boolean>;
  baseZ: number = 0;

  readonly touching: Record<string | number, NFTouchEvent> = {};
  trackActor?: Actor;
  // Pause tracking
  readonly trackingDisabled = new Stack<boolean>({
    defaultValue: false,
  });
  readonly trackable = new Stack<boolean>({
    defaultValue: true,
  });
  private _lastDirection!: Side;

  constructor (
    lci: T,
    spriteSheet: SpriteSheet,
    args?: ActorArgs,
  ) {
    super({
      collider: ex.Shape.Box(
        DirectionalActor.colliderWidth,
        DirectionalActor.colliderHeight,
        Vector.Half,
      ),
      // NOTE: the collider offset is set by isSitting
      ...args,
    });

    this.componentAdded$.subscribe((ac) => this.onComponentAdded(ac));

    this.addComponent(new InventoryComponent());

    this.lci = lci;

    this.populateGraphicCache(graphicKeyStill, getStillSpritesForAllSides(spriteSheet, 0));
    this.populateGraphicCache(graphicKeyAnimated, getAnimationsForAllSides(spriteSheet, 2, 6, animationFrameDuration));
    this.populateGraphicCache(graphicKeySitting, getSittingSpritesForAllSides(spriteSheet, 4));

    this.direction = Side.Bottom;
    this.isSitting = new Stack<boolean>({
      debugName: `${this.name || this.id}-isSitting`,
      defaultValue: false,
      onTopValueChange: (isSitting) => this.collider.get().offset = ex.vec(0,
        isSitting ? DirectionalActor.colliderOffsetSittingY : DirectionalActor.colliderOffsetStandingY),
    });

    this.on('collisionstart', (e) => {
      const evt = new NFTouchEvent(e.other, e.contact);
      this.touching[e.other.id] = evt;

      // this.touching.push(evt);
      debugEntity(this, `collision start`, evt, this.touching);
      this.onCollisionStart(evt, this.touching);
    });
    this.on('collisionend', (e) => {
      const evt = this.touching[e.other.id];
      if (evt) {
        delete (this.touching[e.other.id]);
        debugEntity(this, `collision end`, evt, this.touching);
        this.onCollisionEnd(evt, this.touching);
      }
    });
  }

  get inventory (): InventoryComponent {
    return this.get(InventoryComponent)!;
  }

  get direction (): Side {
    return this._lastDirection;
  }

  set direction (value: Side) {
    // if (this.isSitting && this.sittingSpritesBySide[value] == null) {
    //   // If we're missing the sprite, skip the change of direction
    //   return;
    // }
    this._lastDirection = value;
  }

  private static getGraphicCacheKey (prefix: string, side: Side) {
    return `${prefix}_${side}`;
  }

  pickupObject (engine: Engine, actor: Actor) {
    // Pick up the object
    engine.currentScene.remove(actor);
    this.inventory.addTiledObject(getTiledObjectForActor(actor)!);
  }

  onCollisionStart (touchEvent: NFTouchEvent, touching: Record<number, NFTouchEvent>) {
    const object = touchEvent.tiledObject;

    if (object && !(touchEvent.actor instanceof NPC)) {
      if (object.type == commonObjectTypes.door) {
        const doorComponent = touchEvent.actor.get(DoorComponent);
        if (doorComponent) {
          if (!doorComponent.isLocked) {
            doorComponent.isOpen = true;
          }
        }
      }

      const fixActorDirection = object.getProperty<string>(commonObjectProperties.actorDirectionFixed);
      if (fixActorDirection) {
        const direction = getSideFromString(fixActorDirection.value);
        if (direction) {
          this.fixedDirection.push(direction);
        }
      }

      // Check if the object is forcing some properties on the NPC
      const forceActorStance = object.getProperty(commonObjectProperties.actorStance.key);
      if (forceActorStance) {
        switch (forceActorStance.value) {
          case commonObjectProperties.actorStance.options.sitting:
            this.isSitting.push(true);
            break;
        }
      }

      // Check if the actor cannot be tracked while in the area
      const actorTrackable = object.getProperty<boolean>(commonObjectProperties.actorTrackable);
      if (actorTrackable) {
        this.trackable.push(actorTrackable.value);
      }
    }
  }

  onCollisionEnd (touchEvent: NFTouchEvent, touching: Record<number, NFTouchEvent>) {
    const object = touchEvent.tiledObject;

    if (object && !(touchEvent.actor instanceof NPC)) {
      if (object.type == commonObjectTypes.door) {
        const doorComponent = touchEvent.actor.get(DoorComponent);
        if (doorComponent) {
          doorComponent.isOpen = false;
        }
      }

      const fixActorDirection = object.getProperty<string>(commonObjectProperties.actorDirectionFixed);
      if (fixActorDirection) {
        this.fixedDirection.pop();
      }

      // Check if the object WAS forcing some properties on the NPC
      const forceActorStance = object.getProperty(commonObjectProperties.actorStance.key);
      if (forceActorStance) {
        switch (forceActorStance.value) {
          case commonObjectProperties.actorStance.options.sitting:
            this.isSitting.pop();
            break;
        }
      }

      // Check if the actor cannot be tracked while in the area
      const actorTrackable = object.getProperty<boolean>(commonObjectProperties.actorTrackable);
      if (actorTrackable) {
        this.trackable.pop();
      }
    }
  }

  getTouchedAndFacedEvents (filterFn: (actor: NFTouchEvent) => boolean): NFTouchEvent[] {
    const colliderCenterSelf = this.collider.get().center;

    // const touching: Record<number, NFTouchEvent> = {
    //   ...this.touching,
    //   // Take into account also collisions of the fake collider related to this actor
    //   ...(this.get(FakeColliderReferenceComponent)?.reference as DirectionalActor<T>)?.touching,
    // };

    return Object.values(this.touching)
      .filter(filterFn)
      .filter((e) => {
        const vecColl = e.rawActor.collider ?
          e.rawActor.collider.get().center.sub(colliderCenterSelf) :
          e.rawActor.center.sub(colliderCenterSelf);
        const dir = Side.fromDirection(vecColl);
        const isFacing = dir == this.direction;

        if (!isFacing) {
          return false;
        }

        const interactionForcedSides = e.tiledObject?.getProperty<string>(commonObjectProperties.interactionDirections);
        if (interactionForcedSides) {
          const opposite = Side.getOpposite(dir);
          const sides = interactionForcedSides.value
            .split(',')
            .map((v) => getSideFromString(v))
            .filter((s) => s);
          if (!sides.includes(opposite)) {
            return false;
          }
        }

        return true;
      });
  }

  getTouchedAndFacedActors (filterFn: (actor: NFTouchEvent) => boolean, followReference = true): Actor[] {
    return this.getTouchedAndFacedEvents(filterFn)
      .map((e) => followReference ? e.actor : e.rawActor);
  }

  getTouchedAndFacedActor (filterFn: (event: NFTouchEvent) => boolean, followReference = true): Actor | null {
    const touchEvents = this.getTouchedAndFacedEvents(filterFn);

    if (touchEvents.length == 1) {
      return followReference ? touchEvents[0].actor : touchEvents[0].rawActor;
    }

    let minDist = Number.MAX_VALUE;
    return touchEvents.reduce((acc: Actor | null, el) => {
      const dist = this.center.distance(el.rawActor.center);
      if (dist < minDist) {
        minDist = dist;
        return followReference ? el.actor : el.rawActor;
      }
      return acc;
    }, null);
  }

  async showDialog (text: string | string[], options?: DialogOptions): Promise<number> {
    const sprite = this.getStillBottomSpriteForDialog();

    this.trackingDisabled.push(true);
    const choice = await this.lci.dialogBox.show(text, {
      sprite: sprite,
      ...options,
    });

    this.trackingDisabled.pop();
    return choice;
  }

  async showUIPopup (popup: UIPopup, args?: ShowUIPopupArgsDirectionalActor): Promise<void> {
    return showUIPopup({
      engine: this.lci.engine,
      targetActor: this,
      popup,
      ...args,
    });
  }

  async followPath (pathObj: TiledObject, speed = npcSpeedAction, useClosestPoint = true) {
    const firstPoint = vec(pathObj.x, pathObj.y).add(vec(0, -16));

    // Find the closest path point to start forom
    let startIdx = 0;
    if (useClosestPoint) {
      const collCenter = this.collider.get().center.sub(firstPoint);
      startIdx = pathObj.polyline!.reduce((accIdx: number, el, idx, arr) => {
        if (idx == 0) {
          return accIdx;
        }
        const acc = arr[accIdx];
        const dist = vec(el.x, el.y).distance(collCenter);
        if (dist < vec(acc.x, acc.y).distance(collCenter)) {
          return idx;
        }
        return accIdx;
      }, 0);
    }

    for (const tiledPoint of pathObj.polyline!.slice(startIdx)) {
      await this.actions.moveTo(firstPoint.add(vec(tiledPoint.x, tiledPoint.y)), speed).toPromise();
    }
  }

  // addMeetAction (actor: Actor, speed = npcSpeed) {
  //   this.actions
  //     .getQueue()
  //     .add(new NFMeet({
  //       actor: this,
  //       actorToMeet: actor,
  //       speed: speed,
  //     }));
  // }

  getStillBottomSpriteForDialog (): Sprite {
    const key = DirectionalActor.getGraphicCacheKey(graphicKeyStill, Side.Bottom);
    const sprite = this.graphics.getGraphic(key)!.clone() as Sprite;
    sprite.sourceView.height -= 2;
    sprite.sourceView.y += 2;
    return sprite;
  }

  onPreUpdate (_engine: Engine, _delta: number) {
    super.onPreUpdate(_engine, _delta);

    this.z = this.baseZ + this.center.y + 1;

    let graphicsDirection = this.direction;
    let graphicsVel: number = this.vel.size;

    const fixDir = this.fixedDirection.top();
    if (fixDir) {
      graphicsDirection = fixDir;
    } else if (this.trackActor && this.trackingDisabled.top() != true) {
      graphicsDirection = Side.fromDirection(this.trackActor.collider.get().center.sub(this.collider.get().center));
    } else if (this.setDirectionBasedOnVelocity) {

      // Change animation based on velocity
      const switches: Partial<Record<Side, [
        number,
        boolean
      ]>> = {
        [Side.Left]: [this.vel.x, this.vel.x < 0],
        [Side.Right]: [this.vel.x, this.vel.x > 0],
        [Side.Top]: [this.vel.y, this.vel.y < 0],
        [Side.Bottom]: [this.vel.y, this.vel.y > 0],
      };

      const maxAbs: [Side, number] = Object.keys(switches)
        .reduce((acc: [Side, number], key) => {
          const [side, val] = acc;
          const [value, condition] = switches[key as Side]!;
          if (!condition) {
            return acc;
          }

          const abs = Math.abs(value);
          if (abs > val) {
            return [key as Side, abs] as [Side, number];
          }
          return acc;
        }, [graphicsDirection, 0]);

      const [side, vel] = maxAbs;

      graphicsDirection = side;
      graphicsVel = vel;
    }
    let key: string;

    if (graphicsVel > 0) {
      key = DirectionalActor.getGraphicCacheKey(graphicKeyAnimated, graphicsDirection);
    } else {
      const sitting = this.isSitting.top();
      if (sitting) {
        key = DirectionalActor.getGraphicCacheKey(graphicKeySitting, graphicsDirection);
      } else {
        key = DirectionalActor.getGraphicCacheKey(graphicKeyStill, graphicsDirection);
      }
    }
    if (this.graphics.getGraphic(key) == null) {
      key = DirectionalActor.getGraphicCacheKey(graphicKeyStill, graphicsDirection);
    }

    this.graphics.use(key);
    this.direction = graphicsDirection;

    if (graphicsVel > 0 && DirectionalActor.soundWalkPromise == null) {
      const nextPlayIn = sleeper(animationFrameDuration * 3);
      DirectionalActor.soundWalkPromise = Promise.all([
        nextPlayIn,
        commonSounds.walk.play(),
      ])
        .then(() => DirectionalActor.soundWalkPromise = null);
    }
  }

  debugGetDirectionalActorSettings () {
    return {
      fixedDirection: this.fixedDirection.top(),
      direction: this.direction,
      isSitting: this.isSitting.top(),
    };
  }

  protected onComponentAdded (ac: AddedComponent) {
    const component = ac.data.component;
    debugEntity(this, `component added`, component);

    if (component instanceof TiledObjectComponent) {
      const object = component.object;
      // Check if there were any initialization properties
      const initialNPCDirection = object.getProperty<string>(commonObjectProperties.actorDirectionInitial);
      if (initialNPCDirection) {
        const direction = getSideFromString(initialNPCDirection.value);
        if (direction) {
          this.direction = direction;
        }
      }

      const fixActorDirection = object.getProperty<string>(commonObjectProperties.actorDirectionFixed);
      if (fixActorDirection) {
        const direction = getSideFromString(fixActorDirection.value);
        if (direction) {
          this.fixedDirection.push(direction);
        }
      }

      const forceActorStance = object.getProperty<string>(commonObjectProperties.actorStance.key);
      if (forceActorStance) {
        switch (forceActorStance.value) {
          case commonObjectProperties.actorStance.options.sitting:
            this.isSitting.push(true);
            break;
        }
      }

      // Check if the actor cannot be tracked while in the area
      const actorTrackable = object.getProperty<boolean>(commonObjectProperties.actorTrackable);
      if (actorTrackable) {
        this.trackable.push(actorTrackable.value);
      }
    }
  };

  private populateGraphicCache (prefix: string, records: Partial<Record<Side, any>>) {
    Object.keys(records)
      .forEach((side) => {
        const cacheKey = DirectionalActor.getGraphicCacheKey(prefix, side as Side);
        this.graphics.add(cacheKey, records[side as Side]);
      });
  }
}