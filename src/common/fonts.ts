import {LoaderFont} from '../util/loaderFont';
import * as ex from 'excalibur';
import {BaseAlign, TextAlign} from 'excalibur';

export const fontResources = {
  fontM5X7Loader: new LoaderFont('32px m5x7'),
}

export const fonts = {
  mono16: new ex.Font({
    family: 'm5x7',
    size: 16,
    unit: ex.FontUnit.Px,
    baseAlign: BaseAlign.Hanging,
  }),
  mono32: new ex.Font({
    family: 'm5x7',
    size: 32,
    unit: ex.FontUnit.Px,
    baseAlign: BaseAlign.Hanging,
  }),
  mono64: new ex.Font({
    family: 'm5x7',
    size: 64,
    unit: ex.FontUnit.Px,
  }),
  mono64Center: new ex.Font({
    family: 'm5x7',
    size: 64,
    unit: ex.FontUnit.Px,
    textAlign: TextAlign.Center,
  }),
};