import {Loadable} from 'excalibur';
import {debug} from '../system';

export class TriggerPromiseLoadable<T> implements Loadable<T | undefined> {
  private _result?: T;
  private _promise?: Promise<T>;

  get data (): T | undefined {
    return this._result;
  };

  constructor (
    private label: string,
    private getPromise: () => Promise<T>,
  ) {
  }

  private _loaded = false;

  isLoaded (): boolean {
    return this._loaded;
  }

  async load (): Promise<T> {
    this._promise = this.getPromise();
    const result = await this._promise;
    this._result = result;
    this._loaded = true;
    debug(`promise loadable [${this.label}] loaded`);
    return result;
  }

}