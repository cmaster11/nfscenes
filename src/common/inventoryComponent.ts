import {Component} from 'excalibur';
import {TiledObject} from '@excaliburjs/plugin-tiled';
import {commonObjectTypes} from './constants';

export class InventoryObject {
  tiledObject?: TiledObject;

  constructor (
    readonly name: string,
    readonly type: string,
  ) {
  }
}

export class InventoryComponent extends Component<'ex.InventoryComponent'> {
  readonly type = 'ex.InventoryComponent';

  objects: InventoryObject[] = [];

  get keys (): InventoryObject[] {
    return this.objects.filter((o) => o.tiledObject?.type == commonObjectTypes.keycard);
  }

  addTiledObject (o: TiledObject) {
    const inventoryObject = new InventoryObject(o.name ?? 'anonymous', o.type ?? 'untyped');
    inventoryObject.tiledObject = o;
    this.objects.push(inventoryObject);
  }

  addObject (o: InventoryObject) {
    this.objects.push(o);
  }

  removeObject (o: InventoryObject) {
    const idx = this.objects.indexOf(o);
    if (idx < 0) {
      return;
    }
    this.objects.splice(idx, 1);
  }
}