import * as ex from 'excalibur';
import {
  Actor,
  ActorArgs,
  LimitCameraBoundsStrategy,
  LockCameraToActorStrategy,
  Side,
  SpriteSheet,
  vec,
  Vector,
} from 'excalibur';
import {DirectionalActor} from './directionalActor';
import {NPC} from './npc';
import {BaseLevelController} from './baseLevelController';
import {actorPlayerName, commonObjectProperties, commonTags, npcSpeed} from './constants';
import {NFTouchEvent} from './nfTouchEvent';
import {getTiledObjectForActor} from '../util/scene';
import {getSideFromString, getUnitVectorForSide} from '../util/side';
import {showScreenTransition} from './ui';
import {debug} from '../system';
import {cameraZoom} from '../levels/reward/constants';
import {commonSounds} from './sounds';

const playerMovementCases: Partial<Record<ex.Input.Keys, [Side, (a: Actor) => void]>> = {
  [ex.Input.Keys.Left]: [Side.Left, (a) => a.vel.x = -npcSpeed],
  [ex.Input.Keys.A]: [Side.Left, (a) => a.vel.x = -npcSpeed],

  [ex.Input.Keys.Right]: [Side.Right, (a) => a.vel.x = npcSpeed],
  [ex.Input.Keys.D]: [Side.Right, (a) => a.vel.x = npcSpeed],

  [ex.Input.Keys.Up]: [Side.Top, (a) => a.vel.y = -npcSpeed],
  [ex.Input.Keys.W]: [Side.Top, (a) => a.vel.y = -npcSpeed],

  [ex.Input.Keys.Down]: [Side.Bottom, (a) => a.vel.y = npcSpeed],
  [ex.Input.Keys.S]: [Side.Bottom, (a) => a.vel.y = npcSpeed],
};

export abstract class Player<T extends BaseLevelController<T>> extends DirectionalActor<T> {
  protected talkCounter: Record<string, number> = {};

  constructor (lci: T, spriteSheet: SpriteSheet, args?: ActorArgs) {
    super(lci, spriteSheet, {
      name: actorPlayerName,
      collisionType: ex.CollisionType.Active,
      collider: ex.Shape.Box(
        DirectionalActor.colliderWidth - 2,
        DirectionalActor.colliderHeight,
        Vector.Half,
      ),
      // collider: ex.Shape.Circle(DirectionalActor.colliderWidth / 2, Vector.Half),
      ...args,
    });
  }

  // After main update, once per frame execute this code
  onPreUpdate (engine: ex.Engine, delta: number) {
    super.onPreUpdate(engine, delta);

    if (this.actions.getQueue().isComplete()) {
      this.vel = Vector.Zero;
    }

    if (this.canInteract()) {
      if (engine.input.keyboard.wasPressed(ex.Input.Keys.Space)) {
        let actor: Actor | null;
        if ((actor = this.getTouchedAndFacedActor((e) => e.actor instanceof NPC))) {
          this.lci.setActionHandledThisFrame();

          if (this.onNPCInteraction(engine, actor as NPC<T>)) {
            (actor as NPC<T>).direction = Side.getOpposite(this.direction);
          }
          return;
        }

        if ((actor = this.getTouchedAndFacedActor((e) => e.tiledObject?.getProperty<boolean>(commonObjectProperties.pick)?.value == true))) {
          if (this.onPickInteraction(engine, actor)) {
            this.lci.setActionHandledThisFrame();
            return;
          }
        }

        if (this.onInteraction(engine)) {
          this.lci.setActionHandledThisFrame();
          return;
        }
      } else {
        let anyKeyPressed = false;

        Object.keys(playerMovementCases)
          .map((key) => {
            if (!engine.input.keyboard.isHeld(key as ex.Input.Keys)) {
              return;
            }

            const [side, fn] = playerMovementCases[key as ex.Input.Keys]!;
            this.direction = side;
            fn(this);
            anyKeyPressed = true;
          });

        if (anyKeyPressed) {
          const velSize = this.vel.size;
          if (velSize > 0) {
            const velAngle = this.vel.toAngle();
            this.vel = Vector.fromAngle(velAngle).scale(npcSpeed);
          }
        }
      }
    }

  }

  onCollisionStart (touchEvent: NFTouchEvent, touching: Record<number, NFTouchEvent>) {
    super.onCollisionStart(touchEvent, touching);

    if (touchEvent.actor.hasTag(commonTags.fakeCollider)) {
      return;
    }

    const portal = touchEvent.tiledObject?.getProperty(commonObjectProperties.portal)?.value;
    if (portal == null) {
      return;
    }

    // Find the corresponding portal
    const other = this.lci.queryAllPortals.getEntities()
      .find((e) => {
        const tiledObject = getTiledObjectForActor(e as Actor);
        return tiledObject?.getProperty(commonObjectProperties.portal)?.value == portal && tiledObject != touchEvent.tiledObject;
      });
    if (other == null) {
      return;
    }

    // Clear all pending actions
    this.actions.clearActions();

    const otherObject = getTiledObjectForActor(other as Actor);
    const side = getSideFromString(otherObject!.getProperty(commonObjectProperties.portalExit)!.value as string);
    const offset = getUnitVectorForSide(side || Side.None).scaleEqual(16);

    this.lci.setInteractionDisabled(true);

    commonSounds.doorOpen.play()
    showScreenTransition({
      engine: this.lci.engine,
      onTransitionHalfway: () => {
        const actorOther = other as Actor;
        const otherSize = vec(actorOther.width, actorOther.height);
        const otherOffset = actorOther.anchor.scale(otherSize);
        const posOtherWorld = actorOther.pos.sub(otherOffset);

        const thisCollider = this.collider.get();
        const colliderCenterToPos = thisCollider.center.sub(this.pos);
        const colliderTopLeft = colliderCenterToPos.sub(vec(thisCollider.localBounds.width / 2, thisCollider.localBounds.height / 2));
        const colliderCenteringVector = vec((16 - thisCollider.localBounds.width) / 2, (16 - thisCollider.localBounds.height) / 2);

        const newPos = posOtherWorld
          .add(offset)

          //
          .sub(colliderTopLeft)
          .add(colliderCenteringVector);

        // If there are any camera bounds, process them
        const cameraBoundsRef = otherObject!.getProperty<string>(commonObjectProperties.cameraBounds);
        if (cameraBoundsRef) {
          const cameraBoundsObject = this.lci.findCameraBounds(cameraBoundsRef.value);
          if (cameraBoundsObject) {
            this.lci.engine.currentScene.camera.clearAllStrategies();
            this.lci.engine.currentScene.camera.addStrategy(new LockCameraToActorStrategy(this));
            this.lci.engine.currentScene.camera.addStrategy(new LimitCameraBoundsStrategy(cameraBoundsObject.boundingBox));
          }
        }

        debug('moving player via portal', {
          from: touchEvent.actor,
          fromPos: touchEvent.actor.pos.clone(),
          to: other,
          toPos: actorOther.pos.clone(),

          posOtherWorld,
          offset,

          thisCollider,
          colliderCenterToPos,
          colliderTopLeft,
          colliderCenteringVector,
        });

        this.pos = newPos;

        this.lci.engine.currentScene.camera.zoom = otherObject?.getProperty<number>(commonObjectProperties.cameraZoom)?.value || cameraZoom.global;

        this.onPortalTransitionHalfway(touchEvent, actorOther);
      },
      onTransitionEnded: () => {
        window.setTimeout(() => this.lci.setInteractionDisabled(false), 500);
      },
    });
  }

  onPortalTransitionHalfway (touchEvent: NFTouchEvent, to: Actor) {
  }

  // Invoked when we interact with an object that can be picked up

  protected canInteract = () => !(this.lci.interactionDisabled || this.lci.actionHandledThisFrame);

  // Return true if interaction has been handled, and we should turn the npc
  protected abstract onNPCInteraction (engine: ex.Engine, actor: NPC<T>): boolean;

  // Return true if interaction has been handled
  protected abstract onInteraction (engine: ex.Engine): boolean;

  // Return true if interaction has been handled
  protected abstract onPickInteraction (engine: ex.Engine, actor: Actor): boolean;
}