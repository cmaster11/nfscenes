import {Actor, Camera, CameraStrategy, Engine, vec} from 'excalibur';

export class LockCameraToXStrategy implements CameraStrategy<Actor> {
  constructor (public target: Actor, public x: number) {
  }

  public action = (target: Actor, cam: Camera, _eng: Engine, _delta: number) => {
    const currentFocus = cam.getFocus();
    return vec(this.x, currentFocus.y);
  };
}