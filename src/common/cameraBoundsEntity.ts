import {BoundingBox, Entity, TagComponent} from 'excalibur';
import {TiledObject} from '@excaliburjs/plugin-tiled';
import {cameraBoundsKeyDefault, commonObjectProperties, commonTags} from './constants';

export class CameraBoundsEntity extends Entity {
  constructor (readonly tiledObject: TiledObject) {
    super([
      new TagComponent(commonTags.cameraBounds),
    ], tiledObject.name);
  }

  get boundingBox (): BoundingBox {
    const o = this.tiledObject;
    return new BoundingBox(o.x, o.y, o.x + o.width!, o.y + o.height!);
  }
}

export function cameraBoundsObjectMatchesName (o: TiledObject, name: string): boolean {
  return name == cameraBoundsKeyDefault ?
    o.getProperty(commonObjectProperties.cameraBoundsDefault)?.value == true :
    o.name == name;
}