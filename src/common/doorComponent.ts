import {Actor, CollisionType, Component, Entity, Graphic} from 'excalibur';
import {commonObjectProperties} from './constants';
import {InventoryComponent, InventoryObject} from './inventoryComponent';
import {debugEntity} from '../system';

export class DoorComponent extends Component<'ex.DoorComponent'> {
  readonly type = 'ex.DoorComponent';

  constructor (
    readonly name: string,
    readonly closedGraphic: Graphic,
    readonly openGraphic: Graphic,
    isLocked: boolean = false,
  ) {
    super();

    this.isLocked = isLocked;
  }

  private _isOpen = false;

  get isOpen (): boolean {
    return this._isOpen;
  }

  set isOpen (value: boolean) {
    debugEntity(this.owner!, value ? 'door opened' : 'door closed');
    this._isOpen = value;

    (this.owner as Actor)?.graphics.use(value ? this.openGraphic : this.closedGraphic);
  }

  private _isLocked = false;

  get isLocked (): boolean {
    return this._isLocked;
  }

  set isLocked (value: boolean) {
    this._isLocked = value;

    const body = (this.owner as Actor)?.body;
    if (body) {
      // By doing this, the player cannot walk through a door to a portal!
      body.collisionType = value ? CollisionType.Fixed : CollisionType.Passive;
    }
  }

  onAdd (owner: Entity) {
    // Set initial graphics
    this.isOpen = this.isOpen;
    this.isLocked = this.isLocked;
  }

  actorHasInventoryItemToUnlockTheDoor (actor: Actor): InventoryObject | undefined {
    return actor.get(InventoryComponent)?.objects.find(
      (io) => io.tiledObject?.getProperty(commonObjectProperties.unlocksDoor)?.value == this.name);
  }
}