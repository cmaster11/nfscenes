import {Actor, AnimationStrategy, Color, Engine, Graphic, ScreenElement, Sound, Util, vec, Vector} from 'excalibur';
import {Completer} from '../util/completer';
import {debug, registerInWindow} from '../system';
import {getAnimationFromSpriteSheetRow} from '../util/animation';
import {commonSounds} from './sounds';
import {sleeper} from '../util/promise';
import {sprites} from './resources';
import {zScreenTransition, zUI} from './constants';

export enum UIPopup {
  question,
  exclamationBlue,
  exclamationRed,
  pray,
}

const uiSounds: Partial<Record<UIPopup, Sound>> = {
  [UIPopup.pray]: commonSounds.pray,
  [UIPopup.exclamationBlue]: commonSounds.blipExclamation,
  [UIPopup.exclamationRed]: commonSounds.blipExclamation,
  [UIPopup.question]: commonSounds.blipQuestion,
};

registerInWindow('UIPopup', UIPopup);

export type ShowUIPopupArgs = {
  engine: Engine,
  targetActor: Actor,
  popup: UIPopup,
  duration?: number,
  offset?: Vector
}

export function showUIPopup ({duration, engine, offset, popup, targetActor}: ShowUIPopupArgs): Promise<void> {
  duration = duration || 2000;
  offset = offset || vec(0, 8);
  debug('showing ui popup', duration, offset, popup, targetActor);

  const sound = uiSounds[popup];
  if (sound && !sound.isPlaying()) {
    sleeper(200).then(() => sound.play());
  }

  let graphics: Graphic;
  switch (popup) {
    case UIPopup.question:
      graphics = getAnimationFromSpriteSheetRow(sprites.ui, 0, Util.range(0, 3), 100, AnimationStrategy.Freeze);
      break;
    case UIPopup.exclamationBlue:
      graphics = getAnimationFromSpriteSheetRow(sprites.ui, 1, Util.range(0, 3), 100, AnimationStrategy.Freeze);
      break;
    case UIPopup.exclamationRed:
      graphics = getAnimationFromSpriteSheetRow(sprites.ui, 2, Util.range(0, 3), 100, AnimationStrategy.Freeze);
      break;
    case UIPopup.pray:
      graphics = getAnimationFromSpriteSheetRow(sprites.ui, 3, Util.range(0, 3), 100, AnimationStrategy.Freeze);
      break;
  }

  const actor = new Actor({
    z: zUI,
  });

  const targetBounds = targetActor.graphics.localBounds;
  const pos = targetActor.pos.add(vec(targetBounds.left, targetBounds.top));

  actor.pos = pos.add(offset);

  // Jump
  actor.actions
    .moveBy(vec(0, -2), 32)
    .moveBy(vec(0, 2), 16);

  actor.graphics.anchor = vec(0, 1);
  actor.graphics.use(graphics);
  engine.currentScene.add(actor);

  const completer = new Completer<void>();

  window.setTimeout(() => {
    debug('ui popup completed', duration, offset, popup, targetActor);
    engine.currentScene.remove(actor);
    completer.complete();
  }, duration);

  return completer.promise;
}

export type ShowScreenTransitionArgs = {
  engine: Engine,
  delayMs?: number,
  stepMs?: number,
  onTransitionHalfway?: () => void
  onTransitionEnded?: () => void
}

// Creates a screen that transitions to black and back
export function showScreenTransition ({
  engine,
  delayMs,
  stepMs,
  onTransitionHalfway,
  onTransitionEnded,
}: ShowScreenTransitionArgs) {
  delayMs ??= 1000;
  stepMs ??= 50;

  const stepsCount = delayMs / stepMs;

  const actor = new ScreenElement({
    z: zScreenTransition,
    width: engine.screen.resolution.width,
    height: engine.screen.resolution.height,
    color: Color.fromRGB(0, 0, 0, 0),
  });

  let firstHalf = true;
  let value = 0;

  let interval: number;

  // onTransitionHalfway
  const nextStep = () => {
    if (firstHalf) {
      value += 1.0 / (stepsCount / 2);
    } else {
      value -= 1.0 / (stepsCount / 2);
    }

    if (firstHalf) {
      if (value >= 1) {
        firstHalf = false;
        onTransitionHalfway?.call(null);
      }
    } else {
      if (value <= 0) {
        window.clearInterval(interval);
        engine.currentScene.remove(actor);
        onTransitionEnded?.call(null);
        return;
      }
    }

    actor.color = Color.fromRGB(0, 0, 0, value);
  };

  engine.currentScene.add(actor);
  interval = window.setInterval(nextStep, stepMs);
}

registerInWindow('showUIPopup', showUIPopup);