import {debug} from '../system';

export class CompletionCounter {
  private _debugComplete = false;
  private _entries: Record<string, boolean> = {};

  get total (): number {
    return Object.keys(this._entries).length;
  }

  get progress (): number {
    if (this._debugComplete) {
      return 1;
    }
    const total = this.total;
    const done = Object.values(this._entries).filter((b) => b).length;
    return done / total;
  }

  get complete (): boolean {
    if (this._debugComplete) {
      return true;
    }
    return Object.values(this._entries).find((e) => e == false) == null;
  }

  constructor (
    initialKeys: string[] = [],
    private onComplete?: () => void,
  ) {
    initialKeys.forEach((k) => this.addKey(k));
  }

  addKey (key: string) {
    this._entries[key] = false;
  }

  completeEntry (key: string) {
    if (this._entries[key] == undefined) {
      throw new Error('missing key to complete');
    }
    // noinspection PointlessBooleanExpressionJS
    if (this._entries[key] == true) {
      // Already completed
      return;
    }
    debug('completed entry', key);
    this._entries[key] = true;

    if (this.complete) {
      this.onComplete?.();
    }
  }

  completeAllEntries () {
    Object.keys(this._entries).forEach((k) => this.completeEntry(k));
  }

}