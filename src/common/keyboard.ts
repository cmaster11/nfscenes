/**
 * Provides keyboard support for Excalibur.
 */
import * as ex from 'excalibur';
import {debug} from '../system';

export class NFKeyboard extends ex.Input.Keyboard {
  private _nfKeys: ex.Input.Keys[] = [];
  private _nfKeysUp: ex.Input.Keys[] = [];
  private _nfKeysDown: ex.Input.Keys[] = [];

  /**
   * Initialize Keyboard event listeners
   */
  init (global?: GlobalEventHandlers): void {
    if (!global) {
      try {
        // Try and listen to events on top window frame if within an iframe.
        //
        // See https://github.com/excaliburjs/Excalibur/issues/1294
        //
        // Attempt to add an event listener, which triggers a DOMException on
        // cross-origin iframes
        const noop = () => {
          return;
        };
        window.top?.addEventListener('blur', noop);
        window.top?.removeEventListener('blur', noop);

        // this will be the same as window if not embedded within an iframe
        global = window.top as GlobalEventHandlers;
      } catch {
        // fallback to current frame
        global = window;

        debug(
          'Failed to bind to keyboard events to top frame. ' +
          'If you are trying to embed Excalibur in a cross-origin iframe, keyboard events will not fire.',
        );
      }
    }

    global.addEventListener('blur', () => {
      this._nfKeys.length = 0; // empties array efficiently
    });

    // key up is on window because canvas cannot have focus
    global.addEventListener('keyup', (ev: KeyboardEvent) => {
      ev.preventDefault();

      const code = ev.code as ex.Input.Keys;
      const key = this._nfKeys.indexOf(code);
      this._nfKeys.splice(key, 1);
      this._nfKeysUp.push(code);
      const keyEvent = new ex.Input.KeyEvent(code, ev.key, ev);

      // alias the old api, we may want to deprecate this in the future
      this.eventDispatcher.emit('up', keyEvent);
      this.eventDispatcher.emit('release', keyEvent);
    });

    // key down is on window because canvas cannot have focus
    global.addEventListener('keydown', (ev: KeyboardEvent) => {
      ev.preventDefault();

      const code = ev.code as ex.Input.Keys;
      if (this._nfKeys.indexOf(code) === -1) {
        this._nfKeys.push(code);
        this._nfKeysDown.push(code);
        const keyEvent = new ex.Input.KeyEvent(code, ev.key, ev);
        this.eventDispatcher.emit('down', keyEvent);
        this.eventDispatcher.emit('press', keyEvent);
      }
    });
  }

  public update () {
    // Reset keysDown and keysUp after update is complete
    this._nfKeysDown.length = 0;
    this._nfKeysUp.length = 0;

    // Emit synthetic "hold" event
    for (let i = 0; i < this._nfKeys.length; i++) {
      this.eventDispatcher.emit('hold', new ex.Input.KeyEvent(this._nfKeys[i]));
    }
  }

  /**
   * Gets list of keys being pressed down
   */
  public getKeys (): ex.Input.Keys[] {
    return this._nfKeys;
  }

  /**
   * Tests if a certain key was just pressed this frame. This is cleared at the end of the update frame.
   * @param key Test whether a key was just pressed
   */
  public wasPressed (key: ex.Input.Keys): boolean {
    return this._nfKeysDown.indexOf(key) > -1;
  }

  /**
   * Tests if a certain key is held down. This is persisted between frames.
   * @param key  Test whether a key is held down
   */
  public isHeld (key: ex.Input.Keys): boolean {
    return this._nfKeys.indexOf(key) > -1;
  }

  /**
   * Tests if a certain key was just released this frame. This is cleared at the end of the update frame.
   * @param key  Test whether a key was just released
   */
  public wasReleased (key: ex.Input.Keys): boolean {
    return this._nfKeysUp.indexOf(key) > -1;
  }
}
