import * as ex from 'excalibur';
import {ImageFiltering, ImageSource, Loader} from 'excalibur';
import {NFTiledMapResource} from './nfTiledMapResource';
import {TiledObjects} from './tiledObjectGroup';
import {commonObjectProperties, commonObjectTypes} from './constants';

export class NFMapLoader extends Loader {
  npcImageSources: Record<string, ImageSource> = {};
  imageImageSources: Record<string, ImageSource> = {};

  constructor (
    readonly map: NFTiledMapResource,
    readonly npcRequireFn: (name: string) => string,
    readonly imageRequireFn: (name: string) => string,
  ) {
    super();
  }

  getAllObjects (): TiledObjects {
    return new TiledObjects(this.map.data.getExcaliburObjects().map((l) => l.objects).flat());
  }

  async load (): Promise<any> {
    await this.map.load();

    const objects = this.getAllObjects();

    // Load all NPCs
    for (const o of objects.getObjectsByType(commonObjectTypes.npc)) {
      if (o.name == null) {
        throw new Error(`missing npc name for object ${o.id}`);
      }
      const imageSource = new ex.ImageSource(this.npcRequireFn(o.name));
      await imageSource.load();

      this.npcImageSources[o.name] = imageSource;
    }

    // Load all other images
    for (const o of objects.getObjectsByType(commonObjectTypes.image)) {
      if (o.name == null) {
        throw new Error(`missing image name for object ${o.id}`);
      }
      const antialiasing = o.getProperty<boolean>(commonObjectProperties.antialiasing)?.value == true;
      const imageSource = new ex.ImageSource(this.imageRequireFn(o.name), false, antialiasing ? ImageFiltering.Blended : ImageFiltering.Pixel);
      await imageSource.load();

      this.imageImageSources[o.name] = imageSource;
    }
  }
}