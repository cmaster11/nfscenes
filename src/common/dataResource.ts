/**
 * The [[Resource]] type allows games built in Excalibur to load generic resources.
 * For any type of remote resource it is recommended to use [[Resource]] for preloading.
 */
import {Resource} from 'excalibur';
import {Base64Binary} from '../util/base64Binary';
import {debug} from '../system';

const textDecoder = new TextDecoder('utf8');

export class DataResource<T> extends Resource<T> {
  constructor (
    public path: string,
    public responseType: '' | 'arraybuffer' | 'blob' | 'document' | 'json' | 'text',
    public bustCache: boolean = true,
  ) {
    super(path, responseType, bustCache);
  }

  /**
   * Begin loading the resource and returns a promise to be resolved on completion
   */
  public async load (): Promise<T> {
    try {
      const decoded = Base64Binary.decodeArrayBuffer(this.path.substring(this.path.indexOf(';base64,') + 7));

      if (this.responseType == 'text' || this.responseType == 'json') {
        const decodedText = textDecoder.decode(decoded) as any;
        // debug('decoded text', decodedText);
        return decodedText;
      }

      // debug('decoded binary', decoded);
      return decoded as any;
    } catch (e) {
      debug('exception decoding data', e, this.path);
      throw e;
    }
  }
}
