import {Actor, Component} from 'excalibur';

export class ReferenceComponent extends Component<'ex.RefComponent'> {
  readonly type = 'ex.RefComponent';

  constructor (
    private _localReference: Actor,
  ) {
    super();
  }

  get reference (): Actor {
    if (this._localReference.has(ReferenceComponent)) {
      return this._localReference.get(ReferenceComponent)!.reference;
    }

    return this._localReference;
  }
}