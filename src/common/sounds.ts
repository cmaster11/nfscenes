import {Loadable} from 'excalibur';
import {NFSound} from './nfSound';

export const commonSounds = {
  // OK menu_select
  menuSelect: new NFSound(require('../../res/sounds/menu_select.mp3')),

  // OK dirt-04
  walk: new NFSound(require('../../res/sounds/footstep-dirt-04.mp3')),

  // OK dialogue_beep-01-single_beep
  dialogBeep: new NFSound(require('../../res/sounds/dialogue_beep-01-single_beep.mp3')),

  // OK lose
  gameOverLose: new NFSound(require('../../res/sounds/lose.mp3')),

  // OK strange_drone-03
  gameOverUnknownResult: new NFSound(require('../../res/sounds/strange_drone-03.mp3')),

  // OK alarm-loop-4
  alarm: new NFSound(require('../../res/sounds/alarm-loop-4.mp3')),

  // OK going-through-portal-4
  pray: new NFSound(require('../../res/sounds/going-through-portal-4.mp3')),

  // OK blip-4
  blipExclamation: new NFSound(require('../../res/sounds/blip-4.mp3')),

  // OK question
  blipQuestion: new NFSound(require('../../res/sounds/question.mp3')),

  // OK reset
  reset: new NFSound(require('../../res/sounds/reset.mp3')),

  // OK ok
  blipNext: new NFSound(require('../../res/sounds/ok.mp3')),

  // OK ok
  megaphone: new NFSound(require('../../res/sounds/megaphone.mp3')),

  doorOpen: new NFSound(require('../../res/sounds/door_open.mp3')),

  // Game completed
  complete100: new NFSound(require('../../res/sounds/complete-100.mp3')),

  // https://thricedotted.itch.io/looptober-2021-variety-pack
  musicIntro: new NFSound(require('../../res/sounds/ominous128.mp3')),
};

// Fix volumes
commonSounds.alarm.volume = 0.4;
commonSounds.reset.volume = 0.3;

commonSounds.musicIntro.loop = true;
commonSounds.musicIntro.volume = 0.3;

export const commonSoundResources: Loadable<any>[] = Object.values(commonSounds);

window.getIntroSound = () => commonSounds.musicIntro;