import '../../style.css';
import * as ex from 'excalibur';
import {Loadable, Util} from 'excalibur';
import {debug} from '../system';
import {fontResources} from './fonts';

export function importAll (r: any) {
  r.keys().forEach((k: any) => {
    debug('import', k);
    return r(k);
  });
}

// @ts-ignore
importAll(require.context('../../res/common/', true));

export const resources = {
  dialogBox: new ex.ImageSource(require('../../res/common/dialog.png')),
  ui: new ex.ImageSource(require('../../res/common/ui.png')),
  trackingCamera: new ex.ImageSource(require('../../res/common/camera.png')),
};

export const sprites = {
  dialogBox: ex.SpriteSheet.fromImageSource({
    image: resources.dialogBox,
    grid: {
      columns: 5,
      rows: 3,
      spriteWidth: 16,
      spriteHeight: 16,
    },
  }),
  ui: ex.SpriteSheet.fromImageSource({
    image: resources.ui,
    grid: {
      columns: 5,
      rows: 4,
      spriteWidth: 16,
      spriteHeight: 16,
    },
  }),
  trackingCamera: ex.SpriteSheet.fromImageSource({
    image: resources.trackingCamera,
    grid: {
      columns: 5,
      rows: 3,
      spriteWidth: 16,
      spriteHeight: 16,
    },
  }),
};

export const subSprites = {
  star: sprites.ui.getSprite(4, 0)!,
}

export const animations = {
  trackingCameraAlert: Util.range(0, 4).map((i) => new ex.Animation({
    frames: [
      {graphic: sprites.trackingCamera.getSprite(i, 1)!},
      {graphic: sprites.trackingCamera.getSprite(i, 2)!},
    ],
    frameDuration: 250,
  })),
};

export const commonLoaderResources: Loadable<any>[] = [
  ...Object.values(resources),
  ...Object.values(fontResources),
];