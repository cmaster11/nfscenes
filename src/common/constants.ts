import {CollisionGroupManager} from 'excalibur';

export const textLoaderContinueClick = 'Click here to continue...';
export const textLoaderContinueSpace = 'Press SPACE to continue...';

export const collectionTitle = 'The System:';

export const collisionGroupWorld = CollisionGroupManager.create('world');

export const npcSpeed = 64;
export const npcSpeedAction = 32;
export const playerSpeedAction = 48;

export const commonObjectTypes = {
  cameraLock: 'cameraLock',
  trackingCamera: 'trackingCamera',
  npc: 'npc',
  image: 'image',
  player: 'player',
  door: 'door',
  keycard: 'keycard',
  portal: 'portal',
  cameraBounds: 'cameraBounds',
  info: 'info',
  speaker: 'speaker',
  trigger: 'trigger',
};

export const commonObjectNames = {
  levelOut: 'levelOut',
};

export const commonObjectProperties = {
  zClass: {
    key: 'zClass',
    options: {
      ceiling: 'ceiling',
      bg: 'bg',
      hidden: 'hidden',
    },
  },
  zRel: 'zRel',

  portal: 'portal',
  portalExit: 'portalExit',
  doorLocked: 'doorLocked',
  cameraZoom: 'cameraZoom',
  colliderSize: 'colliderSize',
  colliderSkipFake: 'colliderSkipFake',
  CollisionType: 'CollisionType',
  trackActor: 'trackActor',
  trackable: 'trackable',
  scenes: 'scenes',
  cameraBounds: 'cameraBounds',
  cameraBoundsDefault: 'cameraBoundsDefault',
  cameraLockVertical: 'cameraLockVertical',

  actorTrackable: 'actorTrackable',
  actorDirectionFixed: 'actorDirectionFixed',
  actorDirectionInitial: 'actorDirectionInitial',

  refObjectType: 'refObjectType',
  refObjectName: 'refObjectName',

  interactionDirections: 'interactionDirections',
  secret: 'secret',

  npcDirectionPreferred: 'npcDirectionPreferred',
  unlocksDoor: 'unlocksDoor',
  pick: 'pick',
  antialiasing: 'antialiasing',
  actorStance: {
    key: 'actorStance',
    options: {
      sitting: 'sitting',
    },
  },
};

export const dialogKeyDefault = 'default';

export const actorPlayerName = 'player';
export const commonTags = {
  cameraBounds: 'cameraBounds',
  npc: 'npc',
  fakeCollider: 'fakeCollider',
  portal: 'portal',
  trackableActor: 'trackableActor',
};

export const cameraBoundsKeyDefault = 'default';

export const zBg = 100;
export const zObj = 200;
export const zCeiling = 20000;
export const zUI = 400100;
export const zDialog = 500100;
export const zScreenTransition = 800100;
export const zInfo = 900200;