import {TiledObject} from '@excaliburjs/plugin-tiled';

export class TiledObjects {
  constructor (public objects: TiledObject[] = []) {
  }

  public getObjectByType (type: string): TiledObject | undefined {
    return this.getObjectsByType(type)[0];
  }

  public getObjectsByType (type: string): TiledObject[] {
    return this.objects
      .filter(o => o.type?.toLocaleLowerCase() === type.toLocaleLowerCase());
  }

  public getObjectsByTypes (...type: string[]): TiledObject[] {
    const typeLC = type.map((t) => t.toLocaleLowerCase());
    return this.objects
      .filter(o => o.type && typeLC.includes(o.type?.toLocaleLowerCase()));
  }

  public getObjectByName (name: string): TiledObject | undefined {
    return this.getObjectsByName(name)[0];
  }

  public getObjectsByName (name: string): TiledObject[] {
    return this.objects
      .filter(o => o.name?.toLocaleLowerCase() === name.toLocaleLowerCase());
  }
}