import {Interpreter} from 'xstate';
import {Actor, Engine, LimitCameraBoundsStrategy, Query, vec, Vector} from 'excalibur';
import {debug} from '../system';
import {cameraBoundsKeyDefault, commonTags} from './constants';
import {DialogBox} from './dialogBox';
import {Player} from './player';
import {NPC} from './npc';
import {CameraBoundsEntity, cameraBoundsObjectMatchesName} from './cameraBoundsEntity';
import {NFTiledMapResource} from './nfTiledMapResource';
import {DoorComponent} from './doorComponent';
import {getTiledObjectForActor} from '../util/scene';
import {vecLimitByBoundingBox} from '../util/vector';
import {NFLockCameraToActorStrategy} from './nfLockCameraToActorStrategy';

export abstract class BaseLevelController<T extends BaseLevelController<T>> {
  queryAllTrackableActors!: Query;
  queryAllPortals!: Query;
  queryAllNPCs!: Query;
  queryAllCameraBounds!: Query;

  dialogBox!: DialogBox<T>;
  player!: Player<T>;
  npcs!: NPC<T>[];
  doors!: Actor[];
  abstract levelService: Interpreter<any>;

  protected constructor (
    readonly engine: Engine,
    readonly map: NFTiledMapResource,
  ) {
  }

  private _actionHandledThisFrame: boolean = false;

  get actionHandledThisFrame (): boolean {
    return this._actionHandledThisFrame;
  }

  private _interactionDisabled: boolean = false;

  get interactionDisabled (): boolean {
    return this._interactionDisabled;
  }

  run () {
    const engine = this.engine;

    this.queryAllTrackableActors = engine.currentScene.world.queryManager.createQuery([commonTags.trackableActor]);
    this.queryAllPortals = engine.currentScene.world.queryManager.createQuery([commonTags.portal]);
    this.queryAllNPCs = engine.currentScene.world.queryManager.createQuery([commonTags.npc]);
    this.queryAllCameraBounds = engine.currentScene.world.queryManager.createQuery([commonTags.cameraBounds]);

    this.dialogBox = engine.currentScene.actors.find((a) => a instanceof DialogBox)! as DialogBox<T>;
    this.player = engine.currentScene.actors.find((a) => a instanceof Player)! as Player<T>;
    this.npcs = engine.currentScene.actors.filter((a) => a instanceof NPC) as NPC<T>[];
    this.doors = engine.currentScene.actors.filter((a) => a.has(DoorComponent)) as NPC<T>[];
  }

  findNPC (name: string): NPC<T> | undefined {
    return this.queryAllNPCs.getEntities().find((n) => n.name == name)! as NPC<T>;
  }

  findDoor (name: string): Actor | undefined {
    return this.doors.find((a) => a.name == name)! as Actor;
  }

  findByObjectType (type: string, name: string): Actor | undefined {
    return this.engine.currentScene.actors.find((a) => getTiledObjectForActor(a)?.type == type && a.name == name);
  }

  findCameraBounds (name: string): CameraBoundsEntity | undefined {
    return this.queryAllCameraBounds.getEntities()
      .find((n) => cameraBoundsObjectMatchesName((n as CameraBoundsEntity).tiledObject, name))! as CameraBoundsEntity;
  }

  async focusCameraOnActor (actor: Actor, offset = Vector.Zero, duration = 500) {
    const cameraBounds = this.findCameraBounds(cameraBoundsKeyDefault);
    const camera = this.engine.currentScene.camera;

    const targetPos = actor.center.add(offset);

    debug('focusing camera on actor', actor, targetPos, cameraBounds);
    camera.clearAllStrategies();
    if (cameraBounds) {
      camera.addStrategy(new LimitCameraBoundsStrategy(cameraBounds.boundingBox));
      await camera.move(vecLimitByBoundingBox(
        targetPos,
        cameraBounds.boundingBox,
        vec(this.engine.halfDrawWidth, this.engine.halfDrawHeight),
      ), duration);
    } else {
      await camera.move(targetPos, duration);
    }

    camera.clearAllStrategies();
    camera.addStrategy(new NFLockCameraToActorStrategy(actor, offset));
    if (cameraBounds) {
      camera.addStrategy(new LimitCameraBoundsStrategy(cameraBounds.boundingBox));
    }
  }

  setActionHandledThisFrame (value: boolean = true): void {
    this._actionHandledThisFrame = value;
  }

  setInteractionDisabled (value: boolean): void {
    if (value) {
      debug('interaction disabled');
    } else {
      debug('interaction enabled');
    }
    this._interactionDisabled = value;
  }
}