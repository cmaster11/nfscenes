import * as ex from 'excalibur';
import {ActorArgs, AddedComponent, ImageSource, Side, SpriteSheet, Vector} from 'excalibur';
import {DirectionalActor} from './directionalActor';
import {Player} from './player';
import {BaseLevelController} from './baseLevelController';
import {TiledObjectComponent} from '@excaliburjs/plugin-tiled';
import {commonObjectProperties} from './constants';
import {getSideFromString} from '../util/side';
import {NFTouchEvent} from './nfTouchEvent';
import {Stack} from '../util/stack';

export function getNPCSpriteSheet (source: ImageSource): ex.SpriteSheet {
  return ex.SpriteSheet.fromImageSource({
    image: source,
    grid: {
      columns: 24,
      rows: 5,
      spriteWidth: 16,
      spriteHeight: 32,
    },
  });
}

export class NPC<T extends BaseLevelController<T>> extends DirectionalActor<T> {
  preferredDirection = new Stack<Side>({
    debugName: `${this.name || this.id}-preferredDirection`,
  });
  private turnInMs = 0;

  constructor (lci: T, spriteSheet: SpriteSheet, args?: ActorArgs) {
    super(lci, spriteSheet,
      {
        collisionType: ex.CollisionType.Fixed,
        // collider: ex.Shape.Circle(10, ex.vec(0, 2)),
        ...args,
      });
    this.setDirectionBasedOnVelocity = true;

    this.on('collisionstart', (e) => {
      if (e.other instanceof Player) {
        this.actions.getQueue().getActions().find(() => true)?.stop();
      }
    });

    this.resetTurnRandomly();
  }

  // After main update, once per frame execute this code
  onPreUpdate (engine: ex.Engine, delta: number) {
    super.onPreUpdate(engine, delta);

    // If there are possible interactions, do nothing
    if (this.getTouchedAndFacedActor((a) => a.actor instanceof Player)) {
      return;
    }

    const pDir = this.preferredDirection.top();
    if (pDir) {
      if (this.vel.size == 0) {
        this.direction = pDir;
      }
    } else {
      this.turnInMs -= delta;
      if (this.turnInMs <= 0) {
        // Turn randomly
        this.direction = Side.fromDirection(Vector.fromAngle(Math.random() * 2 * Math.PI));
        this.resetTurnRandomly();
      }
    }
  }

  onCollisionStart (touchEvent: NFTouchEvent, touching: Record<number, NFTouchEvent>) {
    super.onCollisionStart(touchEvent, touching);

    const object = touchEvent.tiledObject;

    if (object) {
      // Check if the object is forcing some properties on the NPC
      const preferNPCDirection = object.getProperty<string>(commonObjectProperties.npcDirectionPreferred);
      if (preferNPCDirection) {
        const direction = getSideFromString(preferNPCDirection.value);
        if (direction) {
          this.preferredDirection.push(direction);
        }
      }
    }
  }

  onCollisionEnd (touchEvent: NFTouchEvent, touching: Record<number, NFTouchEvent>) {
    super.onCollisionEnd(touchEvent, touching);

    const object = touchEvent.tiledObject;

    if (object) {
      // Check if the object is forcing some properties on the NPC
      const preferNPCDirection = object.getProperty<string>(commonObjectProperties.npcDirectionPreferred);
      if (preferNPCDirection) {
        this.preferredDirection.pop();
      }
    }
  }

  protected onComponentAdded (ac: AddedComponent) {
    super.onComponentAdded(ac);

    const component = ac.data.component;

    if (component instanceof TiledObjectComponent) {
      const object = component.object;
      // Check if there were any initialization properties

      const preferNPCDirection = object.getProperty<string>(commonObjectProperties.npcDirectionPreferred);
      if (preferNPCDirection) {
        const direction = getSideFromString(preferNPCDirection.value);
        if (direction) {
          this.preferredDirection.push(direction);
        }
      }
    }
  };

  private resetTurnRandomly () {
    this.turnInMs = Math.random() * 6000 + 2000;
  }
}