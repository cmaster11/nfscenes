import {Actor, ActorArgs, Component, Engine, Entity} from 'excalibur';
import {ReferenceComponent} from './referenceComponent';
import {DirectionalActor} from './directionalActor';
import {BaseLevelController} from './baseLevelController';
import {NFTouchEvent} from './nfTouchEvent';

export class FollowColliderPositionActor extends Actor {
  constructor (readonly trackedActor: Actor, args?: ActorArgs) {
    super({
      ...args,
    });
    this.addComponent(new ReferenceComponent(trackedActor));
  }

  onPreUpdate (_engine: Engine, _delta: number) {
    super.onPreUpdate(_engine, _delta);
    this.pos = this.trackedActor.collider.get().center;
  }
}

// This fake collider component is specific to directional actors, so that they can detect
// that there is an interaction ongoing with the player, even if the player is only touching the fake collider
export class FakeColliderForDirectionalActorComponent<T extends BaseLevelController<T>>
  extends Component<'ex.FakeColliderForDirectionalActorComponent'> {
  readonly type = 'ex.FakeColliderForDirectionalActorComponent';

  constructor (readonly reference: DirectionalActor<T>) {
    super();
  }

  onAdd (owner: Entity) {
    const reference = this.reference;

    owner.on('collisionstart', (e) => {
      const id = `fake_${e.other.id}`;
      const evt = new NFTouchEvent(e.other, e.contact);
      reference.touching[id] = evt;
      reference.onCollisionStart(evt, reference.touching);
    });
    owner.on('collisionend', (e) => {
      const id = `fake_${e.other.id}`;
      const evt = reference.touching[id];
      if (evt) {
        delete (reference.touching[id]);
        reference.onCollisionEnd(evt, reference.touching);
      }
    });
  }
}