import {BaseAlign, Color, Engine, Loadable, Loader, TextAlign, Util} from 'excalibur';
import loaderScreen from '../../res/common/pyong2.png';
import {debug} from '../system';
import {collectionTitle, textLoaderContinueClick, textLoaderContinueSpace} from './constants';
import {ExcaliburFontHack} from '../types';
import {fonts} from './fonts';
import {NFSound} from './nfSound';

const textColor = Color.White;

const sounds = {
  // OK ok
  blipNext: new NFSound(require('../../res/sounds/ok.mp3')),
};

export class NFLoader extends Loader {
  fontContinue = fonts.mono32.clone() as unknown as ExcaliburFontHack;
  fontCollectionTitle = fonts.mono32.clone() as unknown as ExcaliburFontHack;
  fontTitle = fonts.mono64.clone() as unknown as ExcaliburFontHack;
  fontLogo = fonts.mono16.clone() as unknown as ExcaliburFontHack;
  continueText = textLoaderContinueSpace;
  private engine!: Engine;
  private _progressCompleted = false;
  private _exitedLoader = false;

  showTextContinue = true;

  constructor (public args: {
    levelTitle: string,
    loadables?: Loadable<any>[],
  }) {
    super(args.loadables);
    this.logo = loaderScreen;

    this.addResources([
      ...Object.values(sounds),
    ]);
  }

  wireEngine (engine: Engine) {
    super.wireEngine(engine);
    this.engine = engine;
  }

  async showPlayButton (): Promise<void> {
    if (this.suppressPlayButton) {
      return;
    }

    this._progressCompleted = true;

    this.triggerMusicIntro();

    const updateFontContinue = () => {
      const documentHasFocus = document.hasFocus();
      this.continueText = documentHasFocus ? textLoaderContinueSpace : textLoaderContinueClick;
    };
    updateFontContinue();

    window.addEventListener('focus', updateFontContinue);
    window.addEventListener('blur', updateFontContinue);

    this.fontContinue.baseAlign = BaseAlign.Middle;
    this.fontContinue.textAlign = TextAlign.Center;

    this.fontCollectionTitle.baseAlign = BaseAlign.Middle;
    this.fontCollectionTitle.textAlign = TextAlign.Center;

    this.fontTitle.baseAlign = BaseAlign.Middle;
    this.fontTitle.textAlign = TextAlign.Center;

    this.fontLogo.baseAlign = BaseAlign.Middle;
    this.fontLogo.textAlign = TextAlign.Right;

    return new Promise((res, rej) => {
      let listenerSpace: any;
      listenerSpace = (evt: KeyboardEvent) => {
        if (evt.code === 'Space') {
          this._exitedLoader = true;

          window.removeEventListener('focus', updateFontContinue);
          window.removeEventListener('blur', updateFontContinue);

          window.getIntroSound?.().stop();

          document.body.removeEventListener('keyup', listenerSpace);
          sounds.blipNext.play();
          res();
        }
      };
      document.body.addEventListener('keyup', listenerSpace);
    });
  }

  triggerMusicIntro () {
    window.getIntroSound?.().once('playbackstart', (evt: any) => {
      debug('sound evt', evt, evt.target?._audioContext.state);
      if (evt.target?._audioContext.state == 'suspended') {
        // Autoplay has been blocked!

        {
          let listenerSound: any;
          listenerSound = () => {
            document.body.removeEventListener('click', listenerSound);
            window.removeEventListener('focus', listenerSound);

            if (!this._exitedLoader) {
              this.triggerMusicIntro();
            }
          };
          document.body.addEventListener('click', listenerSound);
          window.addEventListener('focus', listenerSound);
        }
      }
    });

    // noinspection ES6MissingAwait
    window.getIntroSound?.().play();
  }

  progressFns: (() => [number, number])[] = [];

  addProgressFn (fn: () => [number, number]) {
    this.progressFns.push(fn);
  }

  get combinedProgress (): number {
    // 1 for local progress
    const totalProgresses = 1 +
      this.progressFns
        .map((fn) => fn())
        .reduce((acc, el) => acc + el[1], 0);
    return (this.progress + this.progressFns
      .map((fn) => fn()).reduce((acc, el) => acc + el[0], 0)) / totalProgresses;
  }

  draw (ctx: CanvasRenderingContext2D) {
    const pixelRatio = this.engine.pixelRatio;
    const screenHeight = this.engine.screen.resolution.height;
    const screenWidth = this.engine.screen.resolution.width;

    ctx.save();
    ctx.drawImage(this._image, 0, 0, screenWidth, screenHeight, 0, 0, screenWidth, screenHeight);

    const gradientTitle = ctx.createLinearGradient(0, 0, 0, screenHeight);
    gradientTitle.addColorStop(0, 'rgba(0,0,0,0.1)');
    gradientTitle.addColorStop(1, 'rgba(0,0,0,0.4)');

    ctx.fillStyle = gradientTitle;
    ctx.fillRect(0, 0, screenWidth, screenHeight);

    ctx.restore();

    if (this._progressCompleted) {
      // const revertTxX = screenWidth * pixelRatio
      const revertTxX = -ctx.canvas.width / 2 + screenWidth / 2;
      const revertTxY = -ctx.canvas.height / 2;

      // Collection
      {
        ctx.save();
        // Revert excalibur tx
        // src/engine/Graphics/Font.ts:219
        // ctx.translate(this.padding + ctx.canvas.width / 2, this.padding + ctx.canvas.height / 2);
        ctx.translate(revertTxX, revertTxY);

        ctx.translate(0, screenHeight * 0.2);

        // ctx.strokeRect();

        this.fontCollectionTitle._drawText(ctx, collectionTitle, textColor, this.fontCollectionTitle.size);
        ctx.restore();
      }

      // Title
      {
        ctx.save();
        // Revert excalibur tx
        ctx.translate(revertTxX, revertTxY);

        ctx.translate(0, screenHeight * 0.35);
        const textWidth = this.fontTitle.localBounds.width * 2;
        this.fontTitle._drawText(ctx, this.args.levelTitle, textColor, this.fontTitle.size);
        ctx.restore();
      }

      // Text continue
      if (this.showTextContinue) {
        ctx.save();
        // Revert excalibur tx
        ctx.translate(revertTxX, revertTxY);

        ctx.translate(0, screenHeight * 0.75);
        this.fontContinue._drawText(ctx, this.continueText, textColor, this.fontContinue.size);
        ctx.restore();
      }

      // Logo
      {
        ctx.save();
        // Revert excalibur tx
        ctx.translate(revertTxX, revertTxY);

        ctx.translate(screenWidth / 2 - 16, screenHeight * 0.95);
        this.fontLogo._drawText(ctx, '@8bittake', textColor, this.fontLogo.size);
        //
        ctx.restore();
      }
      return;
    }

    // Progress bar
    {
      ctx.save();
      const progressBarHeight = 60;
      const progressBarLineWidth = 6;
      const progressBarPadding = progressBarLineWidth + 4;
      const progressBarTotalWidth = screenWidth * 0.75;
      const loadingX = (screenWidth - progressBarTotalWidth) / 2;
      const loadingY = (screenHeight * 0.6);

      ctx.lineWidth = progressBarLineWidth;
      Util.DrawUtil.roundRect(ctx, loadingX, loadingY, progressBarTotalWidth, progressBarHeight, 0, Color.White);
      const progress = progressBarTotalWidth * this.combinedProgress;

      const progressWidth = progress - progressBarPadding * 2;
      const height = progressBarHeight - progressBarPadding * 2;
      Util.DrawUtil.roundRect(
        ctx,
        loadingX + progressBarPadding,
        loadingY + progressBarPadding,
        progressWidth > 10 ? progressWidth : 10,
        height,
        0,
        undefined,
        this.loadingBarColor,
      );
      ctx.restore();
    }
  }
}