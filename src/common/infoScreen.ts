import * as ex from 'excalibur';
import {ActorArgs, Canvas, Color, Engine, GraphicsGroup, ScreenElement, Sprite, Text, vec, Vector} from 'excalibur';
import {BaseLevelController} from './baseLevelController';
import {commonSounds} from './sounds';
import {textLoaderContinueClick, textLoaderContinueSpace} from './constants';
import {fonts} from './fonts';
import {CompletionCounter} from './completionCounter';
import {GraphicsGrouping} from 'excalibur/build/dist/Graphics/GraphicsGroup';
import {subSprites} from './resources';

export type InfoScreenLine = {
  text: string,
  offsetY?: number
}

export type InfoScreenConfig<T extends BaseLevelController<T>> = {
  title: string
  lines: InfoScreenLine[]
  showTextContinue?: boolean
  continueLineText?: string
  onDone?: (infoScreen: InfoScreen<T>) => void
  completionCounter?: CompletionCounter
}

export type ExtractInfoScreenLinesOptions = {
  infoScreenTopLineOffsetY: number
  linesDiffBig?: number,
  linesDiffSmall?: number
}

export function extractInfoScreenLines (
  lines: string[],
  options: ExtractInfoScreenLinesOptions,
): InfoScreenLine[] {
  let infoScreenY = options.infoScreenTopLineOffsetY;
  const getInfoScreenYIncrement = (big = true): number => {
    const oldValue = infoScreenY;
    infoScreenY += big ? (options.linesDiffBig || 60) : (options.linesDiffSmall || 40);
    return oldValue;
  };

  return lines
    .reduce((acc: InfoScreenLine[], s: string, idx, arr) => {
      s = s.trim();
      if (s.includes('\n')) {
        const split = s.split('\n');
        acc.push(...split.map((l, idx, splArr): InfoScreenLine => {
          // noinspection RedundantConditionalExpressionJS
          return {
            text: l,
            offsetY: getInfoScreenYIncrement(idx == splArr.length - 1 ? true : false),
          };
        }));
        return acc;
      }

      acc.push({
        text: s,
        offsetY: getInfoScreenYIncrement(),
      });

      return acc;
    }, []);
}

export class InfoScreen<T extends BaseLevelController<T>> extends ScreenElement {
  static soundPlayPromise: Promise<any> | null = null;
  lci: T;
  private bg!: Canvas;
  private textLogo = new Text({
    text: '@8bittake',
    font: fonts.mono32.clone(),
    color: Color.White,
  });
  private textContinue = new Text({
    text: textLoaderContinueSpace,
    font: fonts.mono64Center.clone(),
    color: Color.White,
  });
  private textProgress = new Text({
    text: textLoaderContinueSpace,
    font: fonts.mono32.clone(),
    color: Color.White,
  });
  private completionStar?: Sprite;
  private textTitle;

  constructor (
    lci: T,
    private config: InfoScreenConfig<T>,
    args?: ActorArgs,
  ) {
    super({
      name: 'infoScreen',
      anchor: Vector.Zero,
      ...args,
    });
    this.lci = lci;
    this.textTitle = new Text({
      text: config.title,
      font: fonts.mono32.clone(),
      color: Color.White,
    });
    if (config.completionCounter) {
      this.completionStar = subSprites.star.clone();
      this.completionStar.destSize = {
        width: 32,
        height: 32,
      };
    }

    this.graphics.visible = false;
  }

  updateTexts = () => {
    const documentHasFocus = document.hasFocus();
    this.textContinue.text = documentHasFocus ? (this.config.continueLineText || textLoaderContinueSpace) : textLoaderContinueClick;
    if (this.config.completionCounter) {
      this.textProgress.text = `Progress: ${Math.floor(this.config.completionCounter.progress * 100)}%`;
      if (this.config.completionCounter.complete) {
        this.completionStar!.opacity = 1;
      } else {
        this.completionStar!.opacity = 0;
      }
    }
  };

  onInitialize (_engine: Engine) {
    super.onInitialize(_engine);

    this.bg = new Canvas({
      width: _engine.screen.resolution.width,
      height: _engine.screen.resolution.height,
      cache: true,
      draw: (ctx) => {
        ctx.globalAlpha = 0.66;
        ctx.fillStyle = 'black';
        ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
      },
    });

    const layerBg = this.graphics.layers.create({name: 'background', order: -1});
    layerBg.use(this.bg);

    const graphicsGroupMembers: GraphicsGrouping[] = [
      {
        graphic: this.textTitle,
        pos: vec(8, _engine.screen.resolution.height - 30),
      },
      {
        graphic: this.textLogo,
        pos: vec(_engine.screen.resolution.width - 106, _engine.screen.resolution.height - 30),
      },
      ...this.config.lines.map((line) => {
          return {
            graphic: new Text({
              text: line.text,
              font: fonts.mono64Center.clone(),
              color: Color.White,
            }),
            pos: vec(_engine.screen.resolution.width / 2, _engine.screen.resolution.height / 2 + (line.offsetY || 0)),
          };
        },
      ),
    ];

    if (this.config.completionCounter) {
      graphicsGroupMembers.push({
        graphic: this.textProgress,
        pos: vec(8, 8),
      });
      graphicsGroupMembers.push({
        graphic: this.completionStar!,
        pos: vec(170, 8),
      });
    }

    const infoScreenGraphics = new GraphicsGroup({
      members: graphicsGroupMembers,
    });

    if (this.config.showTextContinue) {
      infoScreenGraphics.members.push({
        graphic: this.textContinue,
        pos: vec(_engine.screen.resolution.width / 2, _engine.screen.resolution.height * 0.8),
      });
    }

    this.graphics.layers.default.use(infoScreenGraphics);
  }

  show () {
    window.addEventListener('focus', this.updateTexts);
    window.addEventListener('blur', this.updateTexts);
    this.updateTexts();

    this.lci.setInteractionDisabled(true);
    this.graphics.visible = true;
  }

  hide () {
    window.removeEventListener('focus', this.updateTexts);
    window.removeEventListener('blur', this.updateTexts);

    this.lci.setInteractionDisabled(false);
    this.graphics.visible = false;
  }

  onPreUpdate (engine: Engine, _delta: number) {
    super.onPreUpdate(engine, _delta);

    if (!this.graphics.visible || this.lci.actionHandledThisFrame) {
      return;
    }

    if (engine.input.keyboard.wasPressed(ex.Input.Keys.Space)) {
      this.lci.setActionHandledThisFrame();

      if (this.config.onDone) {
        if (InfoScreen.soundPlayPromise == null) {
          InfoScreen.soundPlayPromise = commonSounds.blipNext.play()
            .then(() => InfoScreen.soundPlayPromise = null);
        }
        this.config.onDone?.call(null, this);
      }
    }
  }
}