import * as ex from 'excalibur';
import {Actor, ActorArgs, Engine} from 'excalibur';
import {animations, sprites} from './resources';
import {BaseLevelController} from './baseLevelController';
import {DirectionalActor} from './directionalActor';

export enum TrackingCameraLevel {
  normal,
  warning,
  danger,
}

export class TrackingCamera<T extends BaseLevelController<T>> extends Actor {
  lci: T;

  constructor (
    lci: T,
    args: ActorArgs,
  ) {
    super({
      name: 'trackingCamera',
      collisionType: ex.CollisionType.Passive,
      ...args,
    });

    this.lci = lci;
  }

  onInitialize (_engine: Engine) {
    this.graphics.use(sprites.trackingCamera.getSprite(2, 0)!);
  }

  onPostUpdate (_engine: Engine, _delta: number) {
    super.onPostUpdate(_engine, _delta);

    const closestActor = this.lci.queryAllTrackableActors.getEntities()
      .filter((a) => {
        if (a instanceof DirectionalActor) {
          if (!a.trackable.top()) {
            return false;
          }
        }
        return true;
      })
      .reduce((prev, curr) => {
        const diffAbsPrev = Math.sqrt(Math.pow((prev as Actor).pos.x - this.pos.x, 2) + Math.pow((prev as Actor).pos.y - this.pos.y, 2));
        const diffAbsCurr = Math.sqrt(Math.pow((curr as Actor).pos.x - this.pos.x, 2) + Math.pow((curr as Actor).pos.y - this.pos.y, 2));

        if (diffAbsCurr < diffAbsPrev) {
          return curr;
        }
        return prev;
      }) as Actor;

    let cameraLevel = closestActor instanceof DirectionalActor ? closestActor.trackingCameraLevel : TrackingCameraLevel.normal;

    let idx = 0;

    // Pick the sprite to use depending on the player position
    const diffX = (closestActor.pos.x - this.pos.x) / 16;
    if (diffX <= -1.5) {
      idx = 0;
    } else if (diffX <= -0.5) {
      idx = 1;
    } else if (diffX >= 1.5) {
      idx = 4;
    } else if (diffX >= 0.5) {
      idx = 3;
    } else {
      idx = 2;
    }

    // @ts-ignore
    if (cameraLevel == TrackingCameraLevel.danger) {
      this.graphics.use(animations.trackingCameraAlert[idx]);
    } else {
      let row = 0;
      switch (cameraLevel) {
        case TrackingCameraLevel.normal:
          row = 0;
          break;
        // @ts-ignore
        case TrackingCameraLevel.warning:
          row = 1;
          break;
        // @ts-ignore
        case TrackingCameraLevel.danger:
          row = 2;
          break;
      }
      this.graphics.use(sprites.trackingCamera.getSprite(idx, row)!);
    }
  }
}