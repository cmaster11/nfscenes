import {
  parseExternalTsx,
  RawTiledTileset,
  TiledMap,
  TiledMapOptions,
  TiledMapResource,
} from '@excaliburjs/plugin-tiled';
import {ImageSource} from 'excalibur';
import {DataResource} from './dataResource';
import {debug, debugError} from '../system';

export class NFTiledMapResource extends TiledMapResource {

  constructor (path: string, options: TiledMapOptions) {
    super(path, options);
    (this as any)._resource = new DataResource((this as any)._resource.path, 'text');
  }

  public async load (): Promise<TiledMap> {
    const mapData = await (this as any)._resource.load();
    const tiledMap = (await (this as any)._importMapData(mapData)) as TiledMap;
    let externalTilesets: Promise<any>[] = [];

    // Loop through loaded tileset data
    // If we find an image property, then
    // load the image and sprite

    tiledMap.rawMap.tilesets.forEach(ts => {
      // If we find a source property, then
      // load the tileset data, merge it with
      // existing data, and load the image and sprite

      if (ts.source) {
        const type = ts.source.includes('.tsx') ? 'text' : 'json';
        const convertedPath = this.convertPath(this.path, ts.source);
        // debug('loading converted path', ts.source, this.path, convertedPath);
        var tileset = new DataResource<RawTiledTileset>(convertedPath, type);

        externalTilesets.push(tileset.load().then((external: any) => {
          if (type === 'text') {
            external = parseExternalTsx(external, ts.firstgid, ts.source);
          }
          Object.assign(ts, external);
          tiledMap.tileSets.push(external);
        }, () => {
          debugError(`[Tiled] Error loading external tileset file ${tileset.path}`);
        }));
      }
    });

    // Load all tilesets if necessary
    await Promise.all(externalTilesets).then(() => {

      // external images
      let externalImages: Promise<any>[] = [];

      // retrieve images from tilesets and create textures
      tiledMap.rawMap.tilesets.forEach(ts => {
        let tileSetImage = ts.image;
        if (ts.source) {
          // if external tileset "source" is specified and images are relative to external tileset
          tileSetImage = this.convertPath(ts.source, ts.image);
        } else {
          // otherwise for embedded tilesets, images are relative to the tmx (this.path)
          tileSetImage = this.convertPath(this.path, ts.image);
        }
        if (tileSetImage == undefined) {
          throw new Error(`undefined returned for resource [${ts.source}] [${ts.image}]`);
        }
        const tx = new ImageSource(tileSetImage);
        this.imageMap[ts.firstgid] = tx;
        externalImages.push(tx.load());

        debug('[Tiled] Loading associated tileset: ' + ts.image);
      });

      return Promise.all(externalImages).then(() => {
        (this as any)._createTileMap();
      }, () => {
        debugError('[Tiled] Error loading tileset images');
      });
    });

    (this as any)._parseExcaliburInfo();
    return tiledMap;
  }
}