import {Actor, ColliderComponent, CollisionContact} from 'excalibur';
import {TiledObject, TiledObjectComponent} from '@excaliburjs/plugin-tiled';
import {ReferenceComponent} from './referenceComponent';

export class NFTouchEvent {
  actor: Actor;
  collider: ColliderComponent;
  contact: CollisionContact;
  tiledObject: TiledObject | undefined;
  rawActor: Actor;
  rawActorTiledObject: TiledObject | undefined;

  constructor (actor: Actor, contact: CollisionContact) {
    this.actor = actor.has(ReferenceComponent) ? actor.get(ReferenceComponent)!.reference : actor;
    this.collider = (actor.collider ?? (actor as any)._collider) as ColliderComponent;
    this.contact = contact;
    this.tiledObject = this.actor.get(TiledObjectComponent)?.object;
    this.rawActor = actor;
    this.rawActorTiledObject = actor.get(TiledObjectComponent)?.object;
  }
}

// Deduplicate the collision objects, because the player can collide both with
// the original object, and the fake collision one
export function dedupTouchEventsByTiledObject (evts: NFTouchEvent[]): NFTouchEvent[] {
  return evts.reduce((acc: NFTouchEvent[], evt) => {
    if (evt.tiledObject == undefined) {
      acc.push(evt);
      return acc;
    }

    if (acc.find((aEl) => aEl.tiledObject == evt.tiledObject)) {
      return acc;
    }

    acc.push(evt);
    return acc;
  }, []);
}
