import {ExResponse, NativeSoundProcessedEvent, Sound} from 'excalibur';
import {DataResource} from './dataResource';

export class NFSound extends Sound {
  private dataResource: DataResource<any>;

  constructor (dataPath: string) {
    super('sound.mp3');

    this.dataResource = new DataResource(dataPath, ExResponse.type.arraybuffer);
  }

  public async load (): Promise<AudioBuffer> {
    if (this.data) {
      return this.data;
    }
    const arraybuffer = await this.dataResource.load();
    const audiobuffer = await this.decodeAudio(arraybuffer.slice(0));
    (this as any)._duration = typeof audiobuffer === 'object' ? audiobuffer.duration : undefined;
    this.emit('processed', new NativeSoundProcessedEvent(this, audiobuffer));
    return this.data = audiobuffer;
  }
}