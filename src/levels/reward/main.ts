import * as ex from 'excalibur';
import {DisplayMode} from 'excalibur';
import {debug, registerInWindow} from '../../system';
import {NFLoader} from '../../common/loader';
import {IS_DEBUG, levelTitle} from './constants';
import {WaitForLoadable} from '../../common/waitForLoadable';
import {NFKeyboard} from '../../common/keyboard';
import {TriggerPromiseLoadable} from '../../common/triggerPromiseLoadable';

const loader = new NFLoader({
  levelTitle,
});
registerInWindow('loader', loader);

// --- DEVELOPMENT SETTINGS
// Flags.useCanvasGraphicsContext();
loader.suppressPlayButton = IS_DEBUG;
// --- DEVELOPMENT SETTINGS

const engine = new ex.Engine({
  backgroundColor: ex.Color.fromHex('#d2d2d2'),
  width: 640,
  height: 540,
  // Turn off anti-aliasing for pixel art graphics
  antialiasing: false,
  displayMode: DisplayMode.Fixed,
  suppressConsoleBootMessage: true,
  canvasElementId: 'main',
  maxFps: 60,
});

engine.input.keyboard = new NFKeyboard();
engine.input.keyboard.init(window);

loader.addResource(new WaitForLoadable('onEngineLoaded available', () => typeof window.onEngineLoaded == 'function'));
loader.addResource(new TriggerPromiseLoadable('load all resources', async () => {
  debug('load all resources available');
  await (new WaitForLoadable('loadAllResources available', () => typeof window.loadAllResources == 'function')).load();
  if (typeof window.loadAllResourcesStatus == 'function' && typeof window.loadAllResourcesStatusTotal == 'function') {
    loader.addProgressFn(() => [window.loadAllResourcesStatus!(), window.loadAllResourcesStatusTotal!()]);
  }
  debug('triggering load all resources');
  await window.loadAllResources!();
}));

// import (/* webpackPreload: true */ './levelInit')
//   .then(() => debug('imported level init'));

// Start the engine
engine
  .start(loader)
  .then(() => {
    debug('game start');
    window.onEngineLoaded!(engine);
  })
  .catch((err) => debug('loading error', err));

registerInWindow('engine', engine);