// --- DEVELOPMENT SETTINGS
// Flags.useCanvasGraphicsContext();
import {Level, LevelOptions} from './level';
import * as ex from 'excalibur';
import {Engine} from 'excalibur';
import {commonSoundResources, commonSounds} from '../../common/sounds';
import {completionEntries, IS_DEBUG, LevelScene} from './constants';
import {debug} from '../../system';
import {commonLoaderResources} from '../../common/resources';
import {loaderResources} from './resources';
import {CompletionCounter} from '../../common/completionCounter';

// --- DEVELOPMENT SETTINGS
const levelOptionShowInfoScreen = !IS_DEBUG;
// engine.toggleDebug();
const showIntros = !IS_DEBUG;
const customAfterStartScript = IS_DEBUG;

const sceneKey = 'scene';
let part2Reached = false;

const resources = [
  ...commonSoundResources,
  ...commonLoaderResources,
  ...loaderResources,
];

const completionCounter = new CompletionCounter(
  Object.keys(completionEntries),
  () => commonSounds.complete100.play(),
);

//

const resourcesToLoad = resources.length;
let resourcesLoadedCounter = 0;
window.loadAllResources = () => Promise
  .all(resources.map((r) => {
    return r.load()
      .then(() => {
        resourcesLoadedCounter++;
        return debug('loaded', r);
      })
      .catch((ex) => debug('failed to load resource', r, ex));
  }));
window.loadAllResourcesStatus = () => resourcesLoadedCounter;
window.loadAllResourcesStatusTotal = () => resourcesToLoad;

const startScript = (engine: Engine) => {
  initLevelPart1(engine);
  // initLevelPart2(engine);
};

const afterInit = customAfterStartScript ? (level: Level) => {
  (async () => {
    // await sleeper(250)
    // level.levelController.levelService.send(LevelMachineStatePart2Events.playerFoundTheKey);
    //
    // await sleeper(250)
    // level.levelController.levelService.send(LevelMachineStatePart2Events.playerOpenedABarrel);
    //
    // await sleeper(250)
    // level.levelController.levelService.send(LevelMachineStatePart2Events.playerGaveKeyToGuard);

    // await sleeper(250)
    // level.levelController.levelService.send(LevelMachineStatePart2Events.playerTalkedWithFamily);
  })();

  // const doorGuardhouse = level.levelController.findDoor('doorGuardhouse')?.get(DoorComponent);
  // if (doorGuardhouse) {
  //   doorGuardhouse.isLocked = false;
  // }
  // level.levelController.player.pos = vec(466, 128);

} : () => {
};

// --- DEVELOPMENT SETTINGS

export function initLevelPart1 (engine: Engine, noInfo: boolean = false) {
  const levelOptions: LevelOptions = {
    completionCounter,
    showInfoScreen: noInfo ? false : levelOptionShowInfoScreen,
    showIntros: showIntros,
    scene: LevelScene.part1,
  };

  const level = new Level(levelOptions);

  engine.remove(sceneKey);
  engine.add(sceneKey, level);
  engine.goToScene(sceneKey);
}

export function initLevelPart2 (engine: Engine): Level {
  part2Reached = true;

  const levelOptions: LevelOptions = {
    completionCounter,
    showInfoScreen: false,
    showIntros: showIntros,
    scene: LevelScene.part2,
    afterInit: afterInit,
  };

  const level = new Level(levelOptions);

  engine.remove(sceneKey);
  engine.add(sceneKey, level);
  engine.goToScene(sceneKey);

  return level;
}

window.onEngineLoaded = (engine: Engine) => {

  engine.on('preupdate', (e) => {
    if (engine.input.keyboard.wasPressed(ex.Input.Keys.R)) {
      commonSounds.reset.play();

      if (engine.input.keyboard.isHeld(ex.Input.Keys.Key9)) {
        part2Reached = false;
      } else if (engine.input.keyboard.isHeld(ex.Input.Keys.Key0)) {
        part2Reached = true;
      }

      if (part2Reached) {
        initLevelPart2(engine);
      } else {
        initLevelPart1(engine, true);
      }

    }
  });

  startScript(engine);
};

debug('loaded level init');

