import * as ex from 'excalibur';
import {
  Actor,
  BodyComponent,
  ColliderComponent,
  CollisionType,
  Engine,
  LimitCameraBoundsStrategy,
  LockCameraToActorStrategy,
  Scene,
  Side,
  Sprite,
  vec,
  Vector,
} from 'excalibur';
import {resources, sprites, subSprites} from './resources';
import {getNPCSpriteSheet, NPC} from '../../common/npc';
import {TiledLayerComponent, TiledObject, TiledObjectComponent} from '@excaliburjs/plugin-tiled';
import {DialogBox} from '../../common/dialogBox';
import {Controller, ControllerOptions} from './controller';
import {extractInfoScreenLines, InfoScreen} from '../../common/infoScreen';
import {fixTileMapVector, fixTileMapVectorForNPC} from '../../util/tileMap';
import {
  cameraZoom,
  dialogs,
  infoScreenCapturedName,
  infoScreenControlsName,
  infoScreenHelpedGuardName,
  infoScreenParadise,
  layerNames,
  LevelScene,
  levelTitle,
  objectTypes,
} from './constants';
import {debug, registerInWindow} from '../../system';
import {LevelPlayer} from './player';
import {
  collectionTitle,
  collisionGroupWorld,
  commonObjectProperties,
  commonObjectTypes,
  commonTags,
  zBg,
  zCeiling,
  zDialog,
  zInfo,
  zObj,
} from '../../common/constants';
import {getActorForTiledObject, getTiledObjectForActor} from '../../util/scene';
import {ReferenceComponent} from '../../common/referenceComponent';
import {getSideFromString} from '../../util/side';
import {DoorComponent} from '../../common/doorComponent';
import {TrackingCamera} from '../../common/trackingCamera';
import {getColliderBoxFromColliderSizeStringForTileMapObject} from '../../util/collider';
import {DirectionalActor} from '../../common/directionalActor';
import {
  FakeColliderForDirectionalActorComponent,
  FollowColliderPositionActor,
} from '../../common/followColliderPositionActor';
import {CameraBoundsEntity, cameraBoundsObjectMatchesName} from '../../common/cameraBoundsEntity';
import {LockCameraToXStrategy} from '../../common/cameraStrategy';
import {initLevelPart2} from './levelInit';

const circleColliderPadding = 4;
const infoScreenTopLineOffsetY = -180;

export type LevelOptions = ControllerOptions & {
  showInfoScreen: boolean
  afterInit?: (level: Level) => void
}

export class Level extends ex.Scene {
  levelController!: Controller;

  constructor (public opts: LevelOptions) {
    super();
  }

  isFirstInfoScreen = this.opts.showInfoScreen;

  onInitialize (engine: ex.Engine) {
    const {map, npcImageSources, imageImageSources} = resources.mapLoader;
    const objects = resources.mapLoader.getAllObjects();

    const levelController = new Controller(engine, map, this.opts);
    this.levelController = levelController;
    registerInWindow('levelController', levelController);
    registerInWindow('currentScene', engine.currentScene);
    const fullTitle = `${collectionTitle} ${levelTitle}`;

    // Controls info screen
    {
      const infoScreen = new InfoScreen(levelController, {
        completionCounter: this.opts.completionCounter,
        title: fullTitle,
        lines: extractInfoScreenLines(dialogs[infoScreenControlsName], {
          infoScreenTopLineOffsetY,
        }),
        showTextContinue: true,
        onDone: (infoScreen) => {
          infoScreen.hide();

          if (this.isFirstInfoScreen) {
            this.isFirstInfoScreen = false;
            this.levelController.afterInfoScreen?.();
          }
        },
      }, {
        name: infoScreenControlsName,
        z: zInfo,
      });
      engine.currentScene.add(infoScreen);
    }

    // infoScreenParadise
    {
      const infoScreen = new InfoScreen(levelController, {
        completionCounter: this.opts.completionCounter,
        title: fullTitle,
        lines: extractInfoScreenLines(dialogs[infoScreenParadise], {
          infoScreenTopLineOffsetY,
          linesDiffBig: 70,
        }),
        showTextContinue: true,
        onDone: infoScreen1 => initLevelPart2(engine),
      }, {
        name: infoScreenParadise,
        z: zInfo,
      });
      engine.currentScene.add(infoScreen);
    }

    // Help guard screen
    {
      const lines: string[] = dialogs[infoScreenHelpedGuardName];
      const infoScreen = new InfoScreen(levelController, {
        completionCounter: this.opts.completionCounter,
        title: fullTitle,
        lines: extractInfoScreenLines(lines.slice(0, lines.length - 1), {
          infoScreenTopLineOffsetY,
          linesDiffBig: 70,
        }),
        showTextContinue: true,
        continueLineText: lines[lines.length - 1],
      }, {
        name: infoScreenHelpedGuardName,
        z: zInfo,
      });
      engine.currentScene.add(infoScreen);
    }

    // Lose screen
    {
      const lines: string[] = dialogs[infoScreenCapturedName];
      const infoScreen = new InfoScreen(levelController, {
        completionCounter: this.opts.completionCounter,
        title: fullTitle,
        lines: extractInfoScreenLines(lines.slice(0, lines.length - 1), {
          infoScreenTopLineOffsetY,
          linesDiffBig: 70,
        }),
        showTextContinue: true,
        continueLineText: lines[lines.length - 1],
      }, {
        name: infoScreenCapturedName,
        z: zInfo,
      });
      engine.currentScene.add(infoScreen);
    }

    registerInWindow('map', map);
    map.addTiledMapToScene(this);

    // Clear all actors which do NOT belong to this scene
    {
      const toDelete: TiledObject[] = [];
      // First of all detect all objects that belong to a layer dedicated to another scene
      const objectsDedicatedToAnotherScene = this.opts.scene == LevelScene.part1 ?
        map.data.getObjectLayerByName(layerNames.objectsPart2).objects :
        map.data.getObjectLayerByName(layerNames.objectsPart1).objects;

      toDelete.push(...objectsDedicatedToAnotherScene);
      toDelete.push(...objects
        .objects.filter((o) => {
          const prop = o.getProperty<string>(commonObjectProperties.scenes)?.value;
          if (prop == null) {
            return false;
          }

          const scenes = prop.split(',');
          scenes.forEach((scene) => {
            if ((LevelScene as any)[scene] == null) {
              throw new Error(`invalid scene for object ${o.name}`);
            }
          });

          return !scenes.includes(this.opts.scene as string);
        }));

      toDelete.forEach((o) => {
        const act = getActorForTiledObject(engine.currentScene, o);
        objects.objects.splice(objects.objects.indexOf(o), 1);
        if (act) {
          engine.currentScene.world.remove(act, false);
        }
      });
    }

    // Make solid elements of the map not collide with each other
    this.tileMaps.forEach((tm) => {
      tm.getComponents()
        .filter((c) => c instanceof BodyComponent)
        .forEach((b) => {
          (b as BodyComponent).group = collisionGroupWorld;
        });
    });

    // engine.showDebug(true);
    engine.currentScene.camera.zoom = cameraZoom.global;

    // Initialize all tracking cameras
    objects.getObjectsByType(commonObjectTypes.trackingCamera)
      .forEach((o) => {
        // Delete the old tile
        engine.currentScene.actors
          .filter((a) => a.get(TiledObjectComponent)?.object.rawObject == o.rawObject)
          .forEach((a) => engine.remove(a));

        const actor = new TrackingCamera(levelController, {
          collisionGroup: collisionGroupWorld,
          collider: ex.Shape.Circle(16 / 2 + circleColliderPadding),
          z: zObj,
          name: o.rawObject.name,
        });
        // If the collision happens on a corner, peace and love
        actor.pos = fixTileMapVector(vec(o.x, o.y));
        actor.addComponent(new TiledObjectComponent(o));
        engine.currentScene.add(actor);
      });

    // Init barrels
    objects.getObjectsByTypes(objectTypes.barrel)
      .map((o) => getActorForTiledObject(engine.currentScene, o))
      .filter((a) => a)
      .forEach((actor) => {
        const bc = actor!.get(BodyComponent)!;
        bc.group = collisionGroupWorld;
        bc.collisionType = CollisionType.Fixed;
      });

    // Init all camera bounds
    let cameraBoundsGlobalEntity: CameraBoundsEntity | undefined;
    objects.getObjectsByType(commonObjectTypes.cameraBounds)
      .forEach((o) => {
        const entity = new CameraBoundsEntity(o);
        engine.currentScene.add(entity);
        if (o.getProperty(commonObjectProperties.cameraBoundsDefault)?.value == true) {
          cameraBoundsGlobalEntity = entity;
        }
      });

    // Mark all portals
    objects
      .getObjectsByType(commonObjectTypes.portal)
      .forEach((o) => {
        if (o.getProperty(commonObjectProperties.portal) == null) {
          return;
        }

        // Check if the portal has an invalid camera bounds reference
        const cameraBoundsRef = o.getProperty<string>(commonObjectProperties.cameraBounds);
        if (cameraBoundsRef) {
          const cameraBoundsObject = objects.getObjectsByType(commonObjectTypes.cameraBounds)
            .find((co) => cameraBoundsObjectMatchesName(co, cameraBoundsRef.value));
          if (cameraBoundsObject == null) {
            throw new Error(`missing camera bounds object ${cameraBoundsRef.value} referenced by portal ${o.name}`);
          }
        }

        let actor = getActorForTiledObject(engine.currentScene, o);
        if (actor == null) {
          // If the portal came from a shape, add a fake actor
          actor = new Actor({
            width: o.width,
            height: o.height,
            collisionType: CollisionType.Fixed,
            collisionGroup: collisionGroupWorld,
            name: `collider_${o.name}`,
            anchor: vec(0, 0),
          });
          actor.pos = vec(o.x, o.y);
          actor.addComponent(new TiledObjectComponent(o));
          engine.currentScene.add(actor);
        }
        actor.addTag(commonTags.portal);
        const portalExit = o.getProperty(commonObjectProperties.portalExit)?.value as string;
        if (portalExit == null) {
          throw new Error(`missing portalExit for object ${o.name}`);
        }

        const side = getSideFromString(portalExit);
        if (side == undefined || side == Side.None) {
          throw new Error(`bad portalExit ${portalExit} for object ${o.name}`);
        }
      });

    // Init all doors
    objects
      .getObjectsByType(commonObjectTypes.door)
      .forEach((o) => {
        let actor = getActorForTiledObject(engine.currentScene, o);
        if (actor == null) {
          throw new Error(`missing actor for door ${o.name}`);
        }

        if (o.name == null) {
          throw new Error(`missing door name for ${o.id}`);
        }

        actor.addComponent(new DoorComponent(o.name!, subSprites.doorClosed, subSprites.doorOpen,
          o.getProperty<boolean>(commonObjectProperties.doorLocked)?.value || false));
      });

    // Init all triggers
    objects
      .getObjectsByType(commonObjectTypes.trigger)
      .forEach((o) => {
        const actor = new Actor({
          width: o.width,
          height: o.height,
          collisionType: (o.getProperty<string>(commonObjectProperties.CollisionType)?.value as CollisionType) || CollisionType.Passive,
          collisionGroup: collisionGroupWorld,
          name: `trigger_${o.name}`,
          anchor: vec(0, 0),
        });
        actor.pos = vec(o.x, o.y);
        actor.addComponent(new TiledObjectComponent(o));
        engine.currentScene.add(actor);
      });

    // images
    {
      objects.getObjectsByType(commonObjectTypes.image)
        .forEach((imageObject) => {
          const image = new Actor({
            name: imageObject.name,
            anchor: Vector.Zero,
            height: imageObject.height,
            width: imageObject.width,
          });
          image.graphics.use(new Sprite({
            image: imageImageSources[imageObject.name!],
            destSize: {
              height: imageObject.height!,
              width: imageObject.width!,
            },
          }));
          image.pos = new ex.Vector(imageObject.x, imageObject.y);
          image.addComponent(new TiledObjectComponent(imageObject));
          engine.currentScene.add(image);
        });
    }

    // NPCs
    {
      objects.getObjectsByType(commonObjectTypes.npc)
        .forEach((npcObject) => {
          const placeholderActor = getActorForTiledObject(engine.currentScene, npcObject);

          const npc = new NPC(levelController, getNPCSpriteSheet(npcImageSources[npcObject.name!]), {
            name: npcObject.name,
          });
          npc.baseZ = zObj;
          npc.pos = fixTileMapVectorForNPC(new ex.Vector(npcObject.x, npcObject.y));
          npc.addComponent(new TiledObjectComponent(npcObject));
          npc.addTag(commonTags.npc);

          if (npcObject.getProperty(commonObjectProperties.trackable)?.value != false) {
            npc.addTag(commonTags.trackableActor);
          }

          engine.currentScene.add(npc);
          if (placeholderActor) {
            debug('removing placeholder npc', placeholderActor);
            engine.currentScene.world.remove(placeholderActor, false);
          }
        });
    }

    // Player
    {
      const player = new LevelPlayer(levelController, this.opts.scene == LevelScene.part1 ? sprites.playerPart1 : sprites.playerPart2);

      const playerObject = objects.getObjectsByType(commonObjectTypes.player)[0];

      let placeholderActor: Actor | undefined;
      if (playerObject != null) {
        placeholderActor = getActorForTiledObject(engine.currentScene, playerObject);
        player.pos = fixTileMapVectorForNPC(new ex.Vector(playerObject.x, playerObject.y));
        player.addComponent(new TiledObjectComponent(playerObject));
      }
      player.baseZ = zObj;

      player.addTag(commonTags.trackableActor);

      engine.currentScene.add(player);
      registerInWindow('player', player);

      if (placeholderActor) {
        debug('removing placeholder player', placeholderActor);
        engine.currentScene.world.remove(placeholderActor, false);
      }

      // Add camera bounds
      {
        // TODO (2022-01-26 - cmaster11):
        // TODO (2022-01-26 - cmaster11): DEDUP portal code
        // TODO (2022-01-26 - cmaster11):
        // TODO (2022-01-26 - cmaster11):
        engine.currentScene.camera.addStrategy(new LockCameraToActorStrategy(player));

        const cameraLockObject = objects.getObjectsByType(commonObjectTypes.cameraLock).at(0);
        if (cameraLockObject) {
          if (cameraLockObject.getProperty(commonObjectProperties.cameraLockVertical)?.value == true) {
            engine.currentScene.camera.addStrategy(new LockCameraToXStrategy(player, cameraLockObject.x));
          }
        }

        if (cameraBoundsGlobalEntity) {
          engine.currentScene.camera.addStrategy(new LimitCameraBoundsStrategy(cameraBoundsGlobalEntity.boundingBox));
        }
      }
    }

    // Set up NPCs tracking
    {
      const directionalActors = engine.currentScene.actors
        .filter((a) => a instanceof DirectionalActor) as DirectionalActor<Controller>[];

      directionalActors
        .forEach((a) => {
          let trackActor: string | undefined;
          if ((trackActor = getTiledObjectForActor(a)!.getProperty<string>(commonObjectProperties.trackActor)?.value)) {
            const other = engine.currentScene.actors.find((n2) => getTiledObjectForActor(n2)?.name == trackActor);
            if (other == null) {
              throw new Error(`untrackable actor ${trackActor} for ${a.name}`);
            }

            a.trackActor = other;
          }
        });
    }

    // Fix all z
    {
      let objCounter = 0;
      objects.objects
        .forEach((o) => {
          const actor = getActorForTiledObject(engine.currentScene, o);
          if (actor == null) {
            return;
          }

          if (actor.name == 'anonymous') {
            // @ts-ignore
            actor._setName((o.name || o.type || 'anonymous') + `_${objCounter++}`);
          }

          const zRel = o.getProperty<number>(commonObjectProperties.zRel)?.value || 0;

          switch (o.getProperty(commonObjectProperties.zClass.key)?.value) {
            case commonObjectProperties.zClass.options.ceiling:
              actor.z = zCeiling + zRel + 1;
              break;
            case commonObjectProperties.zClass.options.bg:
              actor.z = zBg + zRel + 1;
              break;
            case commonObjectProperties.zClass.options.hidden:
              actor.visible = false;
              break;
            default:
              actor.z = zObj + zRel + actor.pos.y - 16;
          }

        });

      // Fix all ceiling tiles
      const ceiling = map.getTileMapLayers()
        .find((l) => (l.getComponents()
          .find((c) => c instanceof TiledLayerComponent) as (TiledLayerComponent | null))?.layer?.name == layerNames.ceiling);
      if (ceiling) {
        ceiling.z = zCeiling;
      }
    }

    // Improve all collisions
    objects
      .objects.forEach((o) => {
      const actor = getActorForTiledObject(engine.currentScene, o);
      if (actor == null) {
        return;
      }

      // Do not improve portals, because that may trigger chain of events
      if (actor.hasTag(commonTags.portal)) {
        return;
      }

      const bc = actor!.get(BodyComponent);
      if (bc == null) {
        return;
      }

      if (actor instanceof LevelPlayer) {
      } else {
        bc.group = collisionGroupWorld;
      }
      const cc = actor!.get(ColliderComponent);
      if (cc == null) {
        return;
      }

      // Fix collider shape
      const colliderSize = o.getProperty<string>(commonObjectProperties.colliderSize);
      if (colliderSize) {
        // Reset collider size
        const bb = getColliderBoxFromColliderSizeStringForTileMapObject(colliderSize.value);
        // debug(`initializing new collider by colliderSize`, bb.clone(), actor);
        cc.set(bb);
      }

      if (bc.collisionType == CollisionType.Fixed &&
        (o.getProperty<boolean>(commonObjectProperties.colliderSkipFake)?.value != true)) {
        /*
         If this object has a FIXED collision, also set up a transparent circular
         passive collision to improve the overall collision experience.
         */
        const collisionActor = new FollowColliderPositionActor(actor, {
          collisionType: CollisionType.Passive,
          collisionGroup: bc.group,
          collider: ex.Shape.Circle(16 / 2 + circleColliderPadding),
          z: zObj,
          name: (o.rawObject.name || o.rawObject.type) + '_fakeCollider',
        });
        // If the collision happens on a corner, peace and love
        collisionActor.addTag(commonTags.fakeCollider);
        collisionActor.addComponent(new TiledObjectComponent(o));

        if (actor instanceof DirectionalActor) {
          collisionActor.addComponent(new FakeColliderForDirectionalActorComponent(actor));
        }
        engine.currentScene.add(collisionActor);
      }
    });

    // Init all references
    objects
      .objects
      .forEach((o) => {
        let actor = getActorForTiledObject(engine.currentScene, o);
        if (actor == null) {
          return;
        }

        const refType = o.getProperty<string>(commonObjectProperties.refObjectType);
        const refName = o.getProperty<string>(commonObjectProperties.refObjectName);
        if (refType && refName) {
          const refActor = engine.currentScene.actors
            .find((a) => getTiledObjectForActor(a)?.type == refType.value && a.name == refName.value);
          if (refActor) {
            actor.addComponent(new ReferenceComponent(refActor));
          }
        }
      });

    const dialogBox = new DialogBox(levelController, {
      z: zDialog,
    });
    engine.currentScene.add(dialogBox);
    registerInWindow('dialogBox', dialogBox);
  }

  onActivate (_oldScene: Scene, _newScene: Scene) {
    super.onActivate(_oldScene, _newScene);
    this.levelController.run();

    if (this.opts.showInfoScreen) {
      this.levelController.infoScreenControls?.show();
    } else {
      this.levelController.afterInfoScreen?.();
    }

    this.opts?.afterInit?.(this);
  }

  onPreUpdate (engine: Engine, _delta: number) {
    super.onPreUpdate(engine, _delta);

    const hadActionHandledThisFrame = this.levelController.actionHandledThisFrame;

    this.levelController.setActionHandledThisFrame(false);

    if (hadActionHandledThisFrame || this.levelController.interactionDisabled) {
      return;
    }

    if (engine.input.keyboard.wasPressed(ex.Input.Keys.Esc)) {
      this.levelController.setActionHandledThisFrame();
      this.levelController.infoScreenControls?.show();
    }
  }
}