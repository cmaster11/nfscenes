import {Axis, CollisionType, Engine, Side, vec} from 'excalibur';
import {BaseLevelController} from '../../common/baseLevelController';
import {InfoScreen} from '../../common/infoScreen';
import {interpret, Interpreter, StateValueMap} from 'xstate';
import {
  completionEntries,
  dialogs,
  doors,
  infoScreenCapturedName,
  infoScreenControlsName,
  infoScreenHelpedGuardName,
  infoScreenParadise,
  layerNames,
  LevelScene,
  npcs,
} from './constants';
import {debug} from '../../system';
import {
  levelMachinePart1,
  levelMachinePart2,
  LevelMachineStatePart1,
  LevelMachineStatePart2,
  LevelMachineStatePart2Events,
  LevelMachineStatePart2Keycard,
} from './state';
import {commonObjectTypes, npcSpeedAction, playerSpeedAction} from '../../common/constants';
import {UIPopup} from '../../common/ui';
import {getStateKey} from '../../util/state';
import {NFTiledMapResource} from '../../common/nfTiledMapResource';
import {DoorComponent} from '../../common/doorComponent';
import {getFirstSpriteInActor} from '../../util/sprite';
import {sleeper} from '../../util/promise';
import {getTiledObjectForActor} from '../../util/scene';
import {TrackingCameraLevel} from '../../common/trackingCamera';
import {commonSounds} from '../../common/sounds';
import {NFMeet} from '../../common/actionNFMeet';
import {CompletionCounter} from '../../common/completionCounter';

export type ControllerOptions = {
  completionCounter: CompletionCounter,
  scene: LevelScene
  showIntros: boolean
}

export class Controller extends BaseLevelController<Controller> {
  levelService!: Interpreter<any>;

  infoScreenControls?: InfoScreen<Controller>;
  infoScreenParadise?: InfoScreen<Controller>;
  infoScreenHelpedGuard?: InfoScreen<Controller>;
  infoScreenCaptured?: InfoScreen<Controller>;

  constructor (engine: Engine, map: NFTiledMapResource, readonly opts: ControllerOptions) {
    super(engine, map);
  }

  run () {
    super.run();

    const engine = this.engine;

    this.infoScreenControls = engine.currentScene.actors.find((a) => a instanceof InfoScreen && a.name == infoScreenControlsName) as InfoScreen<Controller>;
    this.infoScreenParadise = engine.currentScene.actors.find((a) => a instanceof InfoScreen && a.name == infoScreenParadise) as InfoScreen<Controller>;
    this.infoScreenHelpedGuard = engine.currentScene.actors.find((a) => a instanceof InfoScreen && a.name == infoScreenHelpedGuardName) as InfoScreen<Controller>;
    this.infoScreenCaptured = engine.currentScene.actors.find((a) => a instanceof InfoScreen && a.name == infoScreenCapturedName) as InfoScreen<Controller>;

    switch (this.opts.scene) {
      case LevelScene.part1:
        this.initScenePart1();
        break;
      case LevelScene.part2:
        this.initScenePart2();
        break;
    }
  }

  findActorByID (id: number) {
    return this.engine.currentScene.actors.find((a) => {
      const o = getTiledObjectForActor(a);
      return o?.id == id;
    });
  }

  afterInfoScreen?: () => Promise<void>;

  initScenePart1 () {
    this.levelService = interpret(levelMachinePart1)
      .onTransition(async (state) => {
        debug('[part1] new state', state.value);
        const stateKey = getStateKey(state.value);

        switch (stateKey as LevelMachineStatePart1) {
          case LevelMachineStatePart1.walk:
            if (this.opts.showIntros) {
              this.afterInfoScreen = async () => {
                this.setInteractionDisabled(true);
                await sleeper(100);
                const paintingParadise = this.findByObjectType(commonObjectTypes.image, 'paradise')!;
                // Spot fix
                await this.focusCameraOnActor(paintingParadise, vec(0.5, 0));
                await this.player.showDialog(dialogs[levelMachinePart1.id].intro, {ignoreInteractionDisabled: true});
                await this.focusCameraOnActor(this.player);
                this.setInteractionDisabled(false);
              };
            }
            return;

          case LevelMachineStatePart1.canEnterBuilding:
            this.setInteractionDisabled(true);
            const policeGrinder = this.findNPC(npcs.policeGrinder)!;

            await sleeper(500);

            this.player.direction = Side.Top;
            await this.player.showUIPopup(UIPopup.exclamationBlue);

            await sleeper(250);

            await this.focusCameraOnActor(policeGrinder);

            const speakerGrinder = 'speakerGrinder';
            commonSounds.megaphone.play();
            await this.dialogBox.show(dialogs[levelMachinePart1.id][stateKey][speakerGrinder], {
              sprite: getFirstSpriteInActor(this.findByObjectType(commonObjectTypes.speaker, speakerGrinder)),
              ignoreInteractionDisabled: true,
            });

            // Move the guard away from the door
            // -15 so that we don't accidentally collide with the door corner
            await policeGrinder.actions.moveBy(vec(-15, 0), npcSpeedAction).toPromise();

            await sleeper(250);
            await this.focusCameraOnActor(this.player);

            this.setInteractionDisabled(false);

            return;
          case LevelMachineStatePart1.enteredBuilding:
            this.opts.completionCounter.completeEntry(completionEntries.part1);
            commonSounds.gameOverUnknownResult.play();
            this.infoScreenParadise?.show();
            this.engine.currentScene.remove(this.player);
            return;
        }
      })
      .start();
  }

  initScenePart2 () {
    const payloadKeyDidSeeBarrel = 'didSeeBarrel';

    this.levelService = interpret(levelMachinePart2)
      .onTransition(async (state) => {
        debug('[part2] new state', state);
        const stateKey = getStateKey(state.value);

        switch (stateKey as LevelMachineStatePart2) {
          case LevelMachineStatePart2.walk:
            switch ((state.value as StateValueMap)[LevelMachineStatePart2.walk]) {
              case  LevelMachineStatePart2Keycard.default:
                if (this.opts.showIntros) {
                  this.afterInfoScreen = async () => {
                    this.setInteractionDisabled(true);
                    await sleeper(100);
                    await this.player.showDialog(dialogs[levelMachinePart2.id].intro, {ignoreInteractionDisabled: true});
                    this.setInteractionDisabled(false);
                  };
                }
                return;
            }
            return;
          case LevelMachineStatePart2.playerHasKey:
            return;
          case LevelMachineStatePart2.playerHasOpenedOneBarrel:
            this.player.trackingCameraLevel = TrackingCameraLevel.warning;
            return;
          case LevelMachineStatePart2.playerHasTalkedWithFamily: {
            this.setInteractionDisabled(true);

            // noinspection ES6MissingAwait
            (async () => {
              this.player.trackingCameraLevel = TrackingCameraLevel.danger;
              const policeGrinder = this.findNPC(npcs.policeGrinder)!;

              policeGrinder.preferredDirection.clear();
              policeGrinder.trackActor = this.player;
              await policeGrinder.showUIPopup(UIPopup.exclamationRed);

              policeGrinder.trackingDisabled.push(true);

              // Move the npc to intersect forcefully the player
              await policeGrinder.actions.moveBy(vec(16, 0), npcSpeedAction).toPromise();
              policeGrinder.actions.getQueue().add(new NFMeet({
                actor: policeGrinder,
                actorToMeet: this.player,
                speed: npcSpeedAction,
                forceAxis: Axis.Y,
              }));
              policeGrinder.actions.getQueue().add(new NFMeet({
                actor: policeGrinder,
                actorToMeet: this.player,
                speed: npcSpeedAction,
                forceAxis: Axis.X,
              }));
              await policeGrinder.actions.toPromise();
              // await policeGrinder.actions.moveTo(vec(policeGrinder.pos.x, this.player.pos.y), npcSpeedAction).toPromise();

              policeGrinder.trackingDisabled.pop();
              this.player.trackActor = policeGrinder;

              await sleeper(50);

              // policeGrinder.actions.getQueue().add(new NFMeet(policeGrinder, this.player, npcSpeedAction));
              // await policeGrinder.actions.toPromise();

              const text = dialogs[levelMachinePart2.id][stateKey].npcs[npcs.randomMan1];
              await policeGrinder.showDialog(text, {ignoreInteractionDisabled: true});

              this.opts.completionCounter.completeEntry(completionEntries.part2RevealSecret);
              commonSounds.gameOverLose.play();
              this.infoScreenCaptured?.show();
            })();
          }
            return;
          case LevelMachineStatePart2.playerHasGivenKeyToGuard: {
            this.setInteractionDisabled(true);

            const npcPlayerPart1 = this.findNPC(npcs.playerPart1)!;
            const npcFamilyWife = this.findNPC(npcs.familyWife)!;
            const policeGrinder = this.findNPC(npcs.policeGrinder)!;
            const policeGuardhouse = this.findNPC(npcs.policeGuardhouse)!;
            const doorGrinder = this.findDoor(doors.doorGrinder)!.get(DoorComponent)!;
            const doorGuardhouse = this.findDoor(doors.doorGuardhouse)!.get(DoorComponent)!;

            // Enable player automatic graphics
            this.player.setDirectionBasedOnVelocity = true;

            await this.focusCameraOnActor(policeGuardhouse);

            // Step back player to allow npc to pass
            debug('step back player begin', this.player.debugGetDirectionalActorSettings());
            this.player.fixedDirection.push(Side.Left);
            await this.player.actions.moveBy(vec(16, 0), npcSpeedAction).toPromise();
            this.player.fixedDirection.clear();
            debug('step back player end', this.player.debugGetDirectionalActorSettings());

            {
              // Move guard to guardhouse
              const pathObj = this.map.data.getObjectLayerByName(layerNames.paths)
                .getObjectByName('policeGuardhouseToGuardhouse')!;

              policeGuardhouse.fixedDirection.clear();
              policeGuardhouse.isSitting.push(false);

              await policeGuardhouse.followPath(pathObj);

              policeGuardhouse.body.collisionType = CollisionType.PreventCollision;
              doorGuardhouse.isOpen = true;
              // Fake entering the door
              await policeGuardhouse.actions.moveBy(vec(0, -4), npcSpeedAction).toPromise();
              this.engine.currentScene.remove(policeGuardhouse);
              doorGuardhouse.isOpen = false;
            }

            await this.focusCameraOnActor(this.player);

            await sleeper(500);

            if (state.history?.value && getStateKey(state.history.value) == LevelMachineStatePart2.playerHasOpenedOneBarrel) {
              await sleeper(500);
              await this.player.showDialog(dialogs[levelMachinePart2.id][stateKey].npcs['playerOhNo'], {ignoreInteractionDisabled: true});
              this.levelService.send(LevelMachineStatePart2Events.playerEnteredGrinder, {[payloadKeyDidSeeBarrel]: true});
              return;
            }

            await this.player.showDialog(dialogs[levelMachinePart2.id][stateKey].npcs['player'], {ignoreInteractionDisabled: true});

            // Walk player to witness the reward
            {
              // Move guard to guardhouse
              const pathObj = this.map.data.getObjectLayerByName(layerNames.paths)
                .getObjectByName('playerToReward')!;
              await this.player.followPath(pathObj, playerSpeedAction);
            }

            await sleeper(500);

            await npcPlayerPart1.showUIPopup(UIPopup.exclamationBlue);
            npcPlayerPart1.trackingDisabled.push(true);

            // Look at speaker
            npcPlayerPart1.preferredDirection.push(Side.Top);

            // Move camera up a bit
            await this.focusCameraOnActor(policeGrinder);

            const speakerGrinder = 'speakerGrinder';
            commonSounds.megaphone.play();
            await this.dialogBox.show(dialogs[levelMachinePart2.id][stateKey][speakerGrinder], {
              sprite: getFirstSpriteInActor(this.findByObjectType(commonObjectTypes.speaker, speakerGrinder)),
              ignoreInteractionDisabled: true,
            });

            await sleeper(500);
            await this.focusCameraOnActor(npcPlayerPart1);
            await sleeper(500);

            npcPlayerPart1.preferredDirection.pop();

            // Talk with player
            npcPlayerPart1.direction = Side.Bottom;

            await npcPlayerPart1.showDialog(dialogs[levelMachinePart2.id][stateKey].npcs[npcPlayerPart1.name], {
              ignoreInteractionDisabled: true,
            });

            await sleeper(500);

            npcFamilyWife.fixedDirection.push(Side.Right);
            await npcFamilyWife.actions.moveBy(vec(-16, 0), npcSpeedAction).toPromise();
            npcFamilyWife.fixedDirection.clear();

            // Move the guard away from the door
            // -15 so that we don't accidentally collide with the door corner
            await policeGrinder.actions.moveBy(vec(-15, 0), npcSpeedAction).toPromise();

            // Fake move the old player to the grinder
            {
              // Move guard to guardhouse
              const pathObj = this.map.data.getObjectLayerByName(layerNames.paths)
                .getObjectByName('playerPart1ToGrinder')!;

              await npcPlayerPart1.followPath(pathObj);

              npcPlayerPart1.body.collisionType = CollisionType.PreventCollision;
              doorGrinder.isOpen = true;
              // Fake entering the door
              await npcPlayerPart1.actions.moveBy(vec(0, -4), npcSpeedAction).toPromise();
              this.engine.currentScene.remove(npcPlayerPart1);
              doorGrinder.isOpen = false;
            }

            await sleeper(1000);
            this.levelService.send(LevelMachineStatePart2Events.playerEnteredGrinder);
          }
            return;

          case LevelMachineStatePart2.playerHasEnteredGrinder: {
            if (state.event[payloadKeyDidSeeBarrel] == true) {
              this.opts.completionCounter.completeEntry(completionEntries.part2HelpGuardWithBarrel);
            } else {
              this.opts.completionCounter.completeEntry(completionEntries.part2HelpGuardNoBarrel);
            }

            commonSounds.gameOverUnknownResult.play();
            this.infoScreenHelpedGuard?.show();
          }
            return;
        }
      })
      .start();
  }

}