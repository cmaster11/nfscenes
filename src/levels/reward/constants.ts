// Cameras will follow trackable actors
import levelText from './level.yaml';
import {registerInWindow} from '../../system';

// -----
export const IS_DEBUG = false;
// -----

export const levelTitle = 'Reward';

export enum LevelScene {
  part1 = 'part1',
  part2 = 'part2',
}

export const layerNames = {
  objects: 'objects',
  benches: 'benches',
  ceiling: 'ceiling',
  barrels: 'barrels',
  plants: 'plants',
  paths: 'paths',
  objectsPart1: 'objectsPart1',
  objectsPart2: 'objectsPart2',
};

export const triggerNames = {
  doorGrinder: 'doorGrinder',
  levelOutPart1: 'levelOutPart1',
};

export const completionEntries = {
  part1: 'part1',
  part2RevealSecret: 'part2RevealSecret',
  part2HelpGuardNoBarrel: 'part2HelpGuardNoBarrel',
  part2HelpGuardWithBarrel: 'part2HelpGuardWithBarrel',
  barrelInAlley: 'barrelInAlley',
  hiddenNote: 'hiddenNote',
  pray: 'pray',
};

export const objectTypes = {
  fence: 'fence',
  barrel: 'barrel',
  computer: 'computer',
  flower: 'flower',
  vase: 'vase',
  vaseTop: 'vaseTop',
};

export const tags = {
  barrelOpen: 'barrelOpen',
};

export const objectProperties = {};

export const interactables = {
  keycardGuardhouse: 'keycardGuardhouse',
};

export const npcs = {
  playerPart1: 'playerPart1',
  policeGrinder: 'policeGrinder',
  policeGuardhouse: 'policeGuardhouse',

  familyKid: 'familyKid',
  familySon: 'familySon',
  familyWife: 'familyWife',

  randomMan1: 'randomMan1',
};

export const doors = {
  doorGrinder: 'doorGrinder',
  doorGuardhouse: 'doorGuardhouse',
};

export const infoScreenControlsName = 'infoScreenControls';
export const infoScreenParadise = 'infoScreenParadise';
export const infoScreenHelpedGuardName = 'infoScreenHelpedGuard';
export const infoScreenCapturedName = 'infoScreenCaptured';

// export const npcKeyPolice = 'npc_police';
// export const npcKeyNPC1 = 'npc_npc1';
//
// export const interactableKeyLeader = 'leader';

export const cameraZoom = {
  global: 3,
};

export const dialogs = levelText.dialogs;
export const dialogsDefault = levelText.dialogs['default'];
registerInWindow('dialogs', dialogs);
registerInWindow('dialogsDefault', dialogsDefault);