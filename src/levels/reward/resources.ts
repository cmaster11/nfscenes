import {TiledMapFormat} from '@excaliburjs/plugin-tiled';
import * as ex from 'excalibur';
import {Loadable} from 'excalibur';
import {NFTiledMapResource} from '../../common/nfTiledMapResource';
import {debug, debugError} from '../../system';
import * as path from 'path';
import {importAll} from '../../common/resources';
import {NFMapLoader} from '../../common/nfMapLoader';
import {getNPCSpriteSheet} from '../../common/npc';

// @ts-ignore
importAll(require.context('../../../res/levels/reward/', true));

export const resources = {
  playerPart1: new ex.ImageSource(require('../../../res/levels/reward/player_part1.png')),
  playerPart2: new ex.ImageSource(require('../../../res/levels/reward/player_part2.png')),
  tilesetCity: new ex.ImageSource(require('../../../res/levels/reward/tileset-city.png')),

  mapLoader: new NFMapLoader(
    new NFTiledMapResource(require('../../../res/levels/reward/level.tmx'), {
      mapFormatOverride: TiledMapFormat.TMX,
    }),
    (name) => require(`../../../res/levels/reward/npc_${name}.png`),
    (name) => require(`../../../res/levels/reward/image_${name}.png`),
  ),
};

export const sprites = {
  playerPart1: getNPCSpriteSheet(resources.playerPart1),
  playerPart2: getNPCSpriteSheet(resources.playerPart2),
  tilesetCity: ex.SpriteSheet.fromImageSource({
    image: resources.tilesetCity,
    grid: {
      columns: 18,
      rows: 20,
      spriteWidth: 16,
      spriteHeight: 16,
    },
  }),
};

export const subSprites = {
  // leader: sprites.tilesetCity.getSprite(9, 1)!,
  // plant: sprites.tilesetCity.getSprite(11, 0)!,
  doorClosed: sprites.tilesetCity.getSprite(5, 2)!,
  doorOpen: sprites.tilesetCity.getSprite(6, 2)!,
  barrel1Open: sprites.tilesetCity.getSprite(6, 8)!,
  barrel2Open: sprites.tilesetCity.getSprite(5, 8)!,
};

export const animations = {};

const basePathLevel = path.normalize(__dirname.replace(/\\/g, '/')).substring('src/'.length);
resources.mapLoader.map.convertPath = function (originPath, relativePath) {
  let basePath = basePathLevel;
  if (!originPath.startsWith('data:')) {
    basePath = path.normalize(basePathLevel + '/' + path.dirname(originPath));
  }

  const requirePath = './res/' + path.normalize(basePath + '/' + relativePath);
  debug('converted path', {originPath, basePathLevel, relativePath, requirePath});
  try {
    const value = require.cache[requirePath]?.exports as string;
    // const value = (__webpack_require__)(requirePath)
    if (value == undefined) {
      debugError('undefined value returned from require cache', requirePath, require.cache);
    }
    return value;
  } catch (e) {
    debugError('failed to require data ', e, requirePath, require.cache);
    throw e;
  }
};

export const loaderResources: Loadable<any>[] = Object.values(resources);