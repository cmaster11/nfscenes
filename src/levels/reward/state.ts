import {createMachine} from 'xstate';
import {registerInWindow} from '../../system';

/*

Storyline (part 1):

1. The game begins as a elderly person who can get to the promised land

    - A guard is blocking the entrance
    - You need to talk to your family for the guard to move
    - After the guard moves, you can enter in the main building.
    - When you enter, win screen, congrats you are traveling towards paradise.


 */

export enum LevelMachineStatePart1 {
  walk = 'walk',
  canEnterBuilding = 'canEnterBuilding',
  enteredBuilding = 'enteredBuilding',
}

export enum LevelMachineStatePart1Events {
  playerTalkedWithAllCharacters = 'playerTalkedWithAllCharacters',
  playerEnteredTheBuilding = 'playerEnteredTheBuilding'
}

registerInWindow('LevelMachineStatePart1Events', LevelMachineStatePart1Events);

export const levelMachinePart1 = createMachine({
  id: 'levelMachinePart1',
  initial: LevelMachineStatePart1.walk,
  states: {
    [LevelMachineStatePart1.walk]: {
      on: {
        [LevelMachineStatePart1Events.playerTalkedWithAllCharacters]: LevelMachineStatePart1.canEnterBuilding,
      },
    },
    [LevelMachineStatePart1.canEnterBuilding]: {
      on: {
        [LevelMachineStatePart1Events.playerEnteredTheBuilding]: LevelMachineStatePart1.enteredBuilding,
      },
    },
    [LevelMachineStatePart1.enteredBuilding]: {},
  },
});

/*

Storyline (part 2):

2. You are now a random citizen, exploring the area

    - A guard has lost his key, please bring it to me!
    - A family is saying by by to a grandpa
    - TODO Where to find the key?

3. Once you have the key:

    - You can then enter in the side building

      - The computer talks about the next batch TODO
      - After you open one barrel, you can see there are human remains

            - Camera goes warning
            - You can go out and talk to the family, and tell them that chopped humans are
              coming out of the main building

                - A guard comes and arrests you, you need to forget.
                - END SCREEN (- points)

            - You can give the key to the guard (4)

4. You can give the key to the guard:

      - The guard then goes back in the side building
      - No interaction
      - You walk together with the family to look at the bye bye
      - A megaphone / or the guard, says to the grandpa that they can now go in
      - The granpa goes in
      - END SCREEN, try again! (+points)

 */

export enum LevelMachineStatePart2Keycard {
  default = 'default',
  playerHasSeenGuardhouseDoor = 'playerHasSeenGuardhouseDoor',
  playerHasSpokenWithGuard = 'playerHasSpokenWithGuard',
}

export enum LevelMachineStatePart2KeycardEvents {
  playerSawGuardhouseDoor = 'playerSawGuardhouseDoor',
  playerSpokeWithGuard = 'playerSpokeWithGuard',
}

export const levelMachinePart2Keycard = {
  id: 'levelMachinePart2Keycard',
  initial: LevelMachineStatePart2Keycard.default,
  states: {
    [LevelMachineStatePart2Keycard.default]: {
      on: {
        [LevelMachineStatePart2KeycardEvents.playerSawGuardhouseDoor]: LevelMachineStatePart2Keycard.playerHasSeenGuardhouseDoor,
        [LevelMachineStatePart2KeycardEvents.playerSpokeWithGuard]: LevelMachineStatePart2Keycard.playerHasSpokenWithGuard,
      },
    },
    [LevelMachineStatePart2Keycard.playerHasSeenGuardhouseDoor]: {
      on: {
        [LevelMachineStatePart2KeycardEvents.playerSpokeWithGuard]: LevelMachineStatePart2Keycard.playerHasSpokenWithGuard,
      },
    },
    [LevelMachineStatePart2Keycard.playerHasSpokenWithGuard]: {},
  },
};

export enum LevelMachineStatePart2 {
  walk = 'walk',
  playerHasKey = 'playerHasKey',
  playerHasOpenedOneBarrel = 'playerHasOpenedOneBarrel',

  playerHasTalkedWithFamily = 'playerHasTalkedWithFamily',
  playerHasGivenKeyToGuard = 'playerHasGivenKeyToGuard',
  playerHasEnteredGrinder = 'playerHasEnteredGrinder',
}

export enum LevelMachineStatePart2Events {
  playerFoundTheKey = 'playerFoundTheKey',
  playerOpenedABarrel = 'playerOpenedABarrel',

  playerTalkedWithFamily = 'playerTalkedWithFamily',
  playerGaveKeyToGuard = 'playerGaveKeyToGuard',
  playerEnteredGrinder = 'playerEnteredGrinder',
}

registerInWindow('LevelMachineStatePart2Events', LevelMachineStatePart2Events);

export const levelMachinePart2 = createMachine({
  id: 'levelMachinePart2',
  initial: LevelMachineStatePart2.walk,
  states: {
    [LevelMachineStatePart2.walk]: {
      on: {
        [LevelMachineStatePart2Events.playerFoundTheKey]: LevelMachineStatePart2.playerHasKey,
      },
      ...levelMachinePart2Keycard,
    },
    [LevelMachineStatePart2.playerHasKey]: {
      on: {
        [LevelMachineStatePart2Events.playerOpenedABarrel]: LevelMachineStatePart2.playerHasOpenedOneBarrel,
        [LevelMachineStatePart2Events.playerGaveKeyToGuard]: LevelMachineStatePart2.playerHasGivenKeyToGuard,
      },
    },
    [LevelMachineStatePart2.playerHasOpenedOneBarrel]: {
      on: {
        [LevelMachineStatePart2Events.playerTalkedWithFamily]: LevelMachineStatePart2.playerHasTalkedWithFamily,
        [LevelMachineStatePart2Events.playerGaveKeyToGuard]: LevelMachineStatePart2.playerHasGivenKeyToGuard,
      },
    },
    [LevelMachineStatePart2.playerHasTalkedWithFamily]: {},
    [LevelMachineStatePart2.playerHasGivenKeyToGuard]: {
      on: {
        [LevelMachineStatePart2Events.playerEnteredGrinder]: LevelMachineStatePart2.playerHasEnteredGrinder,
      },
    },
    [LevelMachineStatePart2.playerHasEnteredGrinder]: {},
  },
});
