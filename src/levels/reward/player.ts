import {Player} from '../../common/player';
import {Actor, BodyComponent, CollisionType, Engine, Side, vec} from 'excalibur';
import {NPC} from '../../common/npc';
import {Controller} from './controller';
import {dialogs, dialogsDefault, doors, interactables, npcs, objectTypes, tags, triggerNames} from './constants';
import {debug} from '../../system';
import {getFirstSpriteInActor} from '../../util/sprite';
import {
  commonObjectNames,
  commonObjectProperties,
  commonObjectTypes,
  dialogKeyDefault,
  playerSpeedAction,
} from '../../common/constants';
import {TrackingCamera} from '../../common/trackingCamera';
import {
  levelMachinePart1,
  levelMachinePart2,
  LevelMachineStatePart1,
  LevelMachineStatePart1Events,
  LevelMachineStatePart2,
  LevelMachineStatePart2Events,
  LevelMachineStatePart2KeycardEvents,
} from './state';
import {showScreenTransition, UIPopup} from '../../common/ui';
import {DoorComponent} from '../../common/doorComponent';
import {getStateKey} from '../../util/state';
import {NFTouchEvent} from '../../common/nfTouchEvent';
import {TiledObject} from '@excaliburjs/plugin-tiled';
import {sleeper} from '../../util/promise';
import {getUnitVectorForSide} from '../../util/side';
import {subSprites} from './resources';

export class LevelPlayer extends Player<Controller> {

  onCollisionStart (touchEvent: NFTouchEvent, touching: Record<number, NFTouchEvent>) {
    super.onCollisionStart(touchEvent, touching);

    if ((touchEvent.tiledObject?.type == commonObjectTypes.trigger)) {
      const objectName = touchEvent.tiledObject.name;
      switch (objectName) {
        case commonObjectNames.levelOut:
          this.showDialog(dialogsDefault.levelOut);
          return;
        case triggerNames.levelOutPart1: {
          const stateKey = getStateKey(this.lci.levelService.state.value);
          const dialog = dialogs[this.lci.levelService.id][stateKey][triggerNames.levelOutPart1];
          this.showDialog(dialog);
        }
          return;
      }

      switch (this.lci.levelService.id) {
        case levelMachinePart1.id:

          switch (objectName) {
            case triggerNames.doorGrinder:
              this.triggerPart1DoorGrinderDialog();
              return;
          }

          return;
      }
    }
  }

  part1CheckTalkedWithFamily () {
    // If we have talked to all the family, unlock next state!
    const familyMembers = [npcs.familyKid, npcs.familySon, npcs.familyWife];
    if (familyMembers.reduce((acc, k) => acc + (this.talkCounter[k] ? 1 : 0), 0) == familyMembers.length) {
      this.lci.levelService.send(LevelMachineStatePart1Events.playerTalkedWithAllCharacters);
    }
  }

  triggerPart1DoorGrinderDialog () {
    const actor = this.lci.findDoor(doors.doorGrinder)!;
    const door = actor.get(DoorComponent)!;
    const sprite = getFirstSpriteInActor(actor);

    const {
      text,
      choices,
    } = dialogs[levelMachinePart1.id][LevelMachineStatePart1.canEnterBuilding][doors.doorGrinder];

    this.lci.dialogBox.show(text, {sprite, choices})
      .then((choice) => {
        if (choice == 0) {
          // GO IN!
          this.lci.setInteractionDisabled(true);

          actor!.get(BodyComponent)!.collisionType = CollisionType.Passive;
          door.isOpen = true;

          // Move up just enough to intercept the door
          this.actions.moveBy(vec(0, -4), playerSpeedAction);
          showScreenTransition({
            engine: this.lci.engine,
            onTransitionHalfway: () => {
              this.lci.levelService.send(LevelMachineStatePart1Events.playerEnteredTheBuilding);
            },
          });
        }
      });
  }

  protected onNPCInteraction (engine: Engine, actor: NPC<Controller>): boolean {
    debug('npc interaction', actor);

    const actorName = actor.name;
    this.talkCounter[actorName] = this.talkCounter[actorName] || 0;
    const stateKey = getStateKey(this.lci.levelService.state.value);
    const levelServiceId = this.lci.levelService.id;

    const text = dialogs[levelServiceId]?.[stateKey]?.npcs[actorName];
    if (text == null) {
      return false;
    }

    let p: Promise<any> = actor.showDialog(text)
      .then(() => {
        this.talkCounter[actorName]++;
      });

    switch (this.lci.levelService.id) {
      case levelMachinePart1.id:

        switch (stateKey) {
          case LevelMachineStatePart1.walk:
            p = p.then(() => this.part1CheckTalkedWithFamily());
            break;
          case LevelMachineStatePart1.canEnterBuilding:
            break;
        }

        break;
      case levelMachinePart2.id:

        switch (stateKey) {
          case LevelMachineStatePart2.walk:

            switch (actor.name) {
              case npcs.policeGuardhouse:
                p = p.then(() => this.lci.levelService.send(LevelMachineStatePart2KeycardEvents.playerSpokeWithGuard));
                return true;
            }

            break;
          case LevelMachineStatePart2.playerHasOpenedOneBarrel:

            switch (actor.name) {
              case npcs.playerPart1: {
                const counterPopsKey = 'popsAfterBarrel';
                this.talkCounter[counterPopsKey] = this.talkCounter[counterPopsKey] || 0;

                switch (this.talkCounter[counterPopsKey]) {
                  case 0:

                    p = p
                      .then(async () => {
                        //
                        actor.trackingDisabled.push(true);
                        {
                          const {text, choices} = dialogs[levelServiceId][stateKey].playerPart1.dialogTell1;
                          const choice = await this.showDialog(text, {choices});
                          if (choice != 0) {
                            actor.trackingDisabled.pop();
                            return;
                          }
                        }

                        {
                          const {text} = dialogs[levelServiceId][stateKey].playerPart1.dialogAnswerBack1;
                          await actor.showDialog(text, {});
                        }

                        actor.trackingDisabled.pop();
                        this.talkCounter[counterPopsKey]++;
                      });

                    break;
                  case 1:

                    p = p
                      .then(async () => {
                        //
                        actor.trackingDisabled.push(true);
                        {
                          const {text, choices} = dialogs[levelServiceId][stateKey].playerPart1.dialogTell2;
                          const choice = await this.showDialog(text, {choices});
                          if (choice != 0) {
                            actor.trackingDisabled.pop();
                            return;
                          }
                        }

                        {
                          const {text} = dialogs[levelServiceId][stateKey].playerPart1.dialogAnswerBack2;
                          await actor.showDialog(text, {});
                        }

                        actor.trackingDisabled.pop();
                        this.lci.levelService.send(LevelMachineStatePart2Events.playerTalkedWithFamily);
                      });

                    break;
                }

                return true;
              }
            }

          // NO BREAK!
          // noinspection FallThroughInSwitchStatementJS
          case LevelMachineStatePart2.playerHasKey:

            switch (actor.name) {
              case npcs.policeGuardhouse: {
                const {text, choices, afterText} = dialogs[levelServiceId][stateKey].policeGuardhouseKey;
                p = p
                  .then(async () => {
                    this.lci.setInteractionDisabled(true);
                    const choice = await this.showDialog(text, {choices, ignoreInteractionDisabled: true});
                    if (choice != 0) {
                      this.lci.setInteractionDisabled(false);
                      return;
                    }

                    const key = this.inventory.keys.find((o) => o.name == interactables.keycardGuardhouse)!;
                    this.inventory.removeObject(key);
                    actor.inventory.addObject(key);
                    await actor.showUIPopup(UIPopup.exclamationBlue);
                    await actor.showDialog(afterText, {ignoreInteractionDisabled: true});

                    this.lci.levelService.send(LevelMachineStatePart2Events.playerGaveKeyToGuard);
                  });
                return true;
              }
            }

            break;
        }

        break;
    }

    return true;
  }

  protected onPickInteraction (engine: Engine, actor: Actor): boolean {
    debug('pick interaction', actor);
    const stateValue = this.lci.levelService.state.value as any;
    const walkState = stateValue[LevelMachineStatePart2.walk] as string | undefined;

    if (walkState) {

      switch (actor.name) {
        case interactables.keycardGuardhouse:
          this.lci.dialogBox.show(dialogs.levelMachinePart2.pick.kaycardGuardhouse[walkState], {sprite: getFirstSpriteInActor(actor)})
            .then(() => this.pickupObject(engine, actor))
            .then(() => this.lci.levelService.send(LevelMachineStatePart2Events.playerFoundTheKey));

          break;
      }

    }

    return true;
  }

  protected onInteraction (engine: Engine): boolean {
    debug('player interaction', this.touching);
    const stateKey = getStateKey(this.lci.levelService.state.value);

    const touchedEvents = this.getTouchedAndFacedEvents((e) => e.tiledObject != null);
    touchedEvents.forEach((e) => {
      const secret = e.tiledObject!.getProperty<string>(commonObjectProperties.secret);
      if (secret) {
        this.lci.opts.completionCounter.completeEntry(secret.value);
      }
    });

    let actor: Actor | null;
    if ((actor = this.getTouchedAndFacedActor((e) => e.actor instanceof TrackingCamera))) {

      // const cameraName = actor.name;
      // if (this.firstTalkedCamera == null) {
      //   this.firstTalkedCamera = cameraName;
      // }
      //
      // switch ((actor as TrackingCamera).cameraLevel) {
      //   case TrackingCameraLevel.normal:
      //     this.lci.dialogBox.show(this.firstTalkedCamera == cameraName ? dialogsDefault.playerToCamera_normal_first : dialogsDefault.playerToCamera_normal_second);
      //     break;
      //   case TrackingCameraLevel.warning:
      //     this.lci.dialogBox.show(this.firstTalkedCamera == cameraName ? dialogsDefault.playerToCamera_warning_first : dialogsDefault.playerToCamera_warning_second);
      //     break;
      //   case TrackingCameraLevel.danger:
      //     break;
      // }

      return true;
    }

    if ((actor = this.getTouchedAndFacedActor((e) => e.tiledObject?.type == objectTypes.barrel))) {
      debug('barrel roll', actor);

      switch (this.lci.levelService.id) {
        case levelMachinePart1.id:
          this.lci.dialogBox.show(dialogsDefault.barrel, {sprite: getFirstSpriteInActor(actor)});
          break;
        case levelMachinePart2.id:

          switch (actor.name) {
            case 'barrel1':
            case 'barrel2':
              const dialogCounterKey = 'barrelBlood';
              this.talkCounter[dialogCounterKey] = (this.talkCounter[dialogCounterKey] || 0);

              switch (this.talkCounter[dialogCounterKey]) {
                case 0: {
                  const {text, choices} = dialogs[levelMachinePart2.id].barrel.barrelFirst;
                  this.lci.dialogBox.show(text, {choices, sprite: getFirstSpriteInActor(actor)})
                    .then(async (choice) => {
                      if (choice > 0) {
                        return;
                      }

                      this.talkCounter[dialogCounterKey]++;
                      const openGraphics = (subSprites as any)[actor!.name + 'Open'];
                      actor!.graphics.use(openGraphics);
                      actor!.addTag(tags.barrelOpen);

                      await this.lci.dialogBox.show(dialogs[levelMachinePart2.id].barrel.barrelOpen.text, {sprite: getFirstSpriteInActor(actor!)});

                      this.lci.levelService.send(LevelMachineStatePart2Events.playerOpenedABarrel);
                    });
                }
                  break;
                default: {
                  const {text} = dialogs[levelMachinePart2.id].barrel[actor.hasTag(tags.barrelOpen) ? 'barrelOpen' : 'barrelSecond'];
                  this.lci.dialogBox.show(text, {sprite: getFirstSpriteInActor(actor)});
                }
                  break;
              }

              return true;
            case 'barrel3': {
              const {text} = dialogs[levelMachinePart2.id].barrel.barrelDry;
              this.lci.dialogBox.show(text, {sprite: getFirstSpriteInActor(actor)});
            }
              return true;
            default:
              this.lci.dialogBox.show(dialogsDefault.barrel, {sprite: getFirstSpriteInActor(actor)});
          }

          break;
      }
      return true;
    }

    if ((actor = this.getTouchedAndFacedActor((e) => e.tiledObject?.type == objectTypes.computer))) {
      let p = this.lci.dialogBox.show(dialogs[this.lci.levelService.id].computer[actor.name || dialogKeyDefault], {sprite: getFirstSpriteInActor(actor)});
      return true;
    }

    // if ((actor = this.getTouchedAndFacedActor((e) =>
    //   e.rawActorTiledObject?.type == objectTypes.vase || e.rawActorTiledObject?.type == objectTypes.flower))) {
    //   debug('flower', actor);
    //   const text = dialogsDefault.flower[actor.name] || dialogsDefault.flower.generic;
    //   this.lci.dialogBox.show(text, {sprite: getFirstSpriteInActor(actor)});
    //   return true;
    // }

    if ((actor = this.getTouchedAndFacedActor((e) => e.tiledObject?.type == commonObjectTypes.door))) {
      debug('door', actor);
      const door = actor.get(DoorComponent)!;
      const sprite = getFirstSpriteInActor(actor);

      switch (this.lci.levelService.id) {
        case levelMachinePart1.id:

          switch (actor.name) {
            case doors.doorGrinder:
              this.triggerPart1DoorGrinderDialog();
              return true;
          }

          break;

        case levelMachinePart2.id:
          switch (actor.name) {
            case doors.doorGuardhouse:
              this.lci.levelService.send(LevelMachineStatePart2KeycardEvents.playerSawGuardhouseDoor);
              break;
          }
          break;
      }

      // Is the door locked and we can open it?
      if (door.isLocked) {
        if (door.actorHasInventoryItemToUnlockTheDoor(this)) {

          const {text, choices} = dialogsDefault.doorWithKey;
          this.lci.dialogBox.show(text, {sprite, choices})
            .then(async (choice) => {
              if (choice != 0) {
                return;
              }

              door.isLocked = false;
              await this.lci.dialogBox.show(dialogsDefault.doorUnlocked, {sprite});

              // Get the direction of the door, and step into it automatically
              const dirVec = (door.owner as Actor).center.sub(this.collider.get().center);
              const direction = Side.fromDirection(dirVec);
              if (direction != Side.None) {
                await this.actions.moveBy(getUnitVectorForSide(direction).scaleEqual(4), playerSpeedAction).toPromise();
              }
            });
        } else {
          this.lci.dialogBox.show(dialogsDefault.door, {sprite});
        }
      }
      return true;
    }

    if ((actor = this.getTouchedAndFacedActor((e) => e.tiledObject?.type == commonObjectTypes.info))) {
      this.talkCounter[actor.name] = this.talkCounter[actor.name] || 0;

      const dialog = dialogs.info[actor.name];
      const sprite = getFirstSpriteInActor(actor);

      const text = dialog.text || dialog;
      const comment = dialog.comment;
      const commentAgain = dialog.commentAgain;

      (async () => {
        if (comment) {
          const counter = this.talkCounter[actor!.name];
          if (commentAgain && counter > 0) {
            await this.showDialog(commentAgain);
          } else {
            await this.showDialog(comment);
          }
        }

        await this.lci.dialogBox.show(text, {sprite});

        switch (actor!.name) {
          case 'leader':
            this.lci.setInteractionDisabled(true);
            await sleeper(400);
            await this.showUIPopup(UIPopup.pray, {duration: 2500});
            await sleeper(400);
            this.lci.setInteractionDisabled(false);
        }

        this.talkCounter[actor!.name]++;
      })();

      return true;
    }

    // Look if there's any relevant dialog to see
    if (touchedEvents.length > 0) {
      const findDialogForObject = (obj: TiledObject): string | undefined => {
        if (obj.type == null) {
          return undefined;
        }
        const name = obj.name || dialogKeyDefault;
        return dialogs[this.lci.levelService.id]?.[stateKey]?.[obj.type]?.[name] ||
          dialogs[this.lci.levelService.id]?.[obj.type]?.[name] ||
          dialogs[obj.type]?.[name] ||
          dialogsDefault[obj.type]?.[name] ||
          dialogs[this.lci.levelService.id]?.[stateKey]?.[obj.type]?.[dialogKeyDefault] ||
          dialogs[this.lci.levelService.id]?.[obj.type]?.[dialogKeyDefault] ||
          dialogs[obj.type]?.[dialogKeyDefault] ||
          dialogsDefault[obj.type]?.[dialogKeyDefault];
      };

      // Find the first actor which can trigger a dialog
      const dialog = touchedEvents
        .map((event) => {
          const obj = event.tiledObject;
          if (obj == null) {
            return null;
          }

          let dialog = findDialogForObject(obj);
          if (dialog == null && event.rawActorTiledObject && event.rawActorTiledObject != event.tiledObject) {
            // Use any references
            dialog = findDialogForObject(event.rawActorTiledObject);
          }

          if (dialog == null) {
            return null;
          }

          return {
            actor: event.actor,
            dialog: dialog,
          };
        })
        .filter((t) => t)
        .at(0);
      if (dialog) {
        this.lci.dialogBox.show(dialog.dialog, {sprite: getFirstSpriteInActor(dialog.actor)});
        return true;
      }
    }

    //
    // if ((actor = this.getTouchedAndFacedActor((e) => e.tiledObject?.type == objectTypes.fence))) {
    //   switch (actor.name) {
    //     case 'fenceGrinder':
    //       this.lci.dialogBox.show(dialogsDefault.barrel, {sprite: getFirstSpriteInActor(actor)});
    //       return true;
    //   }
    // }

    return false;
  }

}