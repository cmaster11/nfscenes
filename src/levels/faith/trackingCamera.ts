import * as ex from 'excalibur';
import {Actor, ActorArgs, Engine, vec} from 'excalibur';
import {animations, sprites} from './resources';
import {Player} from './player';
import {LevelMachineState} from './state';
import {LevelController} from './levelController';

export enum TrackingCameraLevel {
  normal,
  warning,
  danger,
}

export class TrackingCamera extends Actor {
  lci: LevelController;

  constructor (
    lci: LevelController,
    args: ActorArgs,
  ) {
    super({
      name: 'trackingCamera',
      collisionType: ex.CollisionType.Passive,
      ...args,
    });

    this.lci = lci;
  }

  private _cameraLevel: TrackingCameraLevel = TrackingCameraLevel.normal;

  get cameraLevel (): TrackingCameraLevel {
    return this._cameraLevel;
  }

  onInitialize (_engine: Engine) {
    this.graphics.use(sprites.trackingCamera.getSprite(2, 0)!);
    this.graphics.anchor = vec(0.5, 0.65);
  }

  onPostUpdate (_engine: Engine, _delta: number) {
    super.onPostUpdate(_engine, _delta);

    let closestActor: Actor;
    if (this.lci.levelService.state.value == LevelMachineState.playerDidNotPrayAndWentAway) {
      closestActor = this.lci.player;
    } else {
      // Find the closest character
      closestActor = this.lci.queryAllTrackableActors.getEntities().reduce((prev, curr) => {
        const diffAbsPrev = Math.sqrt(Math.pow((prev as Actor).pos.x - this.pos.x, 2) + Math.pow((prev as Actor).pos.y - this.pos.y, 2));
        const diffAbsCurr = Math.sqrt(Math.pow((curr as Actor).pos.x - this.pos.x, 2) + Math.pow((curr as Actor).pos.y - this.pos.y, 2));

        if (diffAbsCurr < diffAbsPrev) {
          return curr;
        }
        return prev;
      }) as Actor;
    }

    let newLevel: TrackingCameraLevel = TrackingCameraLevel.normal;

    if (closestActor instanceof Player) {
      switch (this.lci.levelService.state.value) {
        case LevelMachineState.shouldPray:
        case LevelMachineState.playerDidNotPray:
          newLevel = TrackingCameraLevel.warning;
          break;
        case LevelMachineState.playerDidNotPrayAndWentAway:
          newLevel = TrackingCameraLevel.danger;
          break;
        default:
          newLevel = TrackingCameraLevel.normal;
          break;
      }
    }

    this._cameraLevel = newLevel;

    let idx = 0;

    // Pick the sprite to use depending on the player position
    const diffX = (closestActor.pos.x - this.pos.x) / 16;
    if (diffX <= -1.5) {
      idx = 0;
    } else if (diffX <= -0.5) {
      idx = 1;
    } else if (diffX >= 1.5) {
      idx = 4;
    } else if (diffX >= 0.5) {
      idx = 3;
    } else {
      idx = 2;
    }

    if (newLevel == TrackingCameraLevel.danger) {
      this.graphics.use(animations.trackingCameraAlert[idx]);
    } else {
      let row = 0;
      switch (newLevel) {
        case TrackingCameraLevel.normal:
          row = 0;
          break;
        case TrackingCameraLevel.warning:
          row = 1;
          break;
      }
      this.graphics.use(sprites.trackingCamera.getSprite(idx, row)!);
    }
  }
}