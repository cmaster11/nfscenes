import * as ex from 'excalibur';
import {Actor, BodyComponent, CollisionType, Engine, Shape, Side, SpriteSheet, vec} from 'excalibur';
import {resources, sprites, subSprites} from './resources';
import {Player} from './player';
import {TrackingCamera} from './trackingCamera';
import {NPC} from './npc';
import {TiledLayerComponent, TiledObject, TiledObjectComponent} from '@excaliburjs/plugin-tiled';
import {DialogBox} from '../../common/dialogBox';
import {LevelController} from './levelController';
import {extractInfoScreenLines, InfoScreen} from '../../common/infoScreen';
import {extractTileMapEdges, fixTileMapVector} from '../../util/tileMap';
import {
  dialogs,
  infoScreenControlsName,
  infoScreenNotPrayName,
  infoScreenPrayName,
  interactableKeyLeader,
  levelTitle,
  tagTrackableActor,
} from './constants';
import {registerInWindow} from '../../system';
import {collectionTitle, collisionGroupWorld, zCeiling, zDialog, zInfo, zObj} from '../../common/constants';

const circleColliderPadding = 4;
const mapEdgePadding = 4;
const infoScreenTopLineOffsetY = -180;

export class LevelLeader extends ex.Scene {
  private levelController!: LevelController;

  onInitialize (engine: ex.Engine) {
    const levelController = new LevelController(engine, resources.map);
    registerInWindow('levelController', levelController);
    const fullTitle = `${collectionTitle} ${levelTitle}`;

    // Controls info screen
    {
      const infoScreen = new InfoScreen(levelController, {
        title: fullTitle,
        lines: extractInfoScreenLines(dialogs[infoScreenControlsName], {
          infoScreenTopLineOffsetY,
        }),
        showTextContinue: true,
        onDone: (infoScreen) => infoScreen.hide(),
      }, {
        name: infoScreenControlsName,
        z: zInfo,
      });
      engine.add(infoScreen);
    }

    // Pray screen
    {
      const lines: string[] = dialogs[infoScreenPrayName];
      const infoScreen = new InfoScreen(levelController, {
        title: fullTitle,
        lines: extractInfoScreenLines(lines.slice(0, lines.length - 1), {
          infoScreenTopLineOffsetY,
          linesDiffBig: 70,
        }),
        showTextContinue: true,
        continueLineText: lines[lines.length - 1],
      }, {
        name: infoScreenPrayName,
        z: zInfo,
      });
      engine.add(infoScreen);
    }

    // Not pray screen
    {
      const lines: string[] = dialogs[infoScreenNotPrayName];
      const infoScreen = new InfoScreen(levelController, {
        title: fullTitle,
        lines: extractInfoScreenLines(lines.slice(0, lines.length - 1), {
          infoScreenTopLineOffsetY,
          linesDiffBig: 70,
        }),
        showTextContinue: true,
        continueLineText: lines[lines.length - 1],
      }, {
        name: infoScreenNotPrayName,
        z: zInfo,
      });
      engine.add(infoScreen);
    }

    const layerInteractables = resources.map.data.getObjectLayerByName('interactable');

    resources.map.addTiledMapToScene(this);

    // engine.showDebug(true);
    // engine.currentScene.camera.zoom = 3;

    // Fix all ceiling tiles
    const ceiling = resources.map.getTileMapLayers().find((l) => (l.getComponents().find((c) => c instanceof TiledLayerComponent) as (TiledLayerComponent | null))?.layer?.name == 'ceiling');
    if (ceiling) {
      ceiling.z = zCeiling;
    }

    // Fix all objects which want a zClass ceiling
    engine.currentScene.actors
      .filter((a) => a.get(TiledObjectComponent)?.object.getProperty('zClass')?.value == 'ceiling')
      .forEach((a) => a.z = zCeiling + 1);

    // Initialize all tracking cameras
    resources.map.data.getObjectLayerByName('trackingCamera')?.objects
      .forEach((o) => {
        // Delete the old tile
        engine.currentScene.actors
          .filter((a) => a.get(TiledObjectComponent)?.object.rawObject == o.rawObject)
          .forEach((a) => engine.remove(a));

        const actor = new TrackingCamera(levelController, {
          collisionGroup: collisionGroupWorld,
          collider: ex.Shape.Circle(16 / 2 + circleColliderPadding),
          z: zObj,
          name: o.rawObject.name,
        });
        // If the collision happens on a corner, peace and love
        actor.pos = fixTileMapVector(vec(o.x, o.y));
        actor.addComponent(new TiledObjectComponent(o));
        engine.add(actor);
      });

    // Leader
    {
      const leaderObject: TiledObject = layerInteractables.objects.find((c) => c.name == interactableKeyLeader)!;

      // Delete the old tile
      engine.currentScene.actors
        .filter((a) => a.get(TiledObjectComponent)?.object.rawObject == leaderObject.rawObject)
        .forEach((a) => engine.remove(a));

      const actor = new Actor({
        collisionGroup: collisionGroupWorld,
        collider: ex.Shape.Circle(16 / 2 + circleColliderPadding),
        name: interactableKeyLeader,
        z: zObj,
      });
      // If the collision happens on a corner, peace and love
      actor.pos = fixTileMapVector(vec(leaderObject.x, leaderObject.y));
      actor.addComponent(new TiledObjectComponent(leaderObject));
      actor.graphics.use(subSprites.leader);
      engine.add(actor);
    }

    // Add the map edge as a blocking element
    const edges = extractTileMapEdges(resources.map);
    edges.forEach(([v1, v2], idx) => {
      const edgeSide = Side.fromDirection(v2.sub(v1));

      // BOLD assumption here that edges are only vertical or horizontal!
      const isHorizontal = edgeSide == Side.Left || edgeSide == Side.Right;
      const length = v2.sub(v1).size;
      const collider = Shape.Box(
        isHorizontal ? length : mapEdgePadding,
        isHorizontal ? mapEdgePadding : length,
        vec(isHorizontal ? 0 : 0.5, isHorizontal ? 0.5 : 0),
        edgeSide == Side.Right || edgeSide == Side.Bottom ? v1 : v2,
      );

      // const collider = new NFEdgeCollider({
      //   begin: v1,
      //   end: v2,
      // }, 4);

      const edge = new Actor({
        name: `edge-${idx}-${edgeSide}`,
        collider: collider,
        collisionType: CollisionType.Fixed,
        collisionGroup: collisionGroupWorld,
      });
      engine.add(edge);
    });

    // Make solid elements of the map not collide with each other
    this.tileMaps.forEach((tm) => {
      tm.getComponents().filter((c) => c instanceof BodyComponent)
        .forEach((b) => {
          (b as BodyComponent).group = collisionGroupWorld;
        });
    });

    const dialogBox = new DialogBox(levelController, {
      z: zDialog,
    });

    // NPCs
    {
      const npcLayerObjects: TiledObject[] = resources.map.data.getObjectLayerByName('npc')!.objects;

      npcLayerObjects.forEach((npcObject) => {
        const npc = new NPC(levelController, (sprites as Record<string, SpriteSheet>)[npcObject.name!]!, {
          name: npcObject.name,
        });
        npc.baseZ = zObj;
        npc.pos = new ex.Vector(npcObject.x, npcObject.y);

        if (npcObject.getProperty('trackable')?.value == false) {
          //
        } else {
          npc.addTag(tagTrackableActor);
        }

        engine.add(npc);
      });
    }

    {
      const player = new Player(levelController, sprites.player);

      const playerObject = resources.map.data.getObjectLayerByName('player')!.objects.at(0);
      if (playerObject != null) {
        player.pos = new ex.Vector(playerObject.x, playerObject.y);
      }
      player.baseZ = zObj;

      player.addTag(tagTrackableActor);

      engine.add(player);
      registerInWindow('player', player);

      // engine.currentScene.camera.addStrategy(new LockCameraToActorStrategy(player));
    }

    engine.add(dialogBox);

    registerInWindow('map', resources.map);
    registerInWindow('dialogBox', dialogBox);

    this.levelController = levelController;
    levelController.run();

    levelController.infoScreenControls?.show();
  }

  onPreUpdate (engine: Engine, _delta: number) {
    super.onPreUpdate(engine, _delta);

    const hadActionHandledThisFrame = this.levelController.actionHandledThisFrame;

    this.levelController.setActionHandledThisFrame(false);

    if (hadActionHandledThisFrame || this.levelController.interactionDisabled) {
      return;
    }

    if (engine.input.keyboard.wasPressed(ex.Input.Keys.Esc)) {
      this.levelController.setActionHandledThisFrame();
      this.levelController.infoScreenControls?.show();
    }
  }
}