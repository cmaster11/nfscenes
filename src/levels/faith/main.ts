import * as ex from 'excalibur';
import {DisplayMode, Flags} from 'excalibur';
import {LevelLeader} from './levelLeader';
import {loaderResources} from './resources';
import {NFLoader} from '../../common/loader';
import {debug, registerInWindow} from '../../system';
import {commonSoundResources, commonSounds} from '../../common/sounds';
import {NFKeyboard} from '../../common/keyboard';
import {commonLoaderResources} from '../../common/resources';
import {levelTitle} from './constants';

const loader = new NFLoader({levelTitle});
loader.suppressPlayButton = false;

Flags.useCanvasGraphicsContext();
const engine = new ex.Engine({
  backgroundColor: ex.Color.fromHex('#d2d2d2'),
  width: 640,
  height: 540,
  // Turn off anti-aliasing for pixel art graphics
  antialiasing: false,
  displayMode: DisplayMode.Fixed,
  suppressConsoleBootMessage: true,
  canvasElementId: 'main',
});
engine.input.keyboard = new NFKeyboard();
engine.input.keyboard.init(window);

loader.addResources(commonSoundResources);
loader.addResources(commonLoaderResources);
loader.addResources(loaderResources);

engine.add('leader', new LevelLeader());
engine.goToScene('leader');

engine.on('preupdate', (e) => {
  if (e.engine.input.keyboard.wasPressed(ex.Input.Keys.R)) {
    commonSounds.reset.play();

    e.engine.remove('leader');
    engine.add('leader', new LevelLeader());
    engine.goToScene('leader');
  }
});

// Start the engine
engine.start(loader).then(() => {
  debug('game start');
})
  .catch((err) => debug('loading error', err));

registerInWindow('engine', engine);