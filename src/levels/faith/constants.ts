// Cameras will follow trackable actors
import levelText from './level.yaml';

export const levelTitle = 'Faith';

export const tagTrackableActor = 'trackableActor';

export const infoScreenControlsName = 'infoScreenControls';
export const infoScreenPrayName = 'infoScreenPray';
export const infoScreenNotPrayName = 'infoScreenNotPray';

export const npcKeyPolice = 'npc_police';
export const npcKeyNPC1 = 'npc_npc1';

export const interactableKeyLeader = 'leader';


export const dialogs = levelText.dialogs;
export const dialogsDefault = levelText.dialogs['default'];