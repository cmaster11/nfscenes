import {Actor, Engine, Side, vec} from 'excalibur';
import {BaseLevelController} from '../../common/baseLevelController';
import {InfoScreen} from '../../common/infoScreen';
import {interpret, Interpreter} from 'xstate';
import {levelMachine, LevelMachineState} from './state';
import {NFMeet} from '../../common/actionNFMeet';
import {
  dialogs,
  infoScreenControlsName,
  infoScreenNotPrayName,
  infoScreenPrayName,
  interactableKeyLeader,
  npcKeyPolice,
} from './constants';
import {debug} from '../../system';
import {showUIPopup, UIPopup} from '../../common/ui';
import {commonSounds} from '../../common/sounds';
import {sleeper} from '../../util/promise';
import {npcSpeed} from '../../common/constants';
import {NPC} from './npc';
import {NFTiledMapResource} from '../../common/nfTiledMapResource';

export class LevelController extends BaseLevelController<LevelController> {
  leader!: Actor;
  levelService!: Interpreter<any>;

  infoScreenControls?: InfoScreen<LevelController>;
  infoScreenPray?: InfoScreen<LevelController>;
  infoScreenNotPray?: InfoScreen<LevelController>;

  constructor (engine: Engine, map: NFTiledMapResource) {
    super(engine, map);
  }

  run () {
    const engine = this.engine;

    this.leader = engine.currentScene.actors.find((a) => a.name == interactableKeyLeader)!;

    this.infoScreenControls = engine.currentScene.actors.find((a) => a instanceof InfoScreen && a.name == infoScreenControlsName) as InfoScreen<LevelController>;
    this.infoScreenPray = engine.currentScene.actors.find((a) => a instanceof InfoScreen && a.name == infoScreenPrayName) as InfoScreen<LevelController>;
    this.infoScreenNotPray = engine.currentScene.actors.find((a) => a instanceof InfoScreen && a.name == infoScreenNotPrayName) as InfoScreen<LevelController>;

    const police = this.queryAllNPCs.getEntities().find((n) => n.name == npcKeyPolice)! as NPC;

    this.levelService = interpret(levelMachine)
      .onTransition((state) => {
        debug('new state', state.value);

        switch (state.value as LevelMachineState) {
          case LevelMachineState.playerDidPray:
            commonSounds.gameOverUnknownResult.play();
            this.infoScreenPray?.show();
            break;
          case LevelMachineState.shouldPray:
          case LevelMachineState.playerDidNotPray:
            police.trackActor = this.player;
            break;
          case LevelMachineState.playerDidNotPrayAndWentAway:
            // Player is now blocked
            this.setInteractionDisabled(true);

            Promise.race([
              commonSounds.alarm.play(),
              sleeper(2000),
            ]).then(() => {
              return showUIPopup({
                engine: engine,
                targetActor: police,
                duration: 1500,
                offset: vec(0, 6),
                popup: UIPopup.exclamationRed,
              });
            })
              .then(() => {
                police.actions
                  .getQueue()
                  .add(new NFMeet(
                    {
                      actor: police,
                      actorToMeet: this.player,
                      speed: npcSpeed,
                    }));

                return police.actions
                  .toPromise();
              })
              .then(async () => {
                this.player.direction = Side.getOpposite(police.direction);

                return showUIPopup({
                  engine: engine,
                  targetActor: this.player,
                  duration: 1000,
                  offset: vec(0, 8),
                  popup: UIPopup.question,
                });
              })
              .then(async () => {
                this.player.direction = Side.getOpposite(police.direction);
                await this.dialogBox.show(
                  dialogs[LevelMachineState.playerDidNotPrayAndWentAway]['policeToPlayer'],
                  {ignoreInteractionDisabled: true});
              })
              .then(() => {
                debug('action success');
                commonSounds.gameOverLose.play();
                this.infoScreenNotPray?.show();
              });
            break;
        }
      })
      .start();
  }

}