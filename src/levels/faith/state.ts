import {createMachine} from 'xstate';
import {registerInWindow} from '../../system';

/*

The player can:

- Go to the NPC and talk, the NPC will ask the player if he has already played or not
- Go to a camera
  - If camera is yellow, "they're looking at me"
  - If camera is normal, "they're ignoring me"
- Go to the leader
  - If not talked with NPC, "A lovely portrait of our Leader!"
  - If already talked with NPC, you can pray
    - If pray, "you're a good citizen, time to go to work!"
    - If do not pray, danger, arrest and recondition!

 */

export enum LevelMachineState {
  walk = 'walk',
  shouldPray = 'shouldPray',
  playerDidPray = 'playerDidPray',
  playerDidNotPray = 'playerDidNotPray',
  playerDidNotPrayAndWentAway = 'playerDidNotPrayAndWentAway',
}

export enum LevelMachineStateEvents {
  PLAYER_LOOKED_AT_PAINTING = 'PLAYER_LOOKED_AT_PAINTING',
  PLAYER_DID_PRAY = 'PLAYER_DID_PRAY',
  PLAYER_DID_NOT_PRAY = 'PLAYER_DID_NOT_PRAY',
  PLAYER_WENT_AWAY = 'PLAYER_WENT_AWAY',
}

registerInWindow('LevelMachineStateEvents ', LevelMachineStateEvents);

export const levelMachine = createMachine({
  id: 'level',
  initial: LevelMachineState.walk,
  states: {
    [LevelMachineState.walk]: {
      on: {
        [LevelMachineStateEvents.PLAYER_LOOKED_AT_PAINTING]: LevelMachineState.shouldPray,
        [LevelMachineStateEvents.PLAYER_DID_PRAY]: LevelMachineState.playerDidPray,
        [LevelMachineStateEvents.PLAYER_DID_NOT_PRAY]: LevelMachineState.shouldPray,
      },
    },
    [LevelMachineState.shouldPray]: {
      on: {
        [LevelMachineStateEvents.PLAYER_DID_PRAY]: LevelMachineState.playerDidPray,
        [LevelMachineStateEvents.PLAYER_DID_NOT_PRAY]: LevelMachineState.playerDidNotPray,
      },
    },
    [LevelMachineState.playerDidPray]: {},
    [LevelMachineState.playerDidNotPray]: {
      on: {
        [LevelMachineStateEvents.PLAYER_WENT_AWAY]: LevelMachineState.playerDidNotPrayAndWentAway,
      },
    },
    [LevelMachineState.playerDidNotPrayAndWentAway]: {},
  },
});