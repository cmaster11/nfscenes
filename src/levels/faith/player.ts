import * as ex from 'excalibur';
import {Actor, ActorArgs, CollisionType, Color, Side, SpriteSheet, vec, Vector} from 'excalibur';
import {resources, subSprites} from './resources';
import {DirectionalActor} from '../../common/directionalActor';
import {LevelController} from './levelController';
import {NPC} from './npc';
import {LevelMachineState, LevelMachineStateEvents} from './state';
import {TrackingCamera, TrackingCameraLevel} from './trackingCamera';
import {TiledObjectComponent} from '@excaliburjs/plugin-tiled';
import {dialogs, dialogsDefault, interactableKeyLeader, npcKeyNPC1, npcKeyPolice} from './constants';
import {showUIPopup, UIPopup} from '../../common/ui';
import {sleeper} from '../../util/promise';
import {npcSpeed, zCeiling} from '../../common/constants';

const playerMovementCases: Partial<Record<ex.Input.Keys, [Side, (a: Actor) => void]>> = {
  [ex.Input.Keys.Left]: [Side.Left, (a) => a.vel.x = -npcSpeed],
  [ex.Input.Keys.Right]: [Side.Right, (a) => a.vel.x = npcSpeed],
  [ex.Input.Keys.Up]: [Side.Top, (a) => a.vel.y = -npcSpeed],
  [ex.Input.Keys.Down]: [Side.Bottom, (a) => a.vel.y = npcSpeed],
};

export class Player extends DirectionalActor<LevelController> {
  private talkCounter: Record<string, number> = {};
  private firstTalkedPlant?: string;
  private firstTalkedCamera?: string;

  constructor (lci: LevelController, spriteSheet: SpriteSheet, args?: ActorArgs) {
    super(lci, spriteSheet, {
      name: 'Player',
      collisionType: ex.CollisionType.Active,
      // collider: ex.Shape.Circle(10, ex.vec(0, 2)),
      ...args,
    });
  }

  // After main update, once per frame execute this code
  onPreUpdate (engine: ex.Engine, delta: number) {
    super.onPreUpdate(engine, delta);
    const stateValue = this.lci.levelService.state.value as string;

    const isMoving = this.vel.size > 0;

    if (isMoving && stateValue == LevelMachineState.playerDidNotPray) {
      /*
      As soon as the player moves too far away from the painting, trigger an alarm!
       */

      const dist = this.lci.leader.center.distance(this.center);
      if (dist >= 20) {
        this.lci.levelService.send(LevelMachineStateEvents.PLAYER_WENT_AWAY);
      }
    }

    this.vel = Vector.Zero;

    if (this.lci.interactionDisabled || this.lci.actionHandledThisFrame) {
      return;
    }

    if (engine.input.keyboard.wasPressed(ex.Input.Keys.Space)) {

      let actors: Actor[];
      if ((actors = this.getTouchedAndFacedActors((e) => e.actor instanceof NPC)).length == 1) {
        this.lci.setActionHandledThisFrame();
        let turnActor = false;

        const actorName = actors[0].name;
        this.talkCounter[actorName] = this.talkCounter[actorName] || 0;

        switch (actorName) {
          case npcKeyPolice: {
            turnActor = true;

            switch (stateValue) {
              case LevelMachineState.walk:
                this.lci.dialogBox.show(dialogs.walk.policeToPlayer);
                break;
              case LevelMachineState.shouldPray:
              default:
                const counter = this.talkCounter[actorName]++;

                switch (counter) {
                  case 0:
                    this.lci.dialogBox.show(dialogs.shouldPray.policeToPlayer_0);
                    break;
                  case 1:
                  default:
                    this.lci.dialogBox.show(dialogs.shouldPray.policeToPlayer_1);
                    break;
                }
                break;
            }
          }
            break;
          case npcKeyNPC1: {
            turnActor = true;

            switch (stateValue) {
              case LevelMachineState.walk:
                const counter = this.talkCounter[actorName]++;
                switch (counter) {
                  case 0:
                    this.lci.dialogBox.show(dialogs.walk.npc1ToPlayer_0);
                    break;
                  case 1:
                  default:
                    this.lci.dialogBox.show(dialogs.walk.npc1ToPlayer_1);
                    break;
                }
                break;
              case LevelMachineState.shouldPray:
              default:
                this.lci.dialogBox.show(dialogs.shouldPray.npc1ToPlayer);
            }
          }
            break;
        }
        if (turnActor) {
          (actors[0]! as NPC).direction = Side.getOpposite(this.direction);
        }

      } else if ((actors = this.getTouchedAndFacedActors((e) => e.actor instanceof TrackingCamera)).length == 1) {
        this.lci.setActionHandledThisFrame();

        const cameraName = actors[0].name;
        if (this.firstTalkedCamera == null) {
          this.firstTalkedCamera = cameraName;
        }

        switch ((actors[0] as TrackingCamera).cameraLevel) {
          case TrackingCameraLevel.normal:
            this.lci.dialogBox.show(this.firstTalkedCamera == cameraName ? dialogsDefault.playerToCamera_normal_first : dialogsDefault.playerToCamera_normal_second);
            break;
          case TrackingCameraLevel.warning:
            this.lci.dialogBox.show(this.firstTalkedCamera == cameraName ? dialogsDefault.playerToCamera_warning_first : dialogsDefault.playerToCamera_warning_second);
            break;
          case TrackingCameraLevel.danger:
            break;
        }

      } else if ((actors = this.getTouchedAndFacedActors((e) => e.actor.name == interactableKeyLeader)).length == 1) {
        this.lci.setActionHandledThisFrame();

        let dialogText;

        switch (stateValue) {
          case LevelMachineState.walk:
            dialogText = dialogs.playerToLeader.dialog.walk;
            break;
          case LevelMachineState.shouldPray:
          case LevelMachineState.playerDidNotPray:
            dialogText = dialogs.playerToLeader.dialog.shouldPray;
            break;
        }

        this.lci.dialogBox.show(dialogText, {
          sprite: resources.map.getSpriteForGid((actors[0]).get(TiledObjectComponent)!.object.gid!),
          choices: {
            title: dialogs.playerToLeader.choiceTitle,
            choices: dialogs.playerToLeader.choices,
          },
        }).then((choice) => {
          switch (choice) {
            case 0:
              this.lci.setInteractionDisabled(true);
              return sleeper(400)
                .then(() => showUIPopup({
                  engine: engine,
                  targetActor: this,
                  duration: 2500,
                  offset: vec(0, 8),
                  popup: UIPopup.pray,
                }))
                .then(() => this.lci.setInteractionDisabled(false))
                .then(() => this.lci.dialogBox.show(dialogs.playerToLeader.dialog.choicePray))
                .then(() => this.lci.levelService.send(LevelMachineStateEvents.PLAYER_DID_PRAY));
            case 1:
              this.lci.dialogBox.show(dialogs.playerToLeader.dialog.choiceNotPray)
                .then(() => this.lci.levelService.send(LevelMachineStateEvents.PLAYER_DID_NOT_PRAY));
              break;
          }
        });
      } else if ((actors = this.getTouchedAndFacedActors((e) => e.actor.get(TiledObjectComponent)?.object.type == 'vase')).length == 1) {
        this.lci.setActionHandledThisFrame();

        const thisPlantName = actors[0].get(TiledObjectComponent)!.object.name!;
        if (this.firstTalkedPlant == null) {
          this.firstTalkedPlant = thisPlantName;
        }

        if (thisPlantName == this.firstTalkedPlant) {
          this.talkCounter[thisPlantName] = this.talkCounter[thisPlantName] || 0;
          const counter = this.talkCounter[thisPlantName]++;

          switch (counter) {
            case 0:
              this.lci.dialogBox.show(dialogs.playerToPlant.default_first, {
                sprite: subSprites.plant,
              })
                .then(() => {
                  const paperPixel = new Actor({
                    color: Color.fromHex('#ffe7bf'),
                    collisionType: CollisionType.PreventCollision,
                    width: 1,
                    height: 1,
                    z: zCeiling + 1,
                  });
                  paperPixel.pos = actors[0].pos.add(vec(12, -17));
                  engine.currentScene.add(paperPixel);
                  // Jump
                  paperPixel.actions
                    .moveBy(vec(0, -2), 32)
                    .moveBy(vec(0, 2), 16);
                });
              break;
            case 1:
              this.lci.dialogBox.show(dialogs.playerToPlant.counter_1.dialog_0, {
                sprite: subSprites.plant,
              })
                .then(() => {
                  this.lci.setInteractionDisabled(true);
                  return showUIPopup({
                    engine: engine,
                    targetActor: this,
                    duration: 1500,
                    offset: vec(0, 8),
                    popup: UIPopup.exclamationBlue,
                  });
                })
                .then(() => {
                  this.lci.setInteractionDisabled(false);
                  return this.lci.dialogBox.show(dialogs.playerToPlant.counter_1.dialog_1, {
                    sprite: subSprites.plant,
                  });
                });
              break;
            default:
              this.lci.dialogBox.show(dialogs.playerToPlant.counter_more, {
                sprite: subSprites.plant,
              });
          }
        } else {
          this.lci.dialogBox.show(dialogs.playerToPlant.default_second, {
            sprite: subSprites.plant,
          });
        }

      }
      return;
    }

    Object.keys(playerMovementCases)
      .forEach((key) => {
        if (!engine.input.keyboard.isHeld(key as ex.Input.Keys)) {
          return;
        }

        const [side, fn] = playerMovementCases[key as ex.Input.Keys]!;
        this.direction = side;
        fn(this);
      });

    const velSize = this.vel.size;

    if (velSize > 0) {
      const velAngle = this.vel.toAngle();

      this.vel = Vector.fromAngle(velAngle).scale(npcSpeed);
      // debug('player vel', this.vel, velAngle, velSize)
    }
  }
}