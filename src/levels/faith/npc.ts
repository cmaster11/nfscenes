import * as ex from 'excalibur';
import {ActorArgs, Side, SpriteSheet, Vector} from 'excalibur';
import {DirectionalActor} from '../../common/directionalActor';
import {Player} from './player';
import {LevelController} from './levelController';

export class NPC extends DirectionalActor<LevelController> {
  private turnInMs = 0;

  constructor (lci: LevelController, spriteSheet: SpriteSheet, args?: ActorArgs) {
    super(lci, spriteSheet,
      {
        collisionType: ex.CollisionType.Fixed,
        // collider: ex.Shape.Circle(10, ex.vec(0, 2)),
        ...args,
      });
    this.setDirectionBasedOnVelocity = true;

    this.on('collisionstart', (e) => {
      if (e.other instanceof Player) {
        this.actions.getQueue().getActions().find(() => true)?.stop();
      }
    });

    this.resetTurnRandomly();
  }

  // After main update, once per frame execute this code
  onPreUpdate (engine: ex.Engine, delta: number) {
    super.onPreUpdate(engine, delta);

    // If there are possible interactions, do nothing
    if (this.getTouchedAndFacedActor(() => true)) {
      return;
    }

    this.turnInMs -= delta;
    if (this.turnInMs <= 0) {
      // Turn randomly
      this.direction = Side.fromDirection(Vector.fromAngle(Math.random() * 2 * Math.PI));
      this.resetTurnRandomly();
    }
  }

  private resetTurnRandomly () {
    this.turnInMs = Math.random() * 6000 + 2000;
  }
}