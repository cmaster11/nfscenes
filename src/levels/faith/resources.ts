import {TiledMapFormat} from '@excaliburjs/plugin-tiled';
import * as ex from 'excalibur';
import {Loadable, Util} from 'excalibur';
import {npcKeyNPC1, npcKeyPolice} from './constants';
import {DataResource} from '../../common/dataResource';
import {NFTiledMapResource} from '../../common/nfTiledMapResource';

export const resources = {
  player: new ex.ImageSource(require('../../../res/levels/faith/player.png')),
  [npcKeyPolice]: new ex.ImageSource(require('../../../res/levels/faith/npc_police.png')),
  [npcKeyNPC1]: new ex.ImageSource(require('../../../res/levels/faith/npc_npc1.png')),
  trackingCamera: new ex.ImageSource(require('../../../res/levels/faith/camera.png')),
  tilesetCity: new ex.ImageSource(require('../../../res/levels/faith/tileset-city.png')),

  map: new NFTiledMapResource(require('../../../res/levels/faith/level-walkroad.tmx'), {
    mapFormatOverride: TiledMapFormat.TMX,
  }),
};
(resources.map as any)._resource = new DataResource((resources.map as any)._resource.path, 'text');

export const sprites = {
  player: ex.SpriteSheet.fromImageSource({
    image: resources.player,
    grid: {
      columns: 24,
      rows: 3,
      spriteWidth: 16,
      spriteHeight: 32,
    },
  }),
  [npcKeyPolice]: ex.SpriteSheet.fromImageSource({
    image: resources[npcKeyPolice],
    grid: {
      columns: 24,
      rows: 3,
      spriteWidth: 16,
      spriteHeight: 32,
    },
  }),
  [npcKeyNPC1]: ex.SpriteSheet.fromImageSource({
    image: resources[npcKeyNPC1],
    grid: {
      columns: 24,
      rows: 3,
      spriteWidth: 16,
      spriteHeight: 32,
    },
  }),
  trackingCamera: ex.SpriteSheet.fromImageSource({
    image: resources.trackingCamera,
    grid: {
      columns: 5,
      rows: 3,
      spriteWidth: 16,
      spriteHeight: 16,
    },
  }),
  tilesetCity: ex.SpriteSheet.fromImageSource({
    image: resources.tilesetCity,
    grid: {
      columns: 12,
      rows: 6,
      spriteWidth: 16,
      spriteHeight: 16,
    },
  }),
};

export const subSprites = {
  leader: sprites.tilesetCity.getSprite(9, 1)!,
  plant: sprites.tilesetCity.getSprite(11, 0)!,
};

export const animations = {
  trackingCameraAlert: Util.range(0, 4).map((i) => new ex.Animation({
    frames: [
      {graphic: sprites.trackingCamera.getSprite(i, 1)!},
      {graphic: sprites.trackingCamera.getSprite(i, 2)!},
    ],
    frameDuration: 250,
  })),
};

resources.map.convertPath = function (originPath, relativePath) {
  const relPath = 'levels/faith/' + relativePath;
  return require('../../../res/' + relPath);
};

export const loaderResources: Loadable<any>[] = Object.values(resources);