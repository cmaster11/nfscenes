import {Engine, Sound} from 'excalibur';

interface ResourceLoaderCounter {
  done: boolean;
  max: number;
  current: number;
}

declare global {
  interface Window {
    // areResourcesAvailable?: () => ResourceLoaderCounter;
    // getResourcesLoaders?: () => Loadable<any>[];
    loadAllResources?: () => Promise<any>;
    loadAllResourcesStatus?: () => number;
    loadAllResourcesStatusTotal?: () => number;
    getIntroSound?: () => Sound;
    onEngineLoaded?: (engine: Engine) => void;
  }
}
