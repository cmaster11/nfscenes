const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const HtmlWebpackPlugin = require('html-webpack-plugin');
const {DefinePlugin, NamedModulesPlugin} = require('webpack');

const isProduction = process.env.NODE_ENV === 'production';

module.exports = {
  devtool: isProduction ? undefined : 'source-map',
  devServer: {
    allowedHosts: ['all'],
    port: 9000,
  },
  optimization: {
    moduleIds: 'named',
    runtimeChunk: 'single',
    splitChunks: {
      chunks: 'all',
    },
  },
  node: {
    __dirname: true,
  },
  entry: process.env.NFLEVEL ? {
    main: {
      import: `./src/levels/${process.env.NFLEVEL}/main.ts`,
    },
    initLevel: {
      import: `./src/levels/${process.env.NFLEVEL}/levelInit.ts`,
    },
  } : undefined,
  plugins: [
    new DefinePlugin({
      'process.env.FORCE_DEBUG': JSON.stringify(process.env.FORCE_DEBUG),
    }),
    new CopyPlugin({
      patterns: [
        {from: `./src/levels/${process.env.NFLEVEL}/cover.png`, to: 'cover.png'},
      ],
    }),
    new HtmlWebpackPlugin({
      template: process.env.USE_CSP ? 'index-csp.html' : 'index.html',
      templateParameters: (compilation, assets, tags, options) => {
        tags.headTags.forEach((tag) => {
          if (tag.tagName === 'script') {
            // if (tag.attributes.src.includes('initLevel')) {
            //   tag.attributes.async = true;
            // }
          }
        });
        return {
          htmlWebpackPlugin: {options},
        };
      },
    }),
    isProduction ? new BundleAnalyzerPlugin({
      analyzerMode: 'static',
      openAnalyzer: false,
    }) : undefined,
  ].filter((e) => e),
  resolve: {
    extensions: ['.ts', '.js'],
    fallback: {'path': require.resolve('path-browserify')},
  },
  output: {
    filename: '[name].bundle.js',
    sourceMapFilename: '[file].map',
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.ts?$/,
        use: 'ts-loader',
        include: [
          path.resolve(__dirname, 'src'),
          // path.resolve(__dirname, 'node_modules', '@excaliburjs', 'plugin-tiled', 'src'),
        ],
      },
      {
        test: /\.ttf$/,
        type: 'asset/inline',
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: 'style-loader',
            options: {
              insert: 'head',
              injectType: 'singletonStyleTag',
            },
          },
          'css-loader',
        ],
      },
      {
        test: /\.(mp3)$/,
        type: 'asset/inline',
      },
      {
        test: /\.(png)$/,
        type: 'asset/inline',
      },
      {
        test: /\.(tmx|tsx)$/,
        type: 'asset/inline',
        generator: {
          dataUrl: {
            encoding: 'base64',
            mimetype: 'application/xml',
          },
        },
      },
      {
        test: /\.ya?ml$/,
        type: 'json',
        use: [
          {
            loader: 'yaml-loader',
            options: {
              merge: true,
            },
          },
        ],
      },
    ],
  },
};
