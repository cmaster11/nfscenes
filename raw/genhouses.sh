#!/usr/bin/env bash
set -Eeumo pipefail
DIR=$(
  cd "$(dirname "$0")"
  pwd -P
)

fileIn="$DIR/housebase.png"
dirOut="$DIR/gen"
fileOut="$DIR/../res/common/houses.png"

rm -rf "$dirOut/"*.png || true

info=$(identify "$fileIn")
width=$(echo "$info" | awk '{print $3}' | tr 'x' ' ' | awk '{print $1}')
height=$(echo "$info" | awk '{print $3}' | tr 'x' ' ' | awk '{print $2}')
halfHeight=$((height / 2))

echo "height $height; $halfHeight"

tmpWalls=$(mktemp)
tmpDoors=$(mktemp)

convert -extract "${width}x${halfHeight}+0+0" "$fileIn" "$tmpWalls"
convert -extract "${width}x${halfHeight}+0+${halfHeight}" "$fileIn" "$tmpDoors"

# Shift HUE

hues=(0 20 40 60 80 100 120 140 160 180 200)
sats=(30 60 100)
# shellcheck disable=SC2068
for hue in ${hues[@]}; do
  for sat in ${sats[@]}; do
    fileName="$dirOut/house_h$(printf "%03d" $hue)_s$(printf "%03d" $sat).png"

    # Shift hue
    convert -define modulate:colorspace=HSB "$tmpWalls" \
      -modulate 100,"$sat","$hue" "$fileName"

    # Superimpose doors
    convert "$fileName" "$tmpDoors" -gravity center -composite "$fileName"
  done
done

# shellcheck disable=SC2068
for sat in ${sats[@]}; do
  glob="$dirOut/house_h*_s$(printf "%03d" $sat).png"
  fileName="$dirOut/cat_house__s$(printf "%03d" $sat).png"

  # Merge by sat
  convert -append "$glob" "$fileName"
done

# Merge all
convert +append "$dirOut/cat_*.png" "$fileOut"

# Special entry for grayscale
outGray="$dirOut/gray.png"
(
  sat=0
  lums=(30 60 90)

  # shellcheck disable=SC2068
  for lum in ${lums[@]}; do
    fileName="$dirOut/house_l$(printf "%03d" $lum)_s$(printf "%03d" $sat).png"

    # Shift lum
    convert -define modulate:colorspace=HSB "$tmpWalls" \
      -modulate "$lum","$sat",0 "$fileName"

    # Superimpose doors
    convert "$fileName" "$tmpDoors" -gravity center -composite "$fileName"
  done

  convert +append "$dirOut/house_l*.png" "$outGray"
)

# Merge with gray
convert -append "$outGray" "$fileOut" "$fileOut"